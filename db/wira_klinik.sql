-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 01, 2017 at 11:05 PM
-- Server version: 5.6.25
-- PHP Version: 5.5.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wira_klinik`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `pembayaran_detail`(IN `pembayaran_id` VARCHAR(32), IN `pembayaran_tipe` ENUM('resep','pendaftaran','tambahan'))
    NO SQL
BEGIN
IF pembayaran_tipe = 'pendaftaran' or pembayaran_tipe='tambahan'
THEN
select poliklinik_layanan.nama as layanan, poliklinik_layanan.tarif from pembayaran inner join pendaftaran on pembayaran.id_pendaftaran = pendaftaran.id_pendaftaran inner join pemeriksaan on pemeriksaan.id_pendaftaran = pendaftaran.id_pendaftaran inner join pemeriksaan_detail on pemeriksaan_detail.id_pemeriksaan = pemeriksaan.id_pemeriksaan inner join poliklinik_layanan on poliklinik_layanan.id_p_layanan = pemeriksaan_detail.id_p_layanan where pembayaran.id_pembayaran= pembayaran_id and pemeriksaan.tipe = pembayaran_tipe;
ELSE
	SELECT pembayaran.tipe_pembayaran as layanan, pembayaran.jumlah as tarif from pembayaran where pembayaran.id_pembayaran = pembayaran_id and pembayaran.tipe_pembayaran='resep';
END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `pendaftaran_detail`(IN `pendaftaran_id` VARCHAR(32))
    NO SQL
select pemeriksaan_detail.id_p_detail as id,('layanan') as jenis, poliklinik_layanan.nama, poliklinik_layanan.tarif from pemeriksaan_detail inner join pemeriksaan on pemeriksaan.id_pemeriksaan = pemeriksaan_detail.id_pemeriksaan inner join pendaftaran on pendaftaran.id_pendaftaran = pemeriksaan.id_pendaftaran inner join poliklinik_layanan on poliklinik_layanan.id_p_layanan = pemeriksaan_detail.id_p_layanan where pendaftaran.id_pendaftaran = pendaftaran_id
union
select resep.id_resep as id, ('resep dokter') as jenis, ('resep') as nama, resep.total as tarif from resep inner join pendaftaran on pendaftaran.id_pendaftaran = resep.id_pendaftaran where pendaftaran.id_pendaftaran = pendaftaran_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `pendapatan_bulanan`(IN `poliklinik_id` VARCHAR(32), IN `tahun` YEAR)
    NO SQL
select b.bulan,jumlah_pasien(poliklinik_id,'month',b.bulan,tahun) as jumlah_pasien,pendapatan(poliklinik_id,'month',b.bulan,tahun) as pendapatan from (SELECT 1 bulan union SELECT 2 bulan union SELECT 3 bulan union SELECT 4 bulan union SELECT 5 bulan union SELECT 6 bulan union SELECT 7 bulan union SELECT 8 bulan union SELECT 9 bulan union SELECT 10 bulan union SELECT 11 bulan union SELECT 12 bulan) as b$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `v_pendapatan`(IN `jenis` ENUM('date','month','year'), IN `tanggal` VARCHAR(30), IN `tahun` YEAR)
    NO SQL
BEGIN
    IF jenis='date'
    THEN select poliklinik.id_poliklinik, poliklinik.nama, jumlah_pasien(poliklinik.id_poliklinik,'date',tanggal,tahun) as jumlah_pasien, pendapatan(poliklinik.id_poliklinik,'date',tanggal,tahun) as pendapatan from poliklinik;
ELSEIF jenis='month'
    THEN select poliklinik.id_poliklinik, poliklinik.nama, jumlah_pasien(poliklinik.id_poliklinik,'month',tanggal,tahun) as jumlah_pasien, pendapatan(poliklinik.id_poliklinik,'month',tanggal,tahun) as pendapatan from poliklinik;
ELSEIF jenis = 'year'
    THEN select poliklinik.id_poliklinik, poliklinik.nama, jumlah_pasien(poliklinik.id_poliklinik,'year','',tahun) as jumlah_pasien, pendapatan(poliklinik.id_poliklinik,'year','',tahun) as pendapatan from poliklinik;

END IF;
END$$

--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `jumlah_pasien`(`poliklinik_id` VARCHAR(32), `jenis` ENUM('date','month','year'), `tanggal` VARCHAR(30), `tahun` YEAR) RETURNS int(11)
    NO SQL
BEGIN
    IF jenis='month'
    THEN SET @hi = (select ifnull(count(pendaftaran.id_pendaftaran),0) from poliklinik inner join dokter on dokter.id_poliklinik = poliklinik.id_poliklinik inner join dokter_jadwal on dokter_jadwal.id_dokter = dokter.id_dokter left join pendaftaran on pendaftaran.id_d_jadwal = dokter_jadwal.id_d_jadwal where month(pendaftaran.tgl) = tanggal  and year(pendaftaran.tgl) = tahun and poliklinik.id_poliklinik = poliklinik_id and pendaftaran.dlt is null and poliklinik.dlt is null
);

ELSEIF jenis='date'
    THEN SET @hi = (select ifnull(count(pendaftaran.id_pendaftaran),0) from poliklinik inner join dokter on dokter.id_poliklinik = poliklinik.id_poliklinik inner join dokter_jadwal on dokter_jadwal.id_dokter = dokter.id_dokter left join pendaftaran on pendaftaran.id_d_jadwal = dokter_jadwal.id_d_jadwal where date(pendaftaran.tgl) = tanggal  and year(pendaftaran.tgl) = tahun and poliklinik.id_poliklinik = poliklinik_id and pendaftaran.dlt is null and poliklinik.dlt is null
);
    ELSEIF jenis = 'year'
    THEN SET @hi = (select ifnull(count(pendaftaran.id_pendaftaran),0) from poliklinik inner join dokter on dokter.id_poliklinik = poliklinik.id_poliklinik inner join dokter_jadwal on dokter_jadwal.id_dokter = dokter.id_dokter left join pendaftaran on pendaftaran.id_d_jadwal = dokter_jadwal.id_d_jadwal where year(pendaftaran.tgl) = tahun and poliklinik.id_poliklinik = poliklinik_id and pendaftaran.dlt is null and poliklinik.dlt is null
);
    END IF;
    
    return @hi;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `no_antrian`(`dokter_jadwal_id` VARCHAR(32)) RETURNS int(4)
    NO SQL
return (SELECT ifnull((select no_antrian FROM `v_antrian` inner join dokter on dokter.id_poliklinik = v_antrian.id_poliklinik inner join dokter_jadwal on dokter_jadwal.id_dokter = dokter.id_dokter where dokter_jadwal.id_d_jadwal=dokter_jadwal_id), 0))+1$$

CREATE DEFINER=`root`@`localhost` FUNCTION `pendapatan`(`poliklinik_id` VARCHAR(32), `jenis` ENUM('date','month','year'), `tanggal` VARCHAR(30), `tahun` YEAR) RETURNS double
    NO SQL
BEGIN
    IF jenis='month'
    THEN SET @hi = (select ifnull(sum(pembayaran.jumlah),0) from poliklinik inner join dokter on dokter.id_poliklinik = poliklinik.id_poliklinik inner join dokter_jadwal on dokter_jadwal.id_dokter = dokter.id_dokter left join pendaftaran on pendaftaran.id_d_jadwal = dokter_jadwal.id_d_jadwal left join pembayaran on pembayaran.id_pendaftaran = pendaftaran.id_pendaftaran where month(pembayaran.tgl) = tanggal and year(pembayaran.tgl) = tahun and poliklinik.id_poliklinik = poliklinik_id and poliklinik.dlt is null and pembayaran.dlt is null and pembayaran.tipe_pembayaran NOT IN ('resep'));

ELSEIF jenis='date'
    THEN SET @hi = (select ifnull(sum(pembayaran.jumlah),0) from poliklinik inner join dokter on dokter.id_poliklinik = poliklinik.id_poliklinik inner join dokter_jadwal on dokter_jadwal.id_dokter = dokter.id_dokter left join pendaftaran on pendaftaran.id_d_jadwal = dokter_jadwal.id_d_jadwal left join pembayaran on pembayaran.id_pendaftaran = pendaftaran.id_pendaftaran where date(pembayaran.tgl) = tanggal and year(pembayaran.tgl) = tahun and poliklinik.id_poliklinik = poliklinik_id and poliklinik.dlt is null and pembayaran.dlt is null and pembayaran.tipe_pembayaran NOT IN ('resep'));

ELSEIF jenis = 'year'
    THEN SET @hi = (select ifnull(sum(pembayaran.jumlah),0) from poliklinik inner join dokter on dokter.id_poliklinik = poliklinik.id_poliklinik inner join dokter_jadwal on dokter_jadwal.id_dokter = dokter.id_dokter left join pendaftaran on pendaftaran.id_d_jadwal = dokter_jadwal.id_d_jadwal left join pembayaran on pembayaran.id_pendaftaran = pendaftaran.id_pendaftaran where year(pembayaran.tgl) = tahun and poliklinik.id_poliklinik = poliklinik_id and poliklinik.dlt is null and pembayaran.dlt is null and pembayaran.tipe_pembayaran NOT IN ('resep'));

END IF;
    
    return @hi;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `pilih_dokter`(`poli_id` VARCHAR(32)) RETURNS varchar(32) CHARSET latin1
    NO SQL
return (select dokter_jadwal.id_d_jadwal from dokter_jadwal inner join dokter on dokter.id_dokter = dokter_jadwal.id_dokter inner join poliklinik on poliklinik.id_poliklinik = dokter.id_poliklinik where poliklinik.id_poliklinik = poli_id and hari=DAYNAME(date(now())) and time(now()) BETWEEN praktik_mulai and praktik_selesai and dokter_jadwal.dlt is null and poliklinik.dlt is null and dokter.dlt is null)$$

CREATE DEFINER=`root`@`localhost` FUNCTION `total_obat`(`obat_id` VARCHAR(32), `obat_jumlah` INT(32)) RETURNS double
    NO SQL
return (SELECT obat.harga from obat where obat.id_obat = obat_id)*obat_jumlah$$

CREATE DEFINER=`root`@`localhost` FUNCTION `total_pembayaran`(`pendaftaran_id` VARCHAR(32), `pendaftaran_tipe` ENUM('pendaftaran','tambahan')) RETURNS double
    NO SQL
return (select ifnull(sum(poliklinik_layanan.tarif),0) from pendaftaran inner join pemeriksaan on pemeriksaan.id_pendaftaran = pendaftaran.id_pendaftaran inner join pemeriksaan_detail on pemeriksaan_detail.id_pemeriksaan = pemeriksaan.id_pemeriksaan inner join poliklinik_layanan on poliklinik_layanan.id_p_layanan = pemeriksaan_detail.id_p_layanan where pendaftaran.id_pendaftaran = pendaftaran_id and pemeriksaan.tipe=pendaftaran_tipe)$$

CREATE DEFINER=`root`@`localhost` FUNCTION `total_resep`(`pendaftaran_id` VARCHAR(32)) RETURNS double
    NO SQL
return (select ifnull(sum(resep_detail.subtotal),0) from pendaftaran inner join resep on resep.id_pendaftaran = pendaftaran.id_pendaftaran inner join resep_detail on resep_detail.id_resep = resep.id_resep where pendaftaran.id_pendaftaran = pendaftaran_id)$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id_admin` varchar(32) NOT NULL,
  `id_c_kat_admin` varchar(32) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `crt` datetime NOT NULL,
  `mdf` datetime NOT NULL,
  `dlt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `id_c_kat_admin`, `username`, `password`, `crt`, `mdf`, `dlt`) VALUES
('a6ae869c-e208-11e6-8db4-c85b765b', 'admin8cc-e208-11e6-8db4-c85b765b', 'tsabitg23', 'b2238fe000cf5140b2e759c71d5ba36e', '2017-01-24 14:39:20', '2017-01-24 14:39:20', NULL),
('bb64ff04-e208-11e6-8db4-c85b765b', 'kasiraa4-e208-11e6-8db4-c85b765b', 'squid', '49ddafa93b4052e15e041a43d4343fda', '2017-01-24 14:39:55', '2017-01-24 14:39:55', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `config_jenis_obat`
--

CREATE TABLE IF NOT EXISTS `config_jenis_obat` (
  `id_c_jenis_obat` varchar(32) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `crt` datetime NOT NULL,
  `mdf` datetime NOT NULL,
  `dlt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `config_jenis_obat`
--

INSERT INTO `config_jenis_obat` (`id_c_jenis_obat`, `nama`, `crt`, `mdf`, `dlt`) VALUES
('044c750a-dfa0-11e6-8db4-c85b765b', 'antiseptik', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('044c91b6-dfa0-11e6-8db4-c85b765b', 'antivirus', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `config_kat_admin`
--

CREATE TABLE IF NOT EXISTS `config_kat_admin` (
  `id_c_kat_admin` varchar(32) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `crt` datetime NOT NULL,
  `mdf` datetime NOT NULL,
  `dlt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `config_kat_admin`
--

INSERT INTO `config_kat_admin` (`id_c_kat_admin`, `nama`, `crt`, `mdf`, `dlt`) VALUES
('8dac2357-e208-11e6-8db4-c85b765b', 'dokter', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('8dac4a7c-e208-11e6-8db4-c85b765b', 'apoteker', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('admin8cc-e208-11e6-8db4-c85b765b', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('kasiraa4-e208-11e6-8db4-c85b765b', 'kasir', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `config_kat_obat`
--

CREATE TABLE IF NOT EXISTS `config_kat_obat` (
  `id_c_kat_obat` varchar(32) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `crt` datetime NOT NULL,
  `mdf` datetime NOT NULL,
  `dlt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `config_kat_obat`
--

INSERT INTO `config_kat_obat` (`id_c_kat_obat`, `nama`, `crt`, `mdf`, `dlt`) VALUES
('176eb8bf-dfa0-11e6-8db4-c85b765b', 'kapsul', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('176edaf4-dfa0-11e6-8db4-c85b765b', 'tablet', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `config_spesialis`
--

CREATE TABLE IF NOT EXISTS `config_spesialis` (
  `id_c_spesialis` varchar(32) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `config_spesialis`
--

INSERT INTO `config_spesialis` (`id_c_spesialis`, `nama`) VALUES
('gigf72de-e29c-11e6-8db4-c85b765b', 'gigi'),
('umuf5cdd-e29c-11e6-8db4-c85b765b', 'umum');

-- --------------------------------------------------------

--
-- Table structure for table `dokter`
--

CREATE TABLE IF NOT EXISTS `dokter` (
  `id_dokter` varchar(32) NOT NULL,
  `id_poliklinik` varchar(32) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `id_c_spesialis` varchar(32) NOT NULL,
  `alamat` text NOT NULL,
  `telepon` varchar(15) NOT NULL,
  `gaji` double NOT NULL,
  `foto` text NOT NULL,
  `crt` datetime NOT NULL,
  `mdf` datetime NOT NULL,
  `dlt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dokter`
--

INSERT INTO `dokter` (`id_dokter`, `id_poliklinik`, `nama`, `id_c_spesialis`, `alamat`, `telepon`, `gaji`, `foto`, `crt`, `mdf`, `dlt`) VALUES
('3f2ab3eb-e490-11e6-8db4-c85b765b', 'b2f898de-e48f-11e6-8db4-c85b765b', 'lolita sari', 'umuf5cdd-e29c-11e6-8db4-c85b765b', 'jl. jalan coy', '081312312', 500000, 'dadaasdads', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('graham52-dfa1-11e6-8db4-c85b765b', '71c90bb7-dfa0-11e6-8db4-c85b765b', 'graham juli', 'gigf72de-e29c-11e6-8db4-c85b765b', 'jl. galaxy no.6', '08976538212', 3000000, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('suryadi5-dfa2-11e6-8db4-c85b765b', '71c95658-dfa0-11e6-8db4-c85b765b', 'suryadi', 'umuf5cdd-e29c-11e6-8db4-c85b765b', 'jl. bimasakti no.16', '08723723', 5000000, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('tari0fff-dfa1-11e6-8db4-c85b765b', '71c90bb7-dfa0-11e6-8db4-c85b765b', 'tari ngadirjo', 'gigf72de-e29c-11e6-8db4-c85b765b', 'jl. galaxy no.5', '084272323', 3000000, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('yakri5d4-dfa2-11e6-8db4-c85b765b', '71c95658-dfa0-11e6-8db4-c85b765b', 'yakri jaja', 'umuf5cdd-e29c-11e6-8db4-c85b765b', 'jl. bimasakti no.15', '08474743', 5000000, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dokter_jadwal`
--

CREATE TABLE IF NOT EXISTS `dokter_jadwal` (
  `id_d_jadwal` varchar(32) NOT NULL,
  `id_dokter` varchar(32) NOT NULL,
  `hari` varchar(20) NOT NULL,
  `praktik_mulai` time NOT NULL,
  `praktik_selesai` time NOT NULL,
  `crt` datetime NOT NULL,
  `mdf` datetime NOT NULL,
  `dlt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dokter_jadwal`
--

INSERT INTO `dokter_jadwal` (`id_d_jadwal`, `id_dokter`, `hari`, `praktik_mulai`, `praktik_selesai`, `crt`, `mdf`, `dlt`) VALUES
('5a362837-e490-11e6-8db4-c85b765b', '3f2ab3eb-e490-11e6-8db4-c85b765b', 'Friday', '11:30:00', '07:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('grahamju-dfa3-11e6-8db4-c85b765b', 'graham52-dfa1-11e6-8db4-c85b765b', 'Friday', '09:00:00', '15:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('grahamse-dfa3-11e6-8db4-c85b765b', 'graham52-dfa1-11e6-8db4-c85b765b', 'Monday', '09:00:00', '15:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('grahasel-dfa3-11e6-8db4-c85b765b', 'graham52-dfa1-11e6-8db4-c85b765b', 'Tuesday', '09:00:00', '15:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('suryajum-dfa4-11e6-8db4-c85b765b', 'suryadi5-dfa2-11e6-8db4-c85b765b', 'Friday', '13:00:00', '20:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('suryakam-dfa4-11e6-8db4-c85b765b', 'suryadi5-dfa2-11e6-8db4-c85b765b', 'Thursday', '07:00:00', '13:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('suryarab-dfa4-11e6-8db4-c85b765b', 'suryadi5-dfa2-11e6-8db4-c85b765b', 'Wednesday', '07:00:00', '13:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('suryasab-dfa4-11e6-8db4-c85b765b', 'suryadi5-dfa2-11e6-8db4-c85b765b', 'Saturday', '12:00:00', '17:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('suryasel-dfa4-11e6-8db4-c85b765b', 'suryadi5-dfa2-11e6-8db4-c85b765b', 'Tuesday', '13:00:00', '20:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('suryasen-dfa4-11e6-8db4-c85b765b', 'suryadi5-dfa2-11e6-8db4-c85b765b', 'Monday', '13:00:00', '20:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('tarikami-dfa3-11e6-8db4-c85b765b', 'tari0fff-dfa1-11e6-8db4-c85b765b', 'Thursday', '09:00:00', '15:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('tarirabu-dfa3-11e6-8db4-c85b765b', 'tari0fff-dfa1-11e6-8db4-c85b765b', 'Wednesday', '09:00:00', '15:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('tarisabt-dfa3-11e6-8db4-c85b765b', 'tari0fff-dfa1-11e6-8db4-c85b765b', 'Saturday', '09:00:00', '12:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('yakrijum-dfa4-11e6-8db4-c85b765b', 'yakri5d4-dfa2-11e6-8db4-c85b765b', 'Friday', '07:00:00', '13:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('yakrikam-dfa4-11e6-8db4-c85b765b', 'yakri5d4-dfa2-11e6-8db4-c85b765b', 'Thursday', '13:00:00', '20:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('yakrirab-dfa4-11e6-8db4-c85b765b', 'yakri5d4-dfa2-11e6-8db4-c85b765b', 'Wednesday', '13:00:00', '20:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('yakrisab-dfa4-11e6-8db4-c85b765b', 'yakri5d4-dfa2-11e6-8db4-c85b765b', 'Saturday', '07:00:00', '12:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('yakrisel-dfa4-11e6-8db4-c85b765b', 'yakri5d4-dfa2-11e6-8db4-c85b765b', 'Tuesday', '07:00:00', '13:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('yakrisen-dfa4-11e6-8db4-c85b765b', 'yakri5d4-dfa2-11e6-8db4-c85b765b', 'Monday', '07:00:00', '13:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `obat`
--

CREATE TABLE IF NOT EXISTS `obat` (
  `id_obat` varchar(32) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `id_c_jenis_obat` varchar(32) NOT NULL,
  `id_c_kat_obat` varchar(32) NOT NULL,
  `umur` enum('D','A','SU') NOT NULL,
  `harga` double NOT NULL,
  `stok` double NOT NULL,
  `crt` datetime NOT NULL,
  `mdf` datetime NOT NULL,
  `dlt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `obat`
--

INSERT INTO `obat` (`id_obat`, `nama`, `id_c_jenis_obat`, `id_c_kat_obat`, `umur`, `harga`, `stok`, `crt`, `mdf`, `dlt`) VALUES
('7bf90e74-dfa1-11e6-8db4-c85b765b', 'xanax', '044c91b6-dfa0-11e6-8db4-c85b765b', '176eb8bf-dfa0-11e6-8db4-c85b765b', 'D', 200000, 123, '0000-00-00 00:00:00', '2017-01-27 22:03:20', NULL),
('7bf93361-dfa1-11e6-8db4-c85b765b', 'paracetamol', '044c750a-dfa0-11e6-8db4-c85b765b', '176eb8bf-dfa0-11e6-8db4-c85b765b', 'SU', 15000, 98, '0000-00-00 00:00:00', '2017-02-02 03:49:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `obat_pemasokan`
--

CREATE TABLE IF NOT EXISTS `obat_pemasokan` (
  `id_pemasokan` varchar(32) NOT NULL,
  `id_obat` varchar(32) NOT NULL,
  `id_admin` varchar(32) NOT NULL,
  `tgl` datetime NOT NULL,
  `jumlah` int(11) NOT NULL,
  `total` double NOT NULL,
  `crt` datetime NOT NULL,
  `mdf` datetime NOT NULL,
  `dlt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `obat_pemasokan`
--

INSERT INTO `obat_pemasokan` (`id_pemasokan`, `id_obat`, `id_admin`, `tgl`, `jumlah`, `total`, `crt`, `mdf`, `dlt`) VALUES
('be437d92-e4a1-11e6-8db4-c85b765b', '7bf90e74-dfa1-11e6-8db4-c85b765b', 'bb64ff04-e208-11e6-8db4-c85b765b', '2017-01-27 21:58:32', 33, 3000000, '2017-01-27 21:58:32', '2017-01-27 21:58:32', NULL);

--
-- Triggers `obat_pemasokan`
--
DELIMITER $$
CREATE TRIGGER `insert_pemasokan_obat` AFTER INSERT ON `obat_pemasokan`
 FOR EACH ROW BEGIN
update obat set stok = stok+new.jumlah, mdf=now() where id_obat=new.id_obat;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_pemasokan_obat` AFTER UPDATE ON `obat_pemasokan`
 FOR EACH ROW BEGIN
IF OLD.jumlah < NEW.jumlah
THEN
update obat set stok = (stok + (NEW.jumlah-OLD.jumlah)), mdf = now() where id_obat = NEW.id_obat;
ELSEIF OLD.jumlah > NEW.jumlah
THEN
update obat set stok = (stok - (OLD.jumlah-NEW.jumlah)), mdf = now() where id_obat = NEW.id_obat;
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pasien`
--

CREATE TABLE IF NOT EXISTS `pasien` (
  `id_pasien` varchar(32) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama_depan` varchar(255) NOT NULL,
  `nama_belakang` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `jenis_kelamin` enum('L','P') NOT NULL,
  `tgl_lahir` date NOT NULL,
  `telepon` varchar(15) NOT NULL,
  `crt` datetime NOT NULL,
  `mdf` datetime NOT NULL,
  `dlt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pasien`
--

INSERT INTO `pasien` (`id_pasien`, `username`, `password`, `nama_depan`, `nama_belakang`, `alamat`, `jenis_kelamin`, `tgl_lahir`, `telepon`, `crt`, `mdf`, `dlt`) VALUES
('bal2c841-dfa7-11e6-8db4-c85b765b', 'ikbalmaul123', '04b28b27ca5f5b4d77211d2bc29a1e6f', 'ikbal', 'maul', 'jl. sempit', 'L', '1999-09-09', '0823232123', '2017-01-21 14:00:10', '2017-01-21 14:00:10', NULL),
('ham282fb-dfa7-11e6-8db4-c85b765b', 'ilhamdwi258', '4494aa6a84af93e3f341ef83db5da3c9', 'ilham', 'dwi', 'jl. harapan indah', 'L', '1999-02-18', '0823232293', '2017-01-21 14:00:10', '2017-01-21 14:00:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE IF NOT EXISTS `pembayaran` (
  `id_pembayaran` varchar(32) NOT NULL,
  `id_admin` varchar(32) NOT NULL,
  `id_pendaftaran` varchar(32) NOT NULL,
  `tgl` datetime NOT NULL,
  `tipe_pembayaran` enum('resep','pendaftaran','tambahan') NOT NULL,
  `jumlah` double NOT NULL,
  `bayar` double NOT NULL,
  `kembali` double NOT NULL,
  `crt` datetime NOT NULL,
  `mdf` datetime NOT NULL,
  `dlt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembayaran`
--

INSERT INTO `pembayaran` (`id_pembayaran`, `id_admin`, `id_pendaftaran`, `tgl`, `tipe_pembayaran`, `jumlah`, `bayar`, `kembali`, `crt`, `mdf`, `dlt`) VALUES
('05d0722a-e370-11e6-8db4-c85b765b', 'a6ae869c-e208-11e6-8db4-c85b765b', '5ebd140c-e2b8-11e6-8db4-c85b765b', '2017-01-26 09:30:39', 'tambahan', 10000, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('09ccf4e4-e7c0-11e6-8db4-c85b765b', 'a6ae869c-e208-11e6-8db4-c85b765b', 'caffbad2-e7bf-11e6-8db4-c85b765b', '2017-01-31 21:12:14', 'pendaftaran', 500000, 500000, 500000, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('0c41e743-e2b8-11e6-8db4-c85b765b', 'a6ae869c-e208-11e6-8db4-c85b765b', '586a60e6-e2b5-11e6-8db4-c85b765b', '2017-01-25 11:34:31', 'pendaftaran', 50000, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('45fe8eca-e2b8-11e6-8db4-c85b765b', 'a6ae869c-e208-11e6-8db4-c85b765b', '586a60e6-e2b5-11e6-8db4-c85b765b', '2017-01-25 11:36:08', 'tambahan', 50000, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('62d68625-e8be-11e6-8db4-c85b765b', 'bb64ff04-e208-11e6-8db4-c85b765b', '244e33ca-e8be-11e6-8db4-c85b765b', '2017-02-02 03:32:36', 'pendaftaran', 50000, 10000, 500000, '2017-02-02 03:32:36', '2017-02-02 03:32:36', NULL),
('71d4e26d-e342-11e6-8db4-c85b765b', 'a6ae869c-e208-11e6-8db4-c85b765b', '3c8d7a2b-e341-11e6-8db4-c85b765b', '2017-01-26 04:05:06', 'pendaftaran', 50000, 50000, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('9288d725-e2b8-11e6-8db4-c85b765b', 'a6ae869c-e208-11e6-8db4-c85b765b', '5ebd140c-e2b8-11e6-8db4-c85b765b', '2017-01-25 11:38:17', 'pendaftaran', 50000, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('b13f22f3-e82c-11e6-8db4-c85b765b', 'bb64ff04-e208-11e6-8db4-c85b765b', '8b47b7df-e82c-11e6-8db4-c85b765b', '2017-02-01 10:09:54', 'pendaftaran', 500000, 500000, 500000, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('c8d5e195-e36f-11e6-8db4-c85b765b', 'a6ae869c-e208-11e6-8db4-c85b765b', '5ebd140c-e2b8-11e6-8db4-c85b765b', '2017-01-26 09:28:57', 'resep', 215000, 220000, 5000, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('c9404281-e8bb-11e6-8db4-c85b765b', 'bb64ff04-e208-11e6-8db4-c85b765b', '9c794a55-e8bb-11e6-8db4-c85b765b', '2017-02-02 03:13:59', 'pendaftaran', 50000, 1000, 20000, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('ed3c7d25-e8c0-11e6-8db4-c85b765b', 'bb64ff04-e208-11e6-8db4-c85b765b', '244e33ca-e8be-11e6-8db4-c85b765b', '2017-02-02 03:50:47', 'resep', 30000, 40000, 10000, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('f1b97944-e7c5-11e6-8db4-c85b765b', 'a6ae869c-e208-11e6-8db4-c85b765b', 'c18620df-e7c5-11e6-8db4-c85b765b', '2017-01-31 21:54:31', 'pendaftaran', 50000, 50000, 50000, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

--
-- Triggers `pembayaran`
--
DELIMITER $$
CREATE TRIGGER `update_pembayaran` AFTER INSERT ON `pembayaran`
 FOR EACH ROW BEGIN
if new.tipe_pembayaran = 'pendaftaran' or new.tipe_pembayaran = 'tambahan'
then 
update pemeriksaan set status_pembayaran='1', mdf=now() where id_pendaftaran = NEW.id_pendaftaran and tipe = NEW.tipe_pembayaran;
ELSE
update resep set status_pembayaran='1', mdf=now() where id_pendaftaran = NEW.id_pendaftaran;
end if;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pemeriksaan`
--

CREATE TABLE IF NOT EXISTS `pemeriksaan` (
  `id_pemeriksaan` varchar(32) NOT NULL,
  `id_pendaftaran` varchar(32) NOT NULL,
  `tipe` enum('pendaftaran','tambahan') NOT NULL,
  `status_pembayaran` tinyint(1) DEFAULT NULL,
  `crt` datetime NOT NULL,
  `mdf` datetime NOT NULL,
  `dlt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemeriksaan`
--

INSERT INTO `pemeriksaan` (`id_pemeriksaan`, `id_pendaftaran`, `tipe`, `status_pembayaran`, `crt`, `mdf`, `dlt`) VALUES
('23fffd83-e2c5-11e6-8db4-c85b765b', '5ebd140c-e2b8-11e6-8db4-c85b765b', 'tambahan', 1, '0000-00-00 00:00:00', '2017-01-26 09:30:39', NULL),
('27152322-e2b8-11e6-8db4-c85b765b', '586a60e6-e2b5-11e6-8db4-c85b765b', 'tambahan', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('2ed9f363-e8be-11e6-8db4-c85b765b', '244e33ca-e8be-11e6-8db4-c85b765b', 'pendaftaran', 1, '2017-02-02 03:31:08', '2017-02-02 03:32:36', NULL),
('43dab057-e341-11e6-8db4-c85b765b', '3c8d7a2b-e341-11e6-8db4-c85b765b', 'pendaftaran', 1, '0000-00-00 00:00:00', '2017-01-27 19:30:08', NULL),
('62c193c2-e2b5-11e6-8db4-c85b765b', '586a60e6-e2b5-11e6-8db4-c85b765b', 'pendaftaran', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('6e4ed574-e2b8-11e6-8db4-c85b765b', '5ebd140c-e2b8-11e6-8db4-c85b765b', 'pendaftaran', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('92f54060-e82c-11e6-8db4-c85b765b', '8b47b7df-e82c-11e6-8db4-c85b765b', 'pendaftaran', 1, '0000-00-00 00:00:00', '2017-02-01 10:09:54', NULL),
('a5a48aa4-e8bb-11e6-8db4-c85b765b', '9c794a55-e8bb-11e6-8db4-c85b765b', 'pendaftaran', 1, '0000-00-00 00:00:00', '2017-02-02 03:13:59', NULL),
('cb0828f1-e7c5-11e6-8db4-c85b765b', 'c18620df-e7c5-11e6-8db4-c85b765b', 'pendaftaran', 1, '0000-00-00 00:00:00', '2017-01-31 21:54:31', NULL),
('f2238ce4-e7bf-11e6-8db4-c85b765b', 'caffbad2-e7bf-11e6-8db4-c85b765b', 'pendaftaran', 1, '0000-00-00 00:00:00', '2017-01-31 21:12:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pemeriksaan_detail`
--

CREATE TABLE IF NOT EXISTS `pemeriksaan_detail` (
  `id_p_detail` varchar(32) NOT NULL,
  `id_pemeriksaan` varchar(32) NOT NULL,
  `id_p_layanan` varchar(32) NOT NULL,
  `crt` datetime NOT NULL,
  `mdf` datetime NOT NULL,
  `dlt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemeriksaan_detail`
--

INSERT INTO `pemeriksaan_detail` (`id_p_detail`, `id_pemeriksaan`, `id_p_layanan`, `crt`, `mdf`, `dlt`) VALUES
('3581ad6d-e2b8-11e6-8db4-c85b765b', '27152322-e2b8-11e6-8db4-c85b765b', 'c93e9571-dfa7-11e6-8db4-c85b765b', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('3b2e07db-e2c5-11e6-8db4-c85b765b', '23fffd83-e2c5-11e6-8db4-c85b765b', '01ecb524-dfa1-11e6-8db4-c85b765b', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('4c9b0f08-e341-11e6-8db4-c85b765b', '43dab057-e341-11e6-8db4-c85b765b', 'c93e9571-dfa7-11e6-8db4-c85b765b', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('525c71cc-e8be-11e6-8db4-c85b765b', '2ed9f363-e8be-11e6-8db4-c85b765b', 'c93e9571-dfa7-11e6-8db4-c85b765b', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('76e1098d-e2b8-11e6-8db4-c85b765b', '6e4ed574-e2b8-11e6-8db4-c85b765b', 'c93e9571-dfa7-11e6-8db4-c85b765b', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('8b7db5c8-e2b5-11e6-8db4-c85b765b', '62c193c2-e2b5-11e6-8db4-c85b765b', 'c93e9571-dfa7-11e6-8db4-c85b765b', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('9a30f903-e82c-11e6-8db4-c85b765b', '92f54060-e82c-11e6-8db4-c85b765b', '01ece067-dfa1-11e6-8db4-c85b765b', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('b2f81955-e8bb-11e6-8db4-c85b765b', 'a5a48aa4-e8bb-11e6-8db4-c85b765b', 'c93e9571-dfa7-11e6-8db4-c85b765b', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('df2ba8fd-e7c5-11e6-8db4-c85b765b', 'cb0828f1-e7c5-11e6-8db4-c85b765b', '01ece067-dfa1-11e6-8db4-c85b765b', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('f95c5c0b-e7bf-11e6-8db4-c85b765b', 'f2238ce4-e7bf-11e6-8db4-c85b765b', '01ece067-dfa1-11e6-8db4-c85b765b', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pendaftaran`
--

CREATE TABLE IF NOT EXISTS `pendaftaran` (
  `id_pendaftaran` varchar(32) NOT NULL,
  `id_pasien` varchar(32) NOT NULL,
  `id_d_jadwal` varchar(32) NOT NULL,
  `tgl` datetime NOT NULL,
  `no_antrian` int(4) NOT NULL,
  `keluhan` text,
  `diagnosis` text,
  `crt` datetime NOT NULL,
  `mdf` datetime NOT NULL,
  `dlt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendaftaran`
--

INSERT INTO `pendaftaran` (`id_pendaftaran`, `id_pasien`, `id_d_jadwal`, `tgl`, `no_antrian`, `keluhan`, `diagnosis`, `crt`, `mdf`, `dlt`) VALUES
('244e33ca-e8be-11e6-8db4-c85b765b', 'ham282fb-dfa7-11e6-8db4-c85b765b', 'suryakam-dfa4-11e6-8db4-c85b765b', '2017-02-02 03:30:51', 1, NULL, NULL, '2017-02-02 03:30:51', '2017-02-02 03:30:51', NULL),
('2ae11470-e48f-11e6-8db4-c85b765b', 'bal2c841-dfa7-11e6-8db4-c85b765b', 'grahamse-dfa3-11e6-8db4-c85b765b', '2017-01-27 19:45:33', 1, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('3c8d7a2b-e341-11e6-8db4-c85b765b', 'bal2c841-dfa7-11e6-8db4-c85b765b', 'suryajum-dfa4-11e6-8db4-c85b765b', '2017-01-26 03:56:27', 1, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('586a60e6-e2b5-11e6-8db4-c85b765b', 'ham282fb-dfa7-11e6-8db4-c85b765b', 'grahamju-dfa3-11e6-8db4-c85b765b', '2017-01-25 11:15:11', 1, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('5ebd140c-e2b8-11e6-8db4-c85b765b', 'bal2c841-dfa7-11e6-8db4-c85b765b', 'suryakam-dfa4-11e6-8db4-c85b765b', '2017-01-25 11:36:50', 2, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('8b47b7df-e82c-11e6-8db4-c85b765b', 'ham282fb-dfa7-11e6-8db4-c85b765b', 'tarirabu-dfa3-11e6-8db4-c85b765b', '2017-02-01 10:08:50', 1, NULL, NULL, '2017-02-01 10:08:50', '2017-02-01 10:08:50', NULL),
('9c794a55-e8bb-11e6-8db4-c85b765b', 'bal2c841-dfa7-11e6-8db4-c85b765b', 'suryakam-dfa4-11e6-8db4-c85b765b', '2017-02-02 03:12:44', 1, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('c18620df-e7c5-11e6-8db4-c85b765b', 'ham282fb-dfa7-11e6-8db4-c85b765b', 'tarikami-dfa3-11e6-8db4-c85b765b', '2017-01-31 21:53:10', 1, NULL, NULL, '2017-01-31 21:53:10', '2017-01-31 21:53:10', NULL),
('caffbad2-e7bf-11e6-8db4-c85b765b', 'bal2c841-dfa7-11e6-8db4-c85b765b', 'suryarab-dfa4-11e6-8db4-c85b765b', '2017-01-31 00:00:00', 1, NULL, NULL, '2017-01-31 21:10:29', '2017-01-31 21:10:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `poliklinik`
--

CREATE TABLE IF NOT EXISTS `poliklinik` (
  `id_poliklinik` varchar(32) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `crt` datetime NOT NULL,
  `mdf` datetime NOT NULL,
  `dlt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poliklinik`
--

INSERT INTO `poliklinik` (`id_poliklinik`, `nama`, `crt`, `mdf`, `dlt`) VALUES
('71c90bb7-dfa0-11e6-8db4-c85b765b', 'poli gigi', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('71c95658-dfa0-11e6-8db4-c85b765b', 'poli umum', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('b2f898de-e48f-11e6-8db4-c85b765b', 'poli anak', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `poliklinik_jadwal`
--

CREATE TABLE IF NOT EXISTS `poliklinik_jadwal` (
  `id_p_jadwal` varchar(32) NOT NULL,
  `id_poliklinik` varchar(32) NOT NULL,
  `hari` varchar(255) NOT NULL,
  `jam_buka` time NOT NULL,
  `jam_tutup` time NOT NULL,
  `crt` datetime NOT NULL,
  `mdf` datetime NOT NULL,
  `dlt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poliklinik_jadwal`
--

INSERT INTO `poliklinik_jadwal` (`id_p_jadwal`, `id_poliklinik`, `hari`, `jam_buka`, `jam_tutup`, `crt`, `mdf`, `dlt`) VALUES
('08e212f7-dfa6-11e6-8db4-c85b765b', '71c90bb7-dfa0-11e6-8db4-c85b765b', 'selasa', '09:00:00', '15:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('08e239fc-dfa6-11e6-8db4-c85b765b', '71c90bb7-dfa0-11e6-8db4-c85b765b', 'rabu', '07:00:00', '15:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('08e25baa-dfa6-11e6-8db4-c85b765b', '71c90bb7-dfa0-11e6-8db4-c85b765b', 'kamis', '09:00:00', '15:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('08e27a6a-dfa6-11e6-8db4-c85b765b', '71c90bb7-dfa0-11e6-8db4-c85b765b', 'jumat', '09:00:00', '15:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('08e297cc-dfa6-11e6-8db4-c85b765b', '71c90bb7-dfa0-11e6-8db4-c85b765b', 'sabtu', '09:00:00', '12:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('08e2b6c8-dfa6-11e6-8db4-c85b765b', '71c95658-dfa0-11e6-8db4-c85b765b', 'selasa', '07:00:00', '20:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('08e2ddd1-dfa6-11e6-8db4-c85b765b', '71c95658-dfa0-11e6-8db4-c85b765b', 'rabu', '07:00:00', '20:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('08e32342-dfa6-11e6-8db4-c85b765b', '71c95658-dfa0-11e6-8db4-c85b765b', 'kamis', '07:00:00', '20:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('08e34d14-dfa6-11e6-8db4-c85b765b', '71c95658-dfa0-11e6-8db4-c85b765b', 'jumat', '07:00:00', '20:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('08e370ba-dfa6-11e6-8db4-c85b765b', '71c95658-dfa0-11e6-8db4-c85b765b', 'sabtu', '07:00:00', '17:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('a7e76284-dfa0-11e6-8db4-c85b765b', '71c90bb7-dfa0-11e6-8db4-c85b765b', 'senin', '09:00:00', '15:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('a7e7829d-dfa0-11e6-8db4-c85b765b', '71c90bb7-dfa0-11e6-8db4-c85b765b', 'sabtu', '09:00:00', '12:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('cf7b3382-dfa0-11e6-8db4-c85b765b', '71c95658-dfa0-11e6-8db4-c85b765b', 'senin', '07:00:00', '20:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('cf7b51c4-dfa0-11e6-8db4-c85b765b', '71c95658-dfa0-11e6-8db4-c85b765b', 'sabtu', '07:00:00', '17:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `poliklinik_layanan`
--

CREATE TABLE IF NOT EXISTS `poliklinik_layanan` (
  `id_p_layanan` varchar(32) NOT NULL,
  `id_poliklinik` varchar(32) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tarif` double NOT NULL,
  `crt` datetime NOT NULL,
  `mdf` datetime NOT NULL,
  `dlt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poliklinik_layanan`
--

INSERT INTO `poliklinik_layanan` (`id_p_layanan`, `id_poliklinik`, `nama`, `tarif`, `crt`, `mdf`, `dlt`) VALUES
('01ecb524-dfa1-11e6-8db4-c85b765b', '71c95658-dfa0-11e6-8db4-c85b765b', 'cek gula darah', 10000, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('01ece067-dfa1-11e6-8db4-c85b765b', '71c95658-dfa0-11e6-8db4-c85b765b', 'foto x-ray', 100000, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('c93e9571-dfa7-11e6-8db4-c85b765b', '71c90bb7-dfa0-11e6-8db4-c85b765b', 'pemeriksaan', 50000, '2017-01-21 14:01:48', '2017-01-21 14:01:48', NULL),
('c93ecc58-dfa7-11e6-8db4-c85b765b', '71c95658-dfa0-11e6-8db4-c85b765b', 'pemeriksaan', 50000, '2017-01-21 14:01:48', '2017-01-21 14:01:48', NULL),
('e7f5b860-dfa0-11e6-8db4-c85b765b', '71c90bb7-dfa0-11e6-8db4-c85b765b', 'cabut gigi', 50000, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('e7f5cfe0-dfa0-11e6-8db4-c85b765b', '71c90bb7-dfa0-11e6-8db4-c85b765b', 'tambal gigi', 150000, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `resep`
--

CREATE TABLE IF NOT EXISTS `resep` (
  `id_resep` varchar(32) NOT NULL,
  `id_pendaftaran` varchar(32) NOT NULL,
  `tgl` datetime NOT NULL,
  `total` double NOT NULL,
  `status_pembayaran` tinyint(1) DEFAULT NULL,
  `crt` datetime NOT NULL,
  `mdf` datetime NOT NULL,
  `dlt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resep`
--

INSERT INTO `resep` (`id_resep`, `id_pendaftaran`, `tgl`, `total`, `status_pembayaran`, `crt`, `mdf`, `dlt`) VALUES
('88d730e5-e8c0-11e6-8db4-c85b765b', '244e33ca-e8be-11e6-8db4-c85b765b', '2017-02-02 03:47:58', 30000, 1, '0000-00-00 00:00:00', '2017-02-02 03:50:47', NULL),
('f7dde933-e2ba-11e6-8db4-c85b765b', '5ebd140c-e2b8-11e6-8db4-c85b765b', '2017-01-25 11:55:26', 200000, 1, '0000-00-00 00:00:00', '2017-01-26 09:28:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `resep_detail`
--

CREATE TABLE IF NOT EXISTS `resep_detail` (
  `id_r_detail` varchar(32) NOT NULL,
  `id_resep` varchar(32) NOT NULL,
  `id_obat` varchar(32) NOT NULL,
  `dosis` varchar(255) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `subtotal` double NOT NULL,
  `crt` datetime NOT NULL,
  `mdf` datetime NOT NULL,
  `dlt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resep_detail`
--

INSERT INTO `resep_detail` (`id_r_detail`, `id_resep`, `id_obat`, `dosis`, `jumlah`, `subtotal`, `crt`, `mdf`, `dlt`) VALUES
('1c8d69f5-e2bb-11e6-8db4-c85b765b', 'f7dde933-e2ba-11e6-8db4-c85b765b', '7bf90e74-dfa1-11e6-8db4-c85b765b', '3x/1 hari', 1, 200000, '2017-01-25 11:56:27', '2017-01-25 11:56:27', NULL),
('58163ed9-e36d-11e6-8db4-c85b765b', 'f7dde933-e2ba-11e6-8db4-c85b765b', '7bf93361-dfa1-11e6-8db4-c85b765b', '3x/1hari', 1, 15000, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('6a6370c9-e493-11e6-8db4-c85b765b', 'f7dde933-e2ba-11e6-8db4-c85b765b', '7bf90e74-dfa1-11e6-8db4-c85b765b', '3x/1hari', 10, 2000, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('bcbcae90-e8c0-11e6-8db4-c85b765b', '88d730e5-e8c0-11e6-8db4-c85b765b', '7bf93361-dfa1-11e6-8db4-c85b765b', '2x/1 hari', 2, 30000, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

--
-- Triggers `resep_detail`
--
DELIMITER $$
CREATE TRIGGER `insert_resep_detail` AFTER INSERT ON `resep_detail`
 FOR EACH ROW BEGIN
update obat set stok = stok-new.jumlah, mdf=now() where id_obat=new.id_obat;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_resep_detail` AFTER UPDATE ON `resep_detail`
 FOR EACH ROW BEGIN
IF OLD.jumlah < NEW.jumlah
THEN
update obat set stok = (stok - (NEW.jumlah-OLD.jumlah)), mdf = now() where id_obat = NEW.id_obat;
ELSEIF OLD.jumlah > NEW.jumlah
THEN
update obat set stok = (stok + (OLD.jumlah-NEW.jumlah)), mdf = now() where id_obat = NEW.id_obat;
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_antrian`
--
CREATE TABLE IF NOT EXISTS `v_antrian` (
`id_poliklinik` varchar(32)
,`nama` varchar(255)
,`no_antrian` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_laporan_obat`
--
CREATE TABLE IF NOT EXISTS `v_laporan_obat` (
`id_obat` varchar(32)
,`nama` varchar(255)
,`tgl` datetime
,`jenis` varchar(6)
,`jumlah` int(11)
,`total` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_pendaftaran`
--
CREATE TABLE IF NOT EXISTS `v_pendaftaran` (
`id_pasien` varchar(32)
,`nama` varchar(510)
,`id_pendaftaran` varchar(32)
,`tgl` date
,`poliklinik` varchar(255)
,`dokter` varchar(255)
,`status_ambil_resep` int(1)
);

-- --------------------------------------------------------

--
-- Structure for view `v_antrian`
--
DROP TABLE IF EXISTS `v_antrian`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_antrian` AS select `poliklinik`.`id_poliklinik` AS `id_poliklinik`,`poliklinik`.`nama` AS `nama`,count(`pendaftaran`.`id_pendaftaran`) AS `no_antrian` from (((`poliklinik` join `dokter` on((`dokter`.`id_poliklinik` = `poliklinik`.`id_poliklinik`))) join `dokter_jadwal` on((`dokter_jadwal`.`id_dokter` = `dokter`.`id_dokter`))) left join `pendaftaran` on((`dokter_jadwal`.`id_d_jadwal` = `pendaftaran`.`id_d_jadwal`))) where (cast(`pendaftaran`.`tgl` as date) = date_format(now(),'%y-%m-%d')) group by `poliklinik`.`id_poliklinik`;

-- --------------------------------------------------------

--
-- Structure for view `v_laporan_obat`
--
DROP TABLE IF EXISTS `v_laporan_obat`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_laporan_obat` AS select `resep_detail`.`id_obat` AS `id_obat`,`obat`.`nama` AS `nama`,`resep_detail`.`crt` AS `tgl`,'keluar' AS `jenis`,`resep_detail`.`jumlah` AS `jumlah`,`resep_detail`.`subtotal` AS `total` from (`resep_detail` join `obat` on((`obat`.`id_obat` = `resep_detail`.`id_obat`))) where (isnull(`resep_detail`.`dlt`) and isnull(`obat`.`dlt`)) union select `obat_pemasokan`.`id_obat` AS `id_obat`,`obat`.`nama` AS `nama`,`obat_pemasokan`.`tgl` AS `tgl`,'masuk' AS `jenis`,`obat_pemasokan`.`jumlah` AS `jumlah`,`obat_pemasokan`.`total` AS `total` from (`obat_pemasokan` join `obat` on((`obat`.`id_obat` = `obat_pemasokan`.`id_obat`))) where (isnull(`obat_pemasokan`.`dlt`) and isnull(`obat`.`dlt`));

-- --------------------------------------------------------

--
-- Structure for view `v_pendaftaran`
--
DROP TABLE IF EXISTS `v_pendaftaran`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_pendaftaran` AS select `pasien`.`id_pasien` AS `id_pasien`,concat(`pasien`.`nama_depan`,`pasien`.`nama_belakang`) AS `nama`,`pendaftaran`.`id_pendaftaran` AS `id_pendaftaran`,cast(`pendaftaran`.`tgl` as date) AS `tgl`,`poliklinik`.`nama` AS `poliklinik`,`dokter`.`nama` AS `dokter`,if((count(`pemeriksaan`.`id_pemeriksaan`) = sum(`pemeriksaan`.`status_pembayaran`)),1,0) AS `status_ambil_resep` from (((((((((`pasien` join `pendaftaran` on((`pendaftaran`.`id_pasien` = `pasien`.`id_pasien`))) join `dokter_jadwal` on((`dokter_jadwal`.`id_d_jadwal` = `pendaftaran`.`id_d_jadwal`))) join `dokter` on((`dokter`.`id_dokter` = `dokter_jadwal`.`id_dokter`))) join `poliklinik` on((`poliklinik`.`id_poliklinik` = `dokter`.`id_poliklinik`))) join `pemeriksaan` on((`pemeriksaan`.`id_pendaftaran` = `pendaftaran`.`id_pendaftaran`))) join `pemeriksaan_detail` on((`pemeriksaan_detail`.`id_pemeriksaan` = `pemeriksaan`.`id_pemeriksaan`))) join `poliklinik_layanan` on((`poliklinik_layanan`.`id_p_layanan` = `pemeriksaan_detail`.`id_p_layanan`))) left join `resep` on((`resep`.`id_pendaftaran` = `pendaftaran`.`id_pendaftaran`))) left join `resep_detail` on((`resep_detail`.`id_resep` = `resep`.`id_resep`))) where isnull(`pasien`.`dlt`) group by `pendaftaran`.`id_pendaftaran`;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`),
  ADD KEY `id_c_jenis_admin` (`id_c_kat_admin`);

--
-- Indexes for table `config_jenis_obat`
--
ALTER TABLE `config_jenis_obat`
  ADD PRIMARY KEY (`id_c_jenis_obat`);

--
-- Indexes for table `config_kat_admin`
--
ALTER TABLE `config_kat_admin`
  ADD PRIMARY KEY (`id_c_kat_admin`);

--
-- Indexes for table `config_kat_obat`
--
ALTER TABLE `config_kat_obat`
  ADD PRIMARY KEY (`id_c_kat_obat`);

--
-- Indexes for table `config_spesialis`
--
ALTER TABLE `config_spesialis`
  ADD PRIMARY KEY (`id_c_spesialis`);

--
-- Indexes for table `dokter`
--
ALTER TABLE `dokter`
  ADD PRIMARY KEY (`id_dokter`),
  ADD KEY `id_poliklinik` (`id_poliklinik`),
  ADD KEY `id_c_spesialis` (`id_c_spesialis`);

--
-- Indexes for table `dokter_jadwal`
--
ALTER TABLE `dokter_jadwal`
  ADD PRIMARY KEY (`id_d_jadwal`),
  ADD KEY `id_dokter` (`id_dokter`);

--
-- Indexes for table `obat`
--
ALTER TABLE `obat`
  ADD PRIMARY KEY (`id_obat`),
  ADD KEY `id_c_jenis_obat` (`id_c_jenis_obat`),
  ADD KEY `id_c_kat_obat` (`id_c_kat_obat`);

--
-- Indexes for table `obat_pemasokan`
--
ALTER TABLE `obat_pemasokan`
  ADD PRIMARY KEY (`id_pemasokan`),
  ADD KEY `id_obat` (`id_obat`),
  ADD KEY `id_admin` (`id_admin`);

--
-- Indexes for table `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`id_pasien`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`),
  ADD KEY `id_admin` (`id_admin`),
  ADD KEY `id_pendaftaran` (`id_pendaftaran`);

--
-- Indexes for table `pemeriksaan`
--
ALTER TABLE `pemeriksaan`
  ADD PRIMARY KEY (`id_pemeriksaan`),
  ADD KEY `id_pendaftaran` (`id_pendaftaran`);

--
-- Indexes for table `pemeriksaan_detail`
--
ALTER TABLE `pemeriksaan_detail`
  ADD PRIMARY KEY (`id_p_detail`),
  ADD KEY `id_pemeriksaan` (`id_pemeriksaan`),
  ADD KEY `id_p_layanan` (`id_p_layanan`);

--
-- Indexes for table `pendaftaran`
--
ALTER TABLE `pendaftaran`
  ADD PRIMARY KEY (`id_pendaftaran`),
  ADD KEY `id_pasien` (`id_pasien`),
  ADD KEY `id_d_jadwal` (`id_d_jadwal`);

--
-- Indexes for table `poliklinik`
--
ALTER TABLE `poliklinik`
  ADD PRIMARY KEY (`id_poliklinik`);

--
-- Indexes for table `poliklinik_jadwal`
--
ALTER TABLE `poliklinik_jadwal`
  ADD PRIMARY KEY (`id_p_jadwal`),
  ADD KEY `id_poliklinik` (`id_poliklinik`);

--
-- Indexes for table `poliklinik_layanan`
--
ALTER TABLE `poliklinik_layanan`
  ADD PRIMARY KEY (`id_p_layanan`),
  ADD KEY `id_poliklinik` (`id_poliklinik`);

--
-- Indexes for table `resep`
--
ALTER TABLE `resep`
  ADD PRIMARY KEY (`id_resep`),
  ADD KEY `id_pemeriksaan` (`id_pendaftaran`),
  ADD KEY `status_pembayaran` (`status_pembayaran`);

--
-- Indexes for table `resep_detail`
--
ALTER TABLE `resep_detail`
  ADD PRIMARY KEY (`id_r_detail`),
  ADD KEY `id_resep` (`id_resep`),
  ADD KEY `id_obat` (`id_obat`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`id_c_kat_admin`) REFERENCES `config_kat_admin` (`id_c_kat_admin`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `dokter`
--
ALTER TABLE `dokter`
  ADD CONSTRAINT `dokter_ibfk_1` FOREIGN KEY (`id_poliklinik`) REFERENCES `poliklinik` (`id_poliklinik`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `dokter_ibfk_2` FOREIGN KEY (`id_c_spesialis`) REFERENCES `config_spesialis` (`id_c_spesialis`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `dokter_jadwal`
--
ALTER TABLE `dokter_jadwal`
  ADD CONSTRAINT `dokter_jadwal_ibfk_1` FOREIGN KEY (`id_dokter`) REFERENCES `dokter` (`id_dokter`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `obat`
--
ALTER TABLE `obat`
  ADD CONSTRAINT `obat_ibfk_1` FOREIGN KEY (`id_c_kat_obat`) REFERENCES `config_kat_obat` (`id_c_kat_obat`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `obat_ibfk_2` FOREIGN KEY (`id_c_jenis_obat`) REFERENCES `config_jenis_obat` (`id_c_jenis_obat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `obat_pemasokan`
--
ALTER TABLE `obat_pemasokan`
  ADD CONSTRAINT `obat_pemasokan_ibfk_1` FOREIGN KEY (`id_obat`) REFERENCES `obat` (`id_obat`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `obat_pemasokan_ibfk_2` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id_admin`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD CONSTRAINT `pembayaran_ibfk_4` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id_admin`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pembayaran_ibfk_5` FOREIGN KEY (`id_pendaftaran`) REFERENCES `pendaftaran` (`id_pendaftaran`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pemeriksaan`
--
ALTER TABLE `pemeriksaan`
  ADD CONSTRAINT `pemeriksaan_ibfk_1` FOREIGN KEY (`id_pendaftaran`) REFERENCES `pendaftaran` (`id_pendaftaran`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pemeriksaan_detail`
--
ALTER TABLE `pemeriksaan_detail`
  ADD CONSTRAINT `pemeriksaan_detail_ibfk_1` FOREIGN KEY (`id_pemeriksaan`) REFERENCES `pemeriksaan` (`id_pemeriksaan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pemeriksaan_detail_ibfk_2` FOREIGN KEY (`id_p_layanan`) REFERENCES `poliklinik_layanan` (`id_p_layanan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pendaftaran`
--
ALTER TABLE `pendaftaran`
  ADD CONSTRAINT `pendaftaran_ibfk_1` FOREIGN KEY (`id_pasien`) REFERENCES `pasien` (`id_pasien`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pendaftaran_ibfk_2` FOREIGN KEY (`id_d_jadwal`) REFERENCES `dokter_jadwal` (`id_d_jadwal`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `poliklinik_jadwal`
--
ALTER TABLE `poliklinik_jadwal`
  ADD CONSTRAINT `poliklinik_jadwal_ibfk_1` FOREIGN KEY (`id_poliklinik`) REFERENCES `poliklinik` (`id_poliklinik`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `poliklinik_layanan`
--
ALTER TABLE `poliklinik_layanan`
  ADD CONSTRAINT `poliklinik_layanan_ibfk_1` FOREIGN KEY (`id_poliklinik`) REFERENCES `poliklinik` (`id_poliklinik`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `resep`
--
ALTER TABLE `resep`
  ADD CONSTRAINT `resep_ibfk_2` FOREIGN KEY (`id_pendaftaran`) REFERENCES `pendaftaran` (`id_pendaftaran`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `resep_detail`
--
ALTER TABLE `resep_detail`
  ADD CONSTRAINT `resep_detail_ibfk_1` FOREIGN KEY (`id_resep`) REFERENCES `resep` (`id_resep`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `resep_detail_ibfk_2` FOREIGN KEY (`id_obat`) REFERENCES `obat` (`id_obat`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
