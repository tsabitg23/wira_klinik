<!DOCTYPE html>
<html lang="en">

<head>
  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="assets/img/logo/icon.png">
	<title>Login - Wira</title>

    <!-- Bootstrap core CSS -->
    <?=css('bootstrap/bootstrap.css')?>

    <?=css('fontawesome/font-awesome.css');?>
    <!-- Fonts  -->
    <?=css('font/roboto.css')?>

    <!-- Main Style  -->
    <?=css('main/main.css')?>

</head>
<body onload="focus()">	
    
	
    <div class="container" style="margin-top:3%;">
    	<div class="row">
    	<div class="col-lg-4 col-lg-offset-4">
        	<h3 class="text-center"><?=img('logo/logoblue.png', array('style'=>'width:100%;'))?></h3>
            <p class="text-center">Login untuk mengakses Aplikasi   </p>
            <hr class="clean">

              <?php echo form_open($action, 'id="frmlogin" onsubmit= "return validateForm();"');?>
              <div class="form-group input-group">
                <span class="input-group-addon"><i class="fa fa-vcard-o"></i></span>
                <?php echo form_input('username','','class="form-control" placeholder="Username" id="username"')?>
              </div>
              <div class="form-group input-group" style="margin-top:-10px;">
              </div>
              <div class="form-group input-group">
              	<span class="input-group-addon"><i class="fa fa-key"></i></span>
                <?php echo form_password('password','','class="form-control" placeholder="Password" id="password"')?>
              </div>
        	  <button type="submit" class="btn btn-primary  btn-block">Sign in</button>
            
            <?php echo form_close();?>
            <hr>
            
            <p class="text-center text-gray">Hubungi Admin untuk menambah akun<br>Copyright &copy; 2017 Tsabit Ghazwan</p>
        </div>
        </div>
    </div>
    
    <script type="text/javascript">
      function validateForm() {
        if($('#username').val() == "" || $('#username').val() == null) {
          alert("Mohon isi form username");
          return false;
          
        }
        if($('#password').val() == "" || $('#password').val() == null) {
            alert("Mohon isi form password");
            return false;  
        }
        return true;
      }

      function focus(){
        $('#username').focus();
      }
    </script>
    
    <!-- Bootstrap -->
    <?=js('jquery/jquery.min.js')?>
    <?=js('bootstrap/bootstrap.min.js')?>
</body>
</html>
