<?php
class Login_model extends CI_Model {

    function cekLogin($username,$password){
           $this->db->select('a.*,b.nama as level');
           $this->db->from('admin a');
           $this->db->join('config_kat_admin b','b.id_c_kat_admin = a.id_c_kat_admin','inner');
           $this->db->where('a.username', $username);
           $this->db->where('a.password', $password);
           $this->db->limit(1);
           $query = $this->db->get();
               if($query->num_rows() == 1){
                 return $query->result();
               }
               else{
                 return false; 
               }
    }

    function getId($keys = "") {
      $this->db->from('admin');
      $this->db->select("id_admin");
      $this->db->where("username", $keys);
      return $this->db->get()->row()->id_admin;
    }


    
}
?>