<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MX_Controller {

	private $_module;

	public function __construct() {
        parent::__construct();

        //set Module location
        $this->_module = 'login';
        // set library

        $this->load->model('login_model','lm');
        $this->load->helper('form');
	    $this->load->library('form_validation');
	    $this->load->library('session');
 
    }

    public function cekstatuslogin() {
    	if ($this->session->has_userdata('uname') == true) {
			if($this->session->userdata('level') == "admin")
				redirect(base_url('/admin'));
			elseif($this->session->userdata('level') == "asisten_dokter")
				redirect(base_url('/asisten_dokter'));
			elseif($this->session->userdata('level') == "registrasi")
				redirect(base_url('/registrasi'));
			elseif($this->session->userdata('level') == "apoteker")
				redirect(base_url('/apoteker'));
		}
    }

	public function index()
	{
		$this->cekstatuslogin();
		$data['action']	= $this->_module.'/process';
		$this->load->view($this->_module.'/index',$data);
	}

	public function process(){
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if ($this->form_validation->run() == FALSE) {
			redirect(base_url('/login'));
		}
		$uname 	= $this->input->post('username');
		$psw 	= md5($this->input->post('password'));
		$result = $this->lm->cekLogin($uname,$psw);
		
			if ($result) {
				$session['uname'] = $this->input->post('username');
				$session['level'] = $result[0]->level;
				$session['nama'] = $result[0]->nama;
				$session['id'] = $this->lm->getId($this->input->post('username'));

				$this->session->set_userdata($session);
				redirect(base_url('/'.$this->input->post('level')));
			}
			else {
				echo "<script>
	  					alert('username / Password yang anda masukkan salah');
						window.location.href='".base_url('/login/').
					  "'</script>";
					  
			}
	}

	public function logout()
	{
		$destroy = $this->session->sess_destroy();
		redirect(base_url());
	}


}
?>
