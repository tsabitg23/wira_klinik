<?php

class pasien_model extends CI_Model{
    private $_table           = 'pasien';
    private $_table1           = 'poliklinik';
    private $_table2           = 'poliklinik_layanan';
    private $_table3          = 'v_pendaftaran';
    private $_table4          = 'pendaftaran';
    protected $primary_key  = 'id_pasien';

    public function save_as_new($data) {
        $this->db->set('crt','now()',false);
        $this->db->set('mdf','now()',false);
        $this->db->insert($this->_table, $data);
    }

    // untuk update data
    public function save($data, $key) {
        $this->db->set('mdf','now()',false);
        $this->db->update($this->_table, $data, array($this->primary_key => $key));
    }

    // untuk update data
    public function delete($key) {
        $this->db->set('dlt','now()',false);
        $this->db->where(array($this->primary_key => $key));
        $this->db->update($this->_table);
    }

    public function getData($key    = ""){
        $this->db->select("*");
        $this->db->where('dlt',null);
        $this->db->from($this->_table);

        if($key != "")
            $this->db->where($this->primary_key,$key);

        return $this->db->get();
    }

    public function optionPasien($default = '-- Pilih Pasien --',$key = '') {
        $option = array();
        $this->db->where('dlt',null);
        $this->db->from($this->_table);

        if(!empty($key)){
            $this->db->where($this->primary_key,$key);
        }
        else{
            if (!empty($default))
                $option['kosong'] = $default;
        }
        
        $list   = $this->db->get();


        foreach ($list->result() as $row) {
            $option[$row->id_pasien] = $row->id_pasien;
        }

        return $option;
    }

    public function optionPoliklinik($default = '-- Pilih Poliklinik --',$key = '') {
        $option = array();
        $this->db->where('dlt',null);
        $this->db->from($this->_table1);

        if(!empty($key)){
            $this->db->where($this->primary_key,$key);
        }

        
        $list   = $this->db->get();
            if (!empty($default))
                $option['kosong'] = $default;


        foreach ($list->result() as $row) {
            $option[$row->id_poliklinik] = $row->nama;
        }

        return $option;
    }

    public function getNama($key = ''){
        $this->db->select("a.nama_depan,a.nama_belakang");
        $this->db->where('a.dlt',null);
        $this->db->from($this->_table.' a');

        if($key != "")
            $this->db->where('id_pasien',$key);

        return $this->db->get()->row();   
    }

    public function layanan($id_poli){
          $this->db->select("a.*");
          $this->db->where('a.dlt',null);
          $this->db->where('a.id_poliklinik',$id_poli);
          $this->db->from($this->_table2.' a');

          $query = $this->db->get();
          $data = "<option value=''>- Pilih Layanan -</option>";
          foreach ($query->result() as $value) {
              $data .= "<option value='".$value->id_p_layanan."'>".$value->nama."</option>";
          }
          echo $data;
    }

    public function getHarga($key = ''){
        $this->db->select("a.tarif");
        $this->db->where('a.dlt',null);
        $this->db->from($this->_table2.' a');

        if($key != "")
            $this->db->where('id_p_layanan',$key);

        return $this->db->get();   
    }

    function cekUser($id){
           $this->db->select('a.*');
           $this->db->from('pasien a');
           $this->db->where('a.id_pasien', $id);
           $this->db->limit(1);
           $query = $this->db->get();
               if($query->num_rows() == 1){
                 return $query->result();
               }
               else{
                 return false; 
               }
    }

    public function getDataDaftar($key){
        $this->db->select("a.*,b.status_pembayaran");
        $this->db->where('a.id_pasien',$key);
        $this->db->order_by('a.tgl','desc');
        $this->db->join('resep b','b.id_pendaftaran = a.id_pendaftaran','left');
        $this->db->from($this->_table3.' a');

        return $this->db->get();
    }

    public function checkResep($id_daftar){
        $this->db->select("resep.id_resep as resep,ifnull(resep.status_pembayaran,0) as status_resep,v_pendaftaran.status_ambil_resep");
        $this->db->where('pendaftaran.id_pendaftaran',$id_daftar);
        $this->db->join('resep ','resep.id_pendaftaran = pendaftaran.id_pendaftaran','left');
        $this->db->join('v_pendaftaran ','v_pendaftaran.id_pendaftaran = pendaftaran.id_pendaftaran','inner');
        $this->db->from($this->_table4);

        return $this->db->get();
    }

    public function getPendaftaran($key    = ""){
        $this->db->select("a.*");
        $this->db->from($this->_table3.' a');

        if($key != "")
            $this->db->where('a.id_pendaftaran',$key);

        return $this->db->get();
    }

    public function getDataPendaftaran($key    = ""){
        $this->db->select("*");
        $this->db->from($this->_table4);

        if($key != "")
            $this->db->where('id_pendaftaran',$key);

        return $this->db->get();
    }

    public function getPendaftaranDetail($id_pendaftaran){
        $query = $this->db->query("CALL pendaftaran_detail('".$id_pendaftaran."')");
        return $query->result();
    }

    public function getPemeriksaan($key    = ""){
        $this->db->select("tipe,status_pembayaran");
        $this->db->from('pemeriksaan');

        if($key != "")
            $this->db->where('id_pendaftaran',$key);

        return $this->db->get();
    }

    public function searchByName($nama){
        $this->db->select('id_pasien,nama_depan,nama_belakang');
        $this->db->where("concat(nama_depan,' ',nama_belakang) like '%".$nama."%'");
        $this->db->from($this->_table);
        return $this->db->get();
    }




}