<?php

class pendaftaran_model extends CI_Model{
    private $_table           = 'pendaftaran';
    private $_table1           = 'pemeriksaan';
    private $_table2           = 'pemeriksaan_detail';
    protected $primary_key  = 'id_pendaftaran';

    public function save_as_new($data,$id_poliklinik) {
        $this->db->set('crt','now()',false);
        $this->db->set('mdf','now()',false);
        $this->db->set('tgl','now()',false);
        $this->db->set('no_antrian',"no_antrian('".$id_poliklinik."')",false);
        $this->db->set('id_d_jadwal',"pilih_dokter('".$id_poliklinik."')",false);
        $this->db->insert($this->_table, $data);
    }

    public function save_as_new_pemeriksaan($data) {
        $this->db->set('crt','now()',false);
        $this->db->set('mdf','now()',false);
        $this->db->insert($this->_table1, $data);
    }

    public function save_as_new_pemeriksaan_detail($data) {
        $this->db->set('crt','now()',false);
        $this->db->set('mdf','now()',false);
        $this->db->set('id_p_detail','uuid()',false);
        $this->db->insert($this->_table2, $data);
    }

    // untuk update data
    public function delete($key) {
        $this->db->set('dlt','now()',false);
        $this->db->where(array($this->primary_key => $key));
        $this->db->update($this->_table);
    }

    public function getData($key    = ""){
        $this->db->select("*");
        $this->db->where('dlt',null);
        $this->db->from($this->_table);

        if($key != "")
            $this->db->where($this->primary_key,$key);

        return $this->db->get();
    }

    public function optionPendaftaran($default = '-- Pilih Pendaftaran --' ,$id_pasien,$key = '') {
        $option = array();
        $this->db->where('dlt',null);
        $this->db->where('id_pasien',$id_pasien);
        $this->db->from($this->_table);

        if(!empty($key))
            $this->db->where('id_pendaftaran',$key);

        $list   = $this->db->get();

        if (!empty($default))
            $option['kosong'] = $default;

        foreach ($list->result() as $row) {
            $option[$row->id_pendaftaran] = $row->id_pendaftaran;
        }

        return $option;
    }

    public function pendaftarannya($id_pasien,$key = ''){
          $this->db->select("a.*");
          $this->db->where('a.id_pasien',$id_pasien);
          $this->db->where('a.status_ambil_resep','0');
          $this->db->from('v_pendaftaran a');

          if(!empty($key))
            $this->db->where('id_pendaftaran',$key);

          $query = $this->db->get();
          $data = "<option value=''>-Pilih Pendaftaran-</option>";
          foreach ($query->result() as $value) {
              $data .= "<option value='".$value->id_pendaftaran."'>".$value->id_pendaftaran."</option>";
          }
          echo $data;
    }

    

}