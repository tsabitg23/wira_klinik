<head>
    <title>Pembayaran</title>
</head>
          
<div class="warper container-fluid">
<ol class="breadcrumb">
    <li>Registrasi</li>
    <li class="active">Pembayaran</li>
</ol>
<div class="page-header"><h1>Form Pembayaran Tambahan</h1></div>

<div class="row">
	<div class="col-md-12">
	<div class="panel panel-default">
	    <div class="panel-heading">Input Pembayaran Tambahan</div>
	    <div class="panel-body">
		<?php 
		$attrib = array('class' => 'form-horizontal');
		$hidden = array('id' => '');
		echo form_open($action,$attrib,$hidden);?>
		<div class="form-group">
			<label for="id_pasien" class="col-sm-2 control-label">ID Pasien</label>
			<div class="col-sm-5">
				<?php 
				echo form_dropdown('id_pasien',$pasien,!empty($default->id_pasien) ? $default->id_pasien : '','class="form-control choose-select" id="id_pasien"');
				?>
			</div>
		</div>
		<div class="form-group">
			<label for="id_pendaftaran" class="col-sm-2 control-label">ID Pendaftaran</label>
			<div class="col-sm-5">
				<?php 
				echo form_dropdown('data[id_pendaftaran]',$pendaftaran,!empty($default->id_pendaftaran) ? $default->id_pendaftaran : '','class="form-control choose-select" id="id_pendaftaran"');
				?>
			</div>
		</div>
		<input type="hidden" name="id_pasien" id="hiddenID" value="<?php echo !empty($default->id_pasien) ? $default->id_pasien : '';?>">
		<div class="form-group">
			<label for="jumlah" class="col-sm-2 control-label">Jumlah</label>
			<div class="col-sm-3">
				<?php echo form_input('data[jumlah]',!empty($default->total) ? $default->total : '', 'placeholder="Total" readonly class="form-control col-md-7 col-xs-12" id="jumlah" ') ?>
			</div>
		</div>
		<div class="form-group" >
			<label for="bayar" class="col-sm-2 control-label">Bayar</label>
			<div class="col-sm-3">
				<?php 
				$data = array(
					'name'        => 'data[bayar]',
					'id'          => 'bayarannyacoy',
					'value'       =>  '',
					'placeholder' => 'jumlah bayar',
					'class' => 'form-control',
					'type' => 'number'
					);

				echo form_input($data);
				?>
			</div>
		</div>
		<div class="form-group" >
			<label for="kembali" class="col-sm-2 control-label">Kembali</label>
			<div class="col-sm-3">
				<?php 
				$data = array(
					'name'        => 'data[kembali]',
					'id'          => 'kembali',
					'value'       => '',
					'placeholder' => 'kembalian',
					'type' => 'number',
					'class' => 'form-control',
					'readonly' => true
					);

				echo form_input($data);
				?>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-9">
				<button type="submit" class="btn btn-primary" id="simpan">Simpan</button>
				<a type="submit" class="btn btn-danger" id="lalala" onclick="javascript:location.reload()">Batal</a>
			</div>
		</div>
		<?php echo form_close();?>
	</div>
	</div>
	</div>
</div>
            
</div>

    <?=js('jquery/jquery.min.js')?>
    <?=js('bootstrap/bootstrap.min.js')?>
    <?=js('app/custom.js')?> 
    <?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
    <?=js('plugins/select2/select2.min.js')?>

<script type="text/javascript">

$(document).ready(function($){
	$("#id_pasien").change(function (){

		validate();
		var url = "<?php echo site_url('registrasi/pembayaran/add_ajax_pendaftaran');?>/"+$(this).val();
		$('#id_pendaftaran').load(url);
		return false;

	});

	$("#id_pendaftaran").change(function (){
		validate();
		var url = "<?php echo site_url('registrasi/pembayaran/total');?>/"+$(this).val();
		console.log(url);
		$.get(url, function(result) {
		    $('#jumlah').val(result);
		});
		return false;

	});

	

	$('#bayarannyacoy').on('change paste keyup', function(){
		var a = parseInt($('#jumlah').val());
		var b = parseInt($('#bayarannyacoy').val());
		$('#kembali').val(b-a);
		validate();
	});
	$('.choose-select').select2();
	$('#bayar').addClass('active');
	if($('#hiddenID').val() != ""){
		$('#bayarannyacoy').focus();
	}
	validate();
})

function validate(){
        if ($('#id_pasien').val().length > 6 && $('#id_pendaftaran').val().length > 6 && $('#bayarannyacoy').val().length > 0 && $('#kembali').val() >= 0) {
            $("#simpan").prop("disabled", false);
        }
        else {
            $("#simpan").prop("disabled", true);
        }
    }
</script>