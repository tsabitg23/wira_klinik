<?php
//koneksi ke database
// mysql_connect("localhost","root","");
// mysql_select_db("ukom2017_141510290");
 
// $id_pendf = $_GET['id_pendf'];

// $sql2 = mysql_query("SELECT * FROM v_struk WHERE id_pendf='$id_pendf'") or die();
// $data = array();
// $call = mysql_fetch_assoc($sql2);

// //mengisi judul dan header tabel
// $judul = "Lix Klinik";
// $header = array(
// array("label"=>"Id Pasien", "length"=>30, "align"=>"L"),
// array("label"=>"Nama Pasien", "length"=>50, "align"=>"L"),
// );
 
//memanggil fpdf
$localhost = base_url();
require_once ('fpdf/fpdf.php');
$pdf = new FPDF();
$pdf->AddPage('P',array(100,150));
 
//tampilan Judul Laporan
$pdf->SetFont('helvetica','','14'); //Font helvetica, Tebal/Bold, ukuran font 16
$pdf->Cell(0,10, 'WIRA Polyclinic', '0', 1,'C');
$pdf->SetFont('helvetica','','8'); //Font helvetica, Tebal/Bold, ukuran font 16
$pdf->Cell(0,10, 'Jalan Perwira 3 no. 200, Bekasi', '0', 1,'C');
$pdf->Cell(0,1, 'Telp : (021) 88882567', '0', 1,'C');
$pdf->Cell(0,10, '=================================================', '0', 1);
$pdf->SetFont('helvetica','U','12');
$pdf->Cell(0,5, 'Pendaftaran : '.$info->id_pendaftaran, '0', 1);
$pdf->SetFont('helvetica','','8');
$pdf->Cell(0,10, 'Tanggal'.' : '.nice_date($info->tgl,'d F Y'), '0', 1);
$pdf->Cell(0,1, 'Poliklinik'.' : '.$info->poliklinik, '0', 1);
$pdf->Cell(0,10, 'Dokter'.' : '.$info->dokter, '0', 1);
$pdf->SetFont('helvetica','B','8');
$pdf->Cell(0,1, 'Pasien'.' : '.$info->nama, '0', 1);
$pdf->SetFont('helvetica','','8');
$pdf->Cell(0,10, 'Nomor Antrian : ', '0', 1);
$pdf->SetFont('helvetica','','25');
$pdf->Cell(0,10, $no_antrian->no_antrian, '0', 1,'C');
$pdf->SetFont('helvetica','','8');
$pdf->Cell(0,11, 'Total Pendaftaran'.' : '.'Rp. '.$bayaran->jumlah.',-', '0', 1);
$pdf->Cell(0,1, 'Bayar'.' : '.'Rp. '.$bayaran->bayar.',-', '0', 1);
$pdf->Cell(0,10, 'Kembali'.' : '.'Rp. '.$bayaran->kembali.',-', '0', 1);
$pdf->Cell(0,10, '=================================================', '0', 1);
$pdf->SetFont('helvetica','','9');
$pdf->Cell(0,8, 'Terima Kasih', '0', 1, 'C'); 
//output file pdf
$pdf->Output();
?>