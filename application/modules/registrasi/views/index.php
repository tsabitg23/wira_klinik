<head>
	<title>Registrasi</title>
</head> 

<div class="warper container-fluid">

	<div class="page-header"><h1>Registrasi</h1></div>
	<ul role="tablist" class="nav nav-tabs" id="myTab">
		<li class="active"><a data-toggle="tab" role="tab" href="#home">Daftar Pasien</a></li>
		<li><a data-toggle="tab" role="tab" href="#daftar" id="buttonDaftar">Daftar Baru</a></li>

	</ul>
	<div class="tab-content" id="myTabContent">
		<div id="home" class="tab-pane tabs-up fade in active panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<h4 class="no-margn tabular">Form Pendaftaran Pasien</h4>
						<p><small class="tabular">atau klik "daftar baru" untuk mendaftarkan pasien baru</small></p>    
						<hr>
						<?php 
						$attrib = array('class' => 'form-horizontal','id'=>'form-daftar');
						$hidden = array('id' => '');
						echo form_open($action2,$attrib,$hidden);?>
						<div class="form-group">
							<label for="id_pasien" class="col-sm-2 control-label">ID Pasien</label>
							<div class="col-sm-3">
								<?php 
								echo form_dropdown('data[id_pasien]',$pasien,!empty($default->id_pasien) ? $default->id_pasien : '','class="form-control choose-select" id="id_pasien"');
								?>
							</div>
						</div>
						<div class="form-group">
							<label for="nama" class="col-sm-2 control-label">Nama</label>
							<div class="col-sm-5">
								<?php echo form_input('nama',!empty($default->nama_depan)? $default->nama_depan." ".$default->nama_belakang : '', 'placeholder="Nama Pasien" class="form-control col-md-7 col-xs-12" id="nama" disabled') ?>
							</div>
						</div>
						<div class="form-group">
							<label for="poliklinik" class="col-sm-2 control-label">Poliklinik</label>
							<div class="col-sm-3">
								<?php 
								echo form_dropdown('poliklinik',$poliklinik,'','class="form-control choose-select" id="poliklinik"');
								?>
							</div>
						</div>
						<input type="hidden" id="dataLay" name="dataLay" value="">
						<div class="form-group">
							<label for="layanan" class="col-sm-2 control-label">Layanan</label>
							<div class="col-sm-2">
								<?php echo form_dropdown('layanan',$layanan,'', 'class="form-control choose-select" id="layanan" ') ?>
							</div>
							<div class="col-sm-2" id="har">
							  <?php echo form_input('harga','', 'placeholder="Harga" readonly class="form-control col-md-7 col-xs-12" id="harga" ') ?>
							</div>
							<div class="col-sm-2">
								<a class="btn btn-success" id="addRow">Tambah</a>
							</div>
						</div>
						<table id="tbl_lay" class="table table-bordered" style="width:60%;margin:20px 17%;">
							<thead>
								<th>Layanan</th>
								<th>Tarif</th>
								<th width="5%">Aksi</th>
								<th style="visibility:hidden;">id_p_layanan</th>
							</thead>
							<tbody>
							</tbody>
						</table>
						<input type="hidden" value="pendaftaran" name="pem[tipe_pembayaran]">
						<div class="form-group">
							<label for="nama" class="col-sm-2 control-label">Total</label>
							<div class="col-sm-5">
								<?php echo form_input('pem[jumlah]','0', 'class="form-control col-md-7 col-xs-12" id="total" readonly') ?>
							</div>
						</div>
						<div class="form-group">
							<label for="bayar" class="col-sm-2 control-label">bayar</label>
							<div class="col-sm-5">
								<?php 
								$data = array(
									'name'        => 'pem[bayar]',
									'id'          => 'jumlahBayar',
									'value'       => '',
									'placeholder' => 'Nominal Bayar',
									'type' => 'number',
									'class' => 'form-control'
									);

								echo form_input($data);
								?>
							</div>
						</div>
						<div class="form-group">
							<label for="kembali" class="col-sm-2 control-label">Kembali</label>
							<div class="col-sm-5">
								<?php echo form_input('pem[kembali]','0', 'class="form-control col-md-7 col-xs-12" id="kembali" readonly') ?>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-9">
								<a type="submit" class="btn btn-primary" id="simpanan">Simpan</a>
								<a type="submit" class="btn btn-danger" id="nyanyanya" onclick="javascript:location.reload()">Batal</a>
							</div>
						</div>
						<?php echo form_close();?>	
					</div>
				</div>
			</div>
		</div>
		<div id="daftar" class="tab-pane tabs-up fade panel panel-default">
			<div class="panel-heading h4tab"><h5>Form Input Pasien</h5></div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<?php 
						$attrib = array('class' => 'form-horizontal');
						$hidden = array('id' => '');
						echo form_open($action,$attrib,$hidden);?>
						<div class="form-group">
							<label for="nama_depan" class="col-sm-2 control-label">Nama depan</label>
							<div class="col-sm-8">
								<?php echo form_input('data[nama_depan]','', 'placeholder="Nama Depan Pasien" class="form-control col-md-7 col-xs-12" id="nama_depan" ') ?>
							</div>
						</div>
						<div class="form-group">
							<label for="nama_belakang" class="col-sm-2 control-label">Nama Belakang</label>
							<div class="col-sm-8">
								<?php echo form_input('data[nama_belakang]','', 'placeholder="Nama belakang pasien" class="form-control col-md-7 col-xs-12" id="nama_belakang" ') ?>
							</div>
						</div>
						<div class="form-group" >
							<label for="alamat" class="col-sm-2 control-label">Alamat</label>
							<div class="col-sm-8">
								<?php 
								$data = array(
									'name'        => 'data[alamat]',
									'id'          => 'alamat',
									'value'       => '',
									'rows'        => '6',
									'cols'        => '40',
									'placeholder' => 'Alamat Pasien',
									'class' => 'form-control'
									);

								echo form_textarea($data);
								?>
							</div>
						</div>
						<div class="form-group">
							<label for="jenis_kelamin" class="col-sm-2 control-label">Jenis Kelamin</label>
							<div class="col-sm-8">
								<?php echo form_dropdown('data[jenis_kelamin]',$jenis_k,'', ' class="form-control choose-select" id="jenis_kelamin" ') ?>
							</div>
						</div>
						<div class="form-group" >
							<label for="tgl_lahir" class="col-sm-2 control-label">Tanggal Lahir</label>
							<div class="col-sm-3">
								<?php 
								$data = array(
									'name'        => 'data[tgl_lahir]',
									'id'          => 'tgl_lahir',
									'value'       => '',
									'placeholder' => 'tanggal lahir',
									'type' => 'date',
									'class' => 'form-control'
									);

								echo form_input($data);
								?>
							</div>
						</div>
						<div class="form-group" >
							<label for="telepon" class="col-sm-2 control-label">No Telepon</label>
							<div class="col-sm-3">
								<?php 
								$data = array(
									'name'        => 'data[telepon]',
									'id'          => 'telepon',
									'value'       => '',
									'placeholder' => 'Nomor telepon',
									'type' => 'number',
									'class' => 'form-control'
									);

								echo form_input($data);
								?>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-9">
								<button type="submit" class="btn btn-primary" id="simpan">Simpan</button>
								<a type="submit" class="btn btn-danger" id="batal" onclick="clearForm()">Batal</a>
							</div>
						</div>
						<?php echo form_close();?>
					</div>

				</div>
			</div>
		</div>
	</div>



</div>

<?=js('jquery/jquery.min.js')?>
<?=js('bootstrap/bootstrap.min.js')?>
<?=js('app/custom.js')?>
<?=js('plugins/underscore/underscore-min.js')?> 
<?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
<?=js('plugins/select2/select2.min.js')?>
 <?=js('plugins/tabletojson/jquery.tabletojson.js')?>

<script type="text/javascript">
	$(document).ready(function($){
		validate2();
		$('#jenis_kelamin').select2();
		validate();
		$('#id_pasien').on('change',function(){
			if($(this).val() != 'kosong'){
				var url = "<?php echo site_url('registrasi/nama');?>/"+$(this).val();
				$.get(url, function(result) {
					$('#nama').val(result);
				});
			}
			else{
				$('#nama').val("");
			}
			validate2();
		});

		$('#layanan').on('change',function(){
			if($(this).val().length > 6){
				var url = "<?php echo site_url('registrasi/harga');?>/"+$(this).val();
				$.get(url, function(result) {
					$('#harga').val(result);
				});
			}
			else{
				$('#harga').val("");
			}
		});

		$("#poliklinik").change(function (){
			validate2();
			var url = "<?php echo site_url('registrasi/add_ajax_layanan');?>/"+$(this).val();
			$('#layanan').load(url);
			return false;
		})

		$('#addRow').on('click', function () {
            var data = '<tr><td>'+$('#select2-layanan-container').text()+'</td><td>'+$('#harga').val()+'</td><td><a class="btn btn-danger btn-xs hapus-dong"><i class="fa fa-close"></i></a></td><td width="1" style="display:none;">'+$('#layanan').val()+'</td></tr>';
              $('#tbl_lay').append(data);
              var a = parseInt($('#total').val());
              var b = parseInt($('#harga').val());
              $('#total').val(a+b);
              $('#layanan').val("").trigger('change');
              $('#harga').val("");
              $('#layanan').focus();
              validate2();
        } );
        $('#tbl_lay').on('click','a.hapus-dong',function(){
          var a = parseInt($(this).parent().prev().text());
          var b = parseInt($('#total').val());
          $('#total').val(b-a);
          $(this).parent().parent().remove();
          validate2();
        })

        $('#simpanan').on('click',function(){
          var table = $('#tbl_lay').tableToJSON({
            ignoreColumns : [0,1,2]
          });
          var url   = "<?php echo base_url('/registrasi/savepen')?>";
          
          $('#dataLay').val(JSON.stringify(table));
          $('#form-daftar').submit();
        });

		$('#id_pasien').focus();
		$('#regis').addClass('active');
		$('#nama_belakang,#nama_depan,#alamat,#telepon,#tgl_lahir').on('change paste keyup', validate);
		$('#searchRow').on('click',function(){
			$('#searchnya').submit();

		})
		$('#jumlahBayar').on('change paste keyup',function(){
			$('#kembali').val(hitungKembali());
			validate2();
		})
		$('.choose-select').select2();
	});

	function validate(){
		if ($('#nama_belakang').val().length>0 && $('#nama_depan').val().length>0 && $('#alamat').val().length>0 && $('#tgl_lahir').val().length>0 && $('#telepon').val().length>0) {
			$("#simpan").prop("disabled", false);
		}
		else {
			$("#simpan").prop("disabled", true);
		}
	}

	function validate2(){
		if ($('#id_pasien').val()!= 'kosong' && $('#poliklinik').val()!= 'kosong' && $('#total').val() != 0 && $('#jumlahBayar').val().length > 0 && $('#kembali').val() >=0) {
			$("#simpanan").removeClass("disabled");
		}
		else {
			$("#simpanan").addClass("disabled");
		}
	}

	function hitungKembali(){
		var a = parseInt($('#total').val());
		var b = parseInt($('#jumlahBayar').val());
		var c = b-a;
		return c;
	}

	function clearForm(){
		$('#nama_depan').val("");
		$('#nama_belakang').val("");
		$('#alamat').val("");
		$('#jenis_kelamin').val("").trigger('change');
		$('#tgl_lahir').val("");
		$('#telepon').val("");
		$('#nama_depan').focus();
	}



</script>