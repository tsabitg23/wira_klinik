<head>
  <title>Pasien - Detail</title>
</head>

<div class="warper container-fluid">
  <ol class="breadcrumb">
    <li>Registrasi</li>
    <li><a href="<?php echo base_url('/registrasi/pasien');?>">Pasien</a></li>
    <li class="active" style="text-transform:capitalize;"><?php echo $default->nama_depan.' '.$default->nama_belakang;?></li>
  </ol>
  <div class="page-header" style="text-transform:capitalize;"><h1><?php echo $default->nama_depan.' '.$default->nama_belakang;?> <a href="<?php echo base_url('/registrasi/pasien');?>"><button class="btn btn-primary btn-xs"><i class="fa fa-arrow-left"></i> Back</button></a> <a href="<?php echo base_url().'registrasi?id='.$this->input->get('id')?>" class="btn btn-success btn-xs"><i class="fa fa-address-book-o"></i> Registrasi</a></h1></div>

  <div class="row">
    <div class="col-md-12" id="info">
      <div class="panel panel-default">
        <div class="panel-heading">Info Pasien <span style="float:right;"><a class="btn btn-primary btn-xs" href="<?php echo base_url().'registrasi/pasien/form?id='.base64_encode($default->id_pasien);?>"><i class="fa fa-edit"></i> Edit data</a> <a class="btn btn-warning btn-xs" target="_blank" href="<?php echo base_url().'registrasi/pasien/kartu/'.$default->id_pasien;?>"><i class="fa fa-print"></i> Cetak Kartu</a></span></div>
        <div class="panel-body nicescroll">
          <table id="tbl_info" class="table">
            <thead>
              <tr>
                <th>Data</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Nama</td>
                <td ><?php echo $default->nama_depan.' '.$default->nama_belakang;?></td>
              </tr>
              <tr>
                <td>Jenis Kelamin</td>
                <td ><?php echo ($default->jenis_kelamin == 'L') ? 'Laki-laki' : 'Perempuan';?></td>
              </tr>
              <tr>
                <td>Tanggal lahir</td>
                <td ><?php echo nice_date($default->tgl_lahir,'d M Y');?></td>
              </tr>
              <tr>
                <td>Alamat</td>
                <td ><?php echo $default->alamat;?></td>
              </tr>
              <tr>
                <td>Telepon</td>
                <td ><?php echo $default->telepon;?></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12" id="jad">
      <div class="panel panel-default">
        <div class="panel-heading">Pendaftaran</div>
        <div class="panel-body nicescroll">
          <div class="tabular" style="margin-bottom:3%;">
            <h6 style="display:inline">**</h6> Status Resep <span class="label label-danger">merah</span> : Resep belum bisa diambil , <span class="label label-warning">kuning</span> : Resep Dapat diambil, <span class="label label-success">hijau</span> : Resep telah diambil
          </div>
          <table id="tbl_pen" class="table">
            <thead>
              <tr>
                <th width="1%">No</th>
                <th>Kode Pendaftaran</th>
                <th>Tanggal</th>
                <th>Poliklinik</th>
                <th>Dokter</th>
                <th>Status Resep</th>
                <th width="20%">Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=0; foreach ($pendaftaran as $pen ) { $no++?>    
                <tr>
                  <td><?php echo $no;?></td>
                  <td><?php echo $pen->id_pendaftaran;?></td>
                  <td><?php echo nice_date($pen->tgl,'d M Y');?></td>
                  <td><?php echo $pen->poliklinik;?></td>
                  <td><?php echo $pen->dokter;?></td>
                  <td><?php if($pen->status_ambil_resep == 0){echo '<span class="label label-danger">merah</span>';}else if($pen->status_ambil_resep == 1 && $pen->status_pembayaran == null){echo '<span class="label label-warning">kuning</span>';}else if($pen->status_ambil_resep == 1 && $pen->status_pembayaran == 1){echo '<span class="label label-success">hijau</span>';}?></td>
                  <td>
                  <?php 
                    if($pen->status_ambil_resep == 0){
                        echo "<a href='".base_url()."registrasi/pembayaran?id=".base64_encode($default->id_pasien)."&&daftar=".base64_encode($pen->id_pendaftaran)."'>
                                      <button class='btn btn-primary btn-xs'><i class='fa fa-money'></i> Lunasi</button>
                                    </a>";
                    }
                  ?>
                  <a href="<?php echo base_url().'registrasi/pasien/pendaftaran?id_pasien='.$this->input->get('id').'&&id_daftar='.base64_encode($pen->id_pendaftaran) ?>">
                      <button class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Lihat</button>
                    </a>
                  <a target="_blank" href="<?php echo base_url().'registrasi/pasien/laporan/'.$pen->id_pendaftaran;?>" >
                      <button class="btn btn-warning btn-xs"><i class="fa fa-print"></i> Cetak</button>
                    </a>
                  </td>
                </tr>
                <?php } ?>   

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>      

  </div>

  <?=js('jquery/jquery.min.js')?>
  <?=js('bootstrap/bootstrap.min.js')?>
  <?=js('app/custom.js')?>
  <?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
  <?=js('plugins/data-table/jquery.dataTables.min.js')?>
  <?=js('plugins/data-table/dataTables.bootstrap.min.js')?>
  <script type="text/javascript">
    $(document).ready(function($){
      $('#data').addClass('active');
      $('#tbl_pen').DataTable({
        ordering : false
      });
    });
  </script>

