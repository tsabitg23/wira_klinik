<head>
	<title>Registrasi</title>
</head> 

<div class="warper container-fluid">
<ol class="breadcrumb">
    <li>Registrasi</li>
    <li class="actives">Pasien</li>
</ol>
	<div class="page-header"><h1>Data Pasien</h1></div>
	
	<div class="row">
		<div class="col-md-12">
			<h4 class="no-margn tabular">Cari pasien untuk Melihat datanya</h4>
			<p><small class="tabular">Atau melakukan aksi lainnya </small></p>    
			<hr>
			<div class="alert alert-danger alert-dismissible" role="alert" id="hasilZero" style="display:none;">
				<strong>Hasil Tidak Ditemukan!</strong> Tidak dapat menemukan data pasien 
			</div>
			<div class="alert alert-success alert-dismissible" role="alert" id="hasilFound" style="display:none;">
				<strong><?php echo !empty($jumlah_found)?$jumlah_found : '';?> Data Ditemukan!</strong> klik lihat untuk melihat detail 
			</div>
			<form role="form" id="searchnya" method="POST" action="<?php echo base_url('registrasi/pasien');?>">
				<div class="form-group">
					<div class="form-group">
						<label for="id_pasien">Cari Berdasarkan</label><br>
						<input type="hidden" value="<?php echo $status;?>" id="result">
						<div class="col-sm-2">
							<?php echo form_dropdown('jenis',$jenis,'','class="form-control choose-select" id="jenis" ') ?>
						</div>
						<div class="col-sm-3">
							<?php 
							$data = array(
								'name'        => 'id_pasien',
								'id'          => 'id_pasien',
								'value'       => '',
								'placeholder' => 'ID Pasien',
								'type' => 'text',
								'class' => 'form-control'
								);

							echo form_input($data);
							?>
						</div>
						<div class="col-sm-2">
							<a class="btn btn-success" id="searchRow"><i class="fa fa-search"></i> Cari</a>
						</div>
					</div>
				</div>
			</form>	
		</div>
	</div>

	<div class="row" style="margin-top:5%;">
		<div class="col-md-10" id="hasilDetail" style="display:none;">
			<div class="panel panel-default">
				<div class="panel-heading">Pasien Ditemukan </div>
				<div class="panel-body nicescroll">
					<table id="tbl_pas" class="table table-bordered">
					  <thead>
					    <tr>
					      <th width="1%">No</th>
					      <th>Nama Pasien</th>
					      <th width="15%">Aksi</th>
					    </tr>
					  </thead>
					  <tbody>
					  <?php $no=0; foreach ($hasil_pasien as $hspas ) { $no++?>
					      <tr>
					        <td><?php echo $no;?></td>
					        <td><?php echo $hspas->nama_depan.' '.$hspas->nama_belakang;?></td>
					        <td>
					          <a href="<?php echo base_url().'registrasi/pasien/detail?id='.base64_encode($hspas->id_pasien);?>"><button class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Lihat</button></a>
					        </td>
					      </tr>
					      <?php } ?>   

					    </tbody>
					  </table>
				</div>
			</div>
		</div>
	</div>

      





</div>

<?=js('jquery/jquery.min.js')?>
<?=js('bootstrap/bootstrap.min.js')?>
<?=js('app/custom.js')?> 
<?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
<script type="text/javascript">
	$(document).ready(function($){
		$('#searchRow').on('click',function(){
			$('#searchnya').submit();
		})
		$('#jenis').change(function(){
			var a='';
			if($('#jenis').val() == 'id'){
				$('#id_pasien').attr('placeholder','ID Pasien');
				$('#id_pasien').focus();
			}
			else{
				$('#id_pasien').attr('placeholder','Nama Pasien');
				$('#id_pasien').focus();
			}
		})
		$('#data').addClass('active');
		var status = $('#result').val();
		hasilZero(status);
		$('#id_pasien').focus();
	});

	function hasilZero(status){
		if(status ==0){
			$('#hasilZero').css('display','inherit');
			$('#id_pasien').focus();
		}
		else if(status ==1){
			$('#hasilFound').css('display','inherit');
			$('#hasilDetail').css('display','inherit');
			$('#id_pasien').focus();
		}
	}

</script>