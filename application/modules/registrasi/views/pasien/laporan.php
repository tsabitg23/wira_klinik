<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="icon" type="image/png" href="<?php echo base_url('/assets/img/logo/icon.png');?>">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?=css('font/bariol.css');?>
	<?=css('bootstrap/bootstrap.css');?> 
	<?=css('font/roboto.css');?>
	<?=css('main/main.css');?>
</head>
<body onload="window.print()">


<div class="row">
	    <div class="col-md-12" id="dataResep">
	        <div class="panel panel-default">
	            <div class="panel-body nicescroll">
	            	<div class="row">
	            		<div class="col-lg-4 col-lg-offset-4">
	            	    	<h3 class="text-center"><?=img('logo/logobluesmall.png')?></h3>
	            	    </div>
	            	    <div class="col-md-6 tabular">
	            	    
	            	        <table class="table no-bor">
	            	        	<tr>
	            	        		<td><strong>Data Pendaftaran</strong></td>		
	            	        	</tr>
	            	        	<tr>
	            	        		<td>Nama Pasien</td>		
	            	        		<td>:</td>		
	            	        		<td style="text-transform:capitalize;"><?php echo $info->nama?></td>		
	            	        	</tr>
	            	        	<tr>
	            	        		<td>Tanggal Daftar</td>		
	            	        		<td>:</td>		
	            	        		<td><?php echo nice_date($info->tgl,'d M Y');?></td>		
	            	        	</tr>
	            	        	<tr>
	            	        		<td>Poliklinik</td>		
	            	        		<td>:</td>		
	            	        		<td><?php echo $info->poliklinik;?></td>		
	            	        	</tr>
	            	        	<tr>
	            	        		<td>Dokter</td>		
	            	        		<td>:</td>		
	            	        		<td><?php echo $info->dokter;?></td>		
	            	        	</tr>
	            	        	<tr>
	            	        		<td>Total Resep</td>		
	            	        		<td>:</td>		
	            	        		<td><?php echo ($info->total_resep != 0)? $info->total_resep : 'Tidak Ada Resep';?></td>		
	            	        	</tr>
	            	        </table>
	            	        
	            	        
	            	    </div>
	            	    <div class="col-md-6 text-right">
                    
                        <dl>
                          <dt>Petugas</dt>
                          <dd style="text-transform:capitalize;"><?php echo $this->session->userdata('nama');?></dd>
                        </dl>
                       
                    </div>
	            	    
	            		
	            	</div>
	                <table id="tbl_info" class="table table-striped">
	                  <thead>
	                    <tr>
	                      <th width="1">No</th>
	                      <th>Jenis</th>
	                      <th>Nama</th>
	                      <th>Tarif</th>
	                    </tr>
	                  </thead>
	                  <tbody>
	                  <?php $no=0; foreach ($layanan as $lay ) { $no++;?>    
	                            <tr>
	                                <td><?php echo $no;?></td>
	                                <td><?php echo $lay->jenis;?></td>
	                                <td><?php echo $lay->nama;?></td>
	                                <td class="tarif"><?php echo $lay->tarif;?></td>
	                            </tr>
	                            <?php

	                        } ?>
	                  </tbody>
	                  </table>
	                  <hr>
                        <div class="tabular col-md-offset-10" id="tab1" style="position:relative;left:-5%;">
                                    Total
                                    <span id="total_daftar" style="font-weight:bold;"></span><br>
                        </div>
	            </div>
	        </div>
	    </div>
	</div>

</body>
</html>

	<?=js('jquery/jquery.min.js')?>
	<?=js('bootstrap/bootstrap.min.js')?>
	<?=js('app/custom.js')?>

	<script type="text/javascript">
		var sum = 0;
        var sum2= 0;
		$('.tarif').each(function(){
		    sum += parseFloat($(this).text());  
		});

        $('#total_daftar').text(sum);
	</script>