<head>
	<title>Pasien - Data Pendaftaran</title>
</head>

<div class="warper container-fluid">
	<ol class="breadcrumb">
		<li>Registrasi</li>
		<li><a href="<?php echo base_url().'registrasi/pasien';?>">Pasien</a></li>
		<li style="text-transform:capitalize;"><a href="<?php echo base_url().'registrasi/pasien/detail?id='.base64_encode($default->id_pasien);?>"><?php echo $default->nama_depan.' '.$default->nama_belakang;?></a></li>
		<li class="active">Pendaftaran</li>
	</ol>
	<div class="page-header"><h1>Detail Pendaftaran <a href="<?php echo base_url().'registrasi/pasien/detail?id='.base64_encode($default->id_pasien);?>" class="btn btn-primary btn-xs"><i class="fa fa-arrow-left"></i> Back</a></h1></div>

	<div class="row">
		<div class="col-md-12" id="info">
			<div class="panel panel-default">
				<div class="panel-heading">Info Pendaftaran<span style="float:right;"><a class="btn btn-warning btn-xs" target="_blank" href="<?php echo base_url().'registrasi/pasien/laporan/'.$pendaftaran->id_pendaftaran;?>"><i class="fa fa-print"></i> Cetak Laporan</a></span></div>
				<div class="panel-body nicescroll">
					<table id="tbl_info" class="table">
						<thead>
							<tr>
								<th>Data</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Kode Pendaftaran</td>
								<td ><?php echo $pendaftaran->id_pendaftaran;?></td>
							</tr>
							<tr>
								<td>tgl</td>
								<td ><?php echo nice_date($pendaftaran->tgl,'d M Y');?></td>
							</tr>
							<tr>
								<td>Poliklinik</td>
								<td ><?php echo $pendaftaran->poliklinik;?></td>
							</tr>
							<tr>
								<td>Dokter</td>
								<td ><?php echo $pendaftaran->dokter;?></td>
							</tr>
							<tr>
								<td>Biaya Pendaftaran</td>
								<td ><?php echo $pendaftaran->total_pemeriksaan;?></td>
							</tr>
							<tr>
								<td>Biaya Resep</td>
								<td ><?php echo ($pendaftaran->total_resep > 0)? $pendaftaran->total_resep : 'belum ada data resep';?></td>
							</tr>
							<tr>
								<td>Status Bayar</td>
								<td ><?php echo ($pendaftaran->status_ambil_resep==0)? '<span class="label label-danger">BELUM LUNAS</span>' : '<span class="label label-success">LUNAS</span>';?></td>
							</tr>
							<tr>
								<td>Status Ambil Resep</td>
								<td ><?php 
									if($status_resep->status_ambil_resep == 0){echo '<span class="label label-danger">Tidak Dapat Diambil</span>';}else if($status_resep->status_ambil_resep == 1 && $status_resep->status_resep == 0 && $status_resep->resep != ""){echo '<span class="label label-warning">Dapat Diambil</span>';}else if($status_resep->status_ambil_resep == 1 && $status_resep->status_resep == 0 && $status_resep->resep == ""){echo '<span class="label label-danger">Resep Tidak Ada</span>';}else if($status_resep->status_ambil_resep == 1 && $status_resep->status_resep == 1){echo '<span class="label label-success">Sudah Diambil</span>';}
								?></td>
							</tr>
							<tr>
								<td>Keluhan</td>
								<td ><?php echo ($info_periksa->keluhan != null)? $info_periksa->keluhan : '<i>Belum Diinput</i>';?></td>
							</tr>
							<tr>
								<td>Diagnosis</td>
								<td ><?php echo ($info_periksa->diagnosis != null)? $info_periksa->diagnosis : '<i>Belum Diinput</i>';?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Pendaftaran Layanan</div>
				<div class="panel-body nicescroll">
					<table id="tbl_pen_det" class="table">
						<thead>
							<tr>
								<th width="1%">No</th>
								<th>Layanan</th>
								<th>Tarif</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=0;$stat=0; foreach ($pen_det as $pen ) { $no++;
								if($pen->jenis =='resep dokter'){
									$no--;
								}
								else if($pen->jenis=='layanan' && $pen->tipe == 'tambahan'){
									$no--;
								}
								else if($pen->jenis=='layanan' && $pen->tipe =='pendaftaran'){
									if($pen->status_pembayaran == 1){
										$stat =1;
									}
								?>    
								<tr>
									<td><?php echo $no;?></td>
									<td><?php echo $pen->nama;?></td>
									<td class="tarif"><?php echo $pen->tarif;?></td>
								</tr>
								<?php }} ?>   
                            </tbody>
                        </table>
                        <hr>
                        <div class="tabular" id="tab1" style="display:none;">
                                    Total
                                    <span id="total_daftar" style="font-weight:bold;"></span><br>
                                    Status
                                    <?php echo ($stat==0)?'<span class="label label-danger">BELUM LUNAS</span>' : '<span class="label label-success">LUNAS</span>' ?>
                        </div>
                    </div>
				</div>
			</div>
		</div>

	<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Pendaftaran Tambahan</div>
                <div class="panel-body nicescroll">
                    <table id="tbl_pen_tam" class="table">
                        <thead>
                            <tr>
                                <th width="1%">No</th>
                                <th>Layanan</th>
                                <th>Tarif</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $number=0;$stat=0; foreach ($pen_det as $pen ) { $number++;
                                if($pen->jenis =='resep dokter'){
									$no--;
								}
								else if($pen->jenis=='layanan' && $pen->tipe == 'pendaftaran'){
									$no--;
								}
                                else if($pen->jenis=='layanan' && $pen->tipe =='tambahan'){
                                    if($pen->status_pembayaran == 1){
                                        $stat =1;
                                    }
                                ?>    
                                <tr>
                                    <td><?php echo $number;?></td>
                                    <td><?php echo $pen->nama;?></td>
                                    <td class="tariftambah"><?php echo $pen->tarif;?></td>
                                </tr>
                                <?php }} ?>   
                            </tbody>
                        </table>
                        <hr>
                        <div class="tabular" id="tab2" style="display:none;">
                                    Total
                                    <span id="total_tambah" style="font-weight:bold;"></span><br>
                                    Status
                                    <?php echo ($stat==0)?'<span class="label label-danger">BELUM LUNAS</span>' : '<span class="label label-success">LUNAS</span>' ?>
                        </div>
                </div>
            </div>
        </div>
    </div>

	</div>

	<?=js('jquery/jquery.min.js')?>
	<?=js('bootstrap/bootstrap.min.js')?>
	<?=js('app/custom.js')?>
	<?=js('plugins/underscore/underscore-min.js')?> 
	<?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
	<?=js('plugins/data-table/jquery.dataTables.min.js')?>
    <?=js('plugins/data-table/dataTables.bootstrap.min.js')?>

	<script type="text/javascript">
		var sum = 0;
        var sum2= 0;
		$('.tarif').each(function(){
		    sum += parseFloat($(this).text());  
		});

        $('.tariftambah').each(function(){
            sum2 += parseFloat($(this).text());  
        });

        if($('.tarif').text() != ''){
            $('#tab1').css('display','inherit');
        }

        if($('.tariftambah').text() != ''){
            $('#tab2').css('display','inherit');
        }

        $('#total_daftar').text(sum);
		$('#total_tambah').text(sum2);

        $('#data').addClass('active');
		$('#tbl_pen_det,#tbl_pen_tam').DataTable({
		  searching : false,
		  "lengthChange": false,
		  ordering: false,
		  paging:false,
		  info : false
		});
	</script>