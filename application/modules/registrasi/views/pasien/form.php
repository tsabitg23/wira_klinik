<head>
    <title>Pasien - Edit</title>
</head>
          
<div class="warper container-fluid">
<ol class="breadcrumb">
    <li>Registrasi</li>
    <li><a href="<?php echo base_url().'registrasi/pasien';?>">Pasien</a></li>
    <li style="text-transform:capitalize;"><a href="<?php echo base_url().'registrasi/pasien/detail?id='.base64_encode($default->id_pasien);?>"><?php echo $default->nama_depan.' '.$default->nama_belakang;?></a></li>
    <li class="active">Form</li>
</ol>
<div class="page-header"><h1>Form Edit</h1></div>

<div class="row">
	<div class="col-md-12">
		<?php 
		$attrib = array('class' => 'form-horizontal');
		$hidden = array('id' => $default->id_pasien);
		echo form_open($action,$attrib,$hidden);?>
		<div class="form-group">
			<label for="nama_depan" class="col-sm-2 control-label">Nama depan</label>
			<div class="col-sm-8">
				<?php echo form_input('data[nama_depan]',!empty($default->nama_depan) ? $default->nama_depan : '', 'placeholder="Nama Depan Pasien" class="form-control col-md-7 col-xs-12" id="nama_depan" ') ?>
			</div>
		</div>
		<div class="form-group">
			<label for="nama_belakang" class="col-sm-2 control-label">Nama Belakang</label>
			<div class="col-sm-8">
				<?php echo form_input('data[nama_belakang]',!empty($default->nama_belakang) ? $default->nama_belakang : '', 'placeholder="Nama belakang pasien" class="form-control col-md-7 col-xs-12" id="nama_belakang" ') ?>
			</div>
		</div>
		<div class="form-group" >
			<label for="alamat" class="col-sm-2 control-label">Alamat</label>
			<div class="col-sm-8">
				<?php 
				$data = array(
					'name'        => 'data[alamat]',
					'id'          => 'alamat',
					'value'       => !empty($default->alamat) ? $default->alamat : '',
					'rows'        => '6',
					'cols'        => '40',
					'placeholder' => 'Alamat Pasien',
					'class' => 'form-control'
					);

				echo form_textarea($data);
				?>
			</div>
		</div>
		<div class="form-group">
			<label for="jenis_kelamin" class="col-sm-2 control-label">Jenis Kelamin</label>
			<div class="col-sm-8">
				<?php echo form_dropdown('data[jenis_kelamin]',$jenis_k,!empty($default->jenis_kelamin) ? $default->jenis_kelamin : '', ' class="form-control choose-select" id="jenis_kelamin" ') ?>
			</div>
		</div>
		<div class="form-group" >
			<label for="tgl_lahir" class="col-sm-2 control-label">Tanggal Lahir</label>
			<div class="col-sm-3">
				<?php 
				$data = array(
					'name'        => 'data[tgl_lahir]',
					'id'          => 'tgl_lahir',
					'value'       => !empty($default->tgl_lahir) ? $default->tgl_lahir : '',
					'placeholder' => 'tanggal lahir',
					'type' => 'date',
					'class' => 'form-control'
					);

				echo form_input($data);
				?>
			</div>
		</div>
		<div class="form-group" >
			<label for="telepon" class="col-sm-2 control-label">No Telepon</label>
			<div class="col-sm-3">
				<?php 
				$data = array(
					'name'        => 'data[telepon]',
					'id'          => 'telepon',
					'value'       => !empty($default->telepon) ? $default->telepon : '',
					'placeholder' => 'Nomor telepon',
					'type' => 'number',
					'class' => 'form-control'
					);

				echo form_input($data);
				?>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-9">
				<button type="submit" class="btn btn-primary" id="simpan">Simpan</button>
			</div>
		</div>
		<?php echo form_close();?>
	</div>

</div>
            
</div>

    <?=js('jquery/jquery.min.js')?>
    <?=js('bootstrap/bootstrap.min.js')?>
    <?=js('app/custom.js')?>
    <?=js('plugins/underscore/underscore-min.js')?> 
    <?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>