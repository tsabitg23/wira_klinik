<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pasien extends MX_Controller {

	private $_module;

	public function __construct() {
        parent::__construct();

        //set Module location
        $this->_level = 'registrasi';
        $this->_module = 'registrasi/pasien';
        $this->_header = 'layout/header_registrasi';
        $this->_footer = 'layout/footer';
        
        $this->load->model('pasien_model','paem');
        $this->load->helper('date');
    }

    public function cekstatuslogin() {
        if ($this->session->has_userdata('uname') == FALSE) {
            redirect(base_url());
        }
        elseif ($this->session->has_userdata('uname') == true && $this->session->userdata('level') != $this->_level) {
            redirect(base_url('/'.$this->session->userdata('level')));
        }
    }

    public function index()
    {   
        $this->cekstatuslogin();
        $data['hasil_pasien'][0] = new stdClass();
        $data['hasil_pasien'][0]->id_pasien = '1';
        $data['hasil_pasien'][0]->nama_depan = 'john';
        $data['hasil_pasien'][0]->nama_belakang = 'doe';
        $data['jumlah_found'] = '';
        $data['jenis'] = array('id'=>'ID Pasien','nama'=>'Nama pasien');
        $data['status'] = 2;
        $id_pasien = $this->input->post('id_pasien');
        if($id_pasien != ''){
            $jenis = $this->input->post('jenis');
            if($jenis == 'id'){
                $result = $this->paem->cekUser($id_pasien);
                if($result){
                    echo "<script>
                    window.location.href='".base_url()."registrasi/pasien/detail?id=".base64_encode($id_pasien)."';
                    </script>";
                }
                else{
                    $data['status'] = 0;
                }
            }
            else{
                $result = $this->paem->searchByName($id_pasien);
                if($result->num_rows() == 0 ){
                    $data['status'] = 0;
                }
                else{
                    $data['status'] = 1;
                    $data['jumlah_found'] = $result->num_rows();
                    $data['hasil_pasien'] = $result->result();
                }  
            }
        }



        $this->load->view($this->_header);
        $this->load->view($this->_module.'/search',$data);
        $this->load->view($this->_footer);
    }

    public function detail(){
        $this->cekstatuslogin();
        $idnya = base64_decode($this->input->get('id'));
        $data['default'] = $this->paem->getData($idnya)->row();
        $data['pendaftaran'] = $this->paem->getDataDaftar($idnya)->result();

        $this->load->view($this->_header);
        $this->load->view($this->_module.'/detail',$data);
        $this->load->view($this->_footer);

    }

        public function pendaftaran(){
        $this->cekstatuslogin();
        $id_pasien = base64_decode($this->input->get('id_pasien'));
        $id_pendaftaran = base64_decode($this->input->get('id_daftar'));
        $data['default'] = $this->paem->getData($id_pasien)->row();
        $data['pendaftaran'] = $this->paem->getPendaftaran($id_pendaftaran)->row();
        $data['status_resep'] = $this->paem->checkResep($id_pendaftaran)->row();

        $data['info_periksa'] = $this->paem->getDataPendaftaran($id_pendaftaran)->row();
        $data['pen_det'] = $this->paem->getPendaftaranDetail($id_pendaftaran);

        $this->load->view($this->_header);
        $this->load->view($this->_module.'/pendaftaran',$data);
        $this->load->view($this->_footer);

    }

    public function form(){
        $this->cekstatuslogin();
        $id_pasien = base64_decode($this->input->get('id'));
        $data['action'] = $this->_module.'/save';
        $data['jenis_k'] = array('L'=>'Laki-laki','P'=>'Perempuan');
        $data['default'] = $this->paem->getData($id_pasien)->row();

        $this->load->view($this->_header);
        $this->load->view($this->_module.'/form',$data);
        $this->load->view($this->_footer);
    }

    public function save(){
            
        $id = $this->input->post('id');
        $this->db->trans_begin();
        $dataIns   = $this->input->post("data");

        $this->paem->save($dataIns, $id);
        

        if ($this->db->trans_status() === FALSE ) {
            $this->db->trans_rollback();
            echo "<script>
            alert('Data tidak tersimpan karna kesalahan tertentu ');
            window.location.href='".base_url()."registrasi/pasien/detail?id=".base64_encode($id)."';
            </script>";
        }
        else {
            $this->db->trans_commit();
            echo "<script>
            alert('Data Berhasil Disimpan ');
            window.location.href='".base_url()."registrasi/pasien/detail?id=".base64_encode($id)."';
            </script>";   
        }
        
    }

    public function kartu($id){
        $data['pasien'] = $this->paem->getData($id)->row();

        $this->load->view($this->_module.'/kartu',$data);
    }

    public function laporan($id){
        $data['info'] = $this->paem->getPendaftaran($id)->row();
        $data['layanan'] = $this->paem->getPendaftaranDetail($id);

        $this->load->view($this->_module.'/laporan',$data);
    }


}
?>
