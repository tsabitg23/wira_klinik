<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registrasi extends MX_Controller {

	private $_module;

	public function __construct() {
        parent::__construct();

        //set Module location
        $this->_level = 'registrasi';
        $this->_module = 'registrasi';
        $this->_header = 'layout/header_'.$this->_module;
        $this->_footer = 'layout/footer';
        $this->load->helper('string');
        $this->load->model('pasien_model','paem');
        $this->load->model('pendaftaran_model','pendem');
        $this->load->model('pembayaran_model','peem');
        $this->load->helper('date'); 
    }

    public function cekstatuslogin() {
        if ($this->session->has_userdata('uname') == FALSE) {
            redirect(base_url());
        }
        elseif ($this->session->has_userdata('uname') == true && $this->session->userdata('level') != $this->_level) {
            redirect(base_url('/'.$this->session->userdata('level')));
        }
    }

	public function index()
	{	
        $this->cekstatuslogin();
        $data['action'] = $this->_module.'/save';
        $data['action2'] = $this->_module.'/savepen';
        $data['jenis_k'] = array('L'=>'Laki-laki','P'=>'Perempuan');
        $data['pasien'] = $this->paem->optionPasien();
        $data['poliklinik'] = $this->paem->optionPoliklinik();
        $data['layanan'] = array('kosong' => '-- pilih layanan --');
        $id = base64_decode($this->input->get('id'));
        if($id != ""){
            $data['pasien'] = $this->paem->optionPasien('--pilih pasien--',$id);
            $data['default'] = $this->paem->getData($id)->row();
        }

        $this->load->view($this->_header);
        $this->load->view($this->_module.'/index',$data);  
        $this->load->view($this->_footer);
        
    }

    public function save(){
            
        $id = "PA".random_string('numeric',12);
        $this->db->trans_begin();
        $dataIns   = $this->input->post("data");
        $dataIns['id_pasien'] = $id;

        $this->paem->save_as_new($dataIns);

        if ($this->db->trans_status() === FALSE ) {
            $this->db->trans_rollback();
            echo "<script>
            alert('Data tidak Berhasil Diinput kesalahan tertentu ');
            window.location.href='".base_url()."registrasi/pasien/detail?id=".base64_encode($id)."';
            </script>";
        }
        else {
            $this->db->trans_commit();
            echo "<script>
            alert('Data Berhasil Diinput ');
            window.location.href='".base_url()."registrasi/pasien/detail?id=".base64_encode($id)."';
            </script>";

        }
        
    }

    public function savepen(){
            
        $id = "DA".random_string('numeric',12);
        $id_poliklinik = $this->input->post('poliklinik');
        $id_pemeriksaan = "PE".random_string('numeric',12);
        $layanan = json_decode($this->input->post('dataLay'));
        $this->db->trans_begin();

        $dataPembayaran = $this->input->post('pem');
        $dataPembayaran['id_admin'] = $this->session->userdata('id');
        $dataPembayaran['id_pendaftaran'] = $id;

        $dataIns   = $this->input->post("data");
        $id_pasien = $dataIns['id_pasien'];
        $dataIns['id_pendaftaran'] = $id;

        $dataInsPem['id_pendaftaran'] = $id;
        $dataInsPem['id_pemeriksaan'] = $id_pemeriksaan;
        $dataInsPem['tipe'] = 'pendaftaran';


        $this->pendem->save_as_new($dataIns,$id_poliklinik);
        $this->pendem->save_as_new_pemeriksaan($dataInsPem);

        foreach ($layanan as $key) {
            $dataInsPemDet['id_p_layanan'] = $key->id_p_layanan;
            $dataInsPemDet['id_pemeriksaan'] = $id_pemeriksaan;
            $this->pendem->save_as_new_pemeriksaan_detail($dataInsPemDet);
        }

        $this->peem->save_as_new($dataPembayaran);

        if ($this->db->trans_status() === FALSE ) {
            $this->db->trans_rollback();
            echo "<script>
            alert('Data tidak Berhasil Diinput kesalahan tertentu ');
            window.location.href='".base_url()."registrasi';
            </script>";
        }
        else {
            $this->db->trans_commit();
            echo "<script>
            alert('Data Berhasil Diinput ');
            window.open('".base_url()."registrasi/struk/".$id."','_blank');
            window.location.href='".base_url()."registrasi';
            </script>";

        }
        
    }


    public function nama($id_pasien){
        $this->cekstatuslogin();
        $result = $this->paem->getNama($id_pasien);
        echo $result->nama_depan." ".$result->nama_belakang;
    }

    public function harga($id_p_lay){
        $this->cekstatuslogin();
        $result = $this->paem->getHarga($id_p_lay)->row();
        echo $result->tarif;
    }

    public function add_ajax_layanan($id_poli){
        $this->cekstatuslogin();
        $this->paem->layanan($id_poli);
    }

    public function struk($id_pendaftaran){
        $data['no_antrian'] = $this->paem->getDataPendaftaran($id_pendaftaran)->row();
        $data['bayaran'] = $this->peem->getByDaftar('pendaftaran',$id_pendaftaran)->row();
        $data['info'] = $this->peem->getViewByDaftar($id_pendaftaran)->row();
        
        $this->load->view($this->_module.'/pdf',$data);
    }




}
?>
