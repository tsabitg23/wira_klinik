<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pembayaran extends MX_Controller {

	private $_module;

	public function __construct() {
        parent::__construct();

        //set Module location
        $this->_level = 'registrasi';
        $this->_module = 'registrasi/pembayaran';
        $this->_header = 'layout/header_registrasi';
        $this->_footer = 'layout/footer';
        // $this->load->helper('string');
        $this->load->model('pasien_model','paem');
        $this->load->model('pembayaran_model','peem');
        $this->load->model('pendaftaran_model','pendem');

        $this->load->helper('date');
 
    }

    public function cekstatuslogin() {
        if ($this->session->has_userdata('uname') == FALSE) {
            redirect(base_url());
        }
        elseif ($this->session->has_userdata('uname') == true && $this->session->userdata('level') != $this->_level) {
            redirect(base_url('/'.$this->session->userdata('level')));
        }
    }

	public function index()
	{	
        $this->cekstatuslogin();
        $data['action'] = $this->_module.'/save';
        $data['pasien'] = $this->peem->optionPasien();
        $data['pendaftaran'] = array('kosong'=>'--Pilih Pendaftaran--');
        $jenis  = $this->input->get('jenis');
        $data['jenis'] = 'tambahan';
        

        $id = base64_decode($this->input->get('id'));
        if($id != ""){
            $id_pendaftaran = base64_decode($this->input->get('daftar'));
            $data['pasien'] = $this->peem->optionPasien('',$id);
            $data['pendaftaran'] = $this->pendem->optionPendaftaran('',$id,$id_pendaftaran);
            $data['default'] = new stdClass();
            $data['default']->id_pasien = $id;
            $data['default']->id_pendaftaran = $id_pendaftaran;
            $result = $this->peem->getHarga($id_pendaftaran);
            $data['default']->total = $result->tarif;
        }

        $this->load->view($this->_header);
        $this->load->view($this->_module.'/index',$data);  
        $this->load->view($this->_footer);
        
    }

    public function save(){
        $this->db->trans_begin();
        $dataIns   = $this->input->post("data");
        $dataIns['id_admin'] = $this->session->userdata('id');
        $dataIns['tipe_pembayaran'] = 'tambahan';
        
        $this->peem->save_as_new($dataIns);


        if ($this->db->trans_status() === FALSE ) {
            $this->db->trans_rollback();
            echo "<script>
            alert('Data tidak tersimpan karna kesalahan tertentu ');
            window.location.href='".base_url()."registrasi';
            </script>";
        }
        else {
            $this->db->trans_commit();
            echo "<script>
            alert('Data Berhasil Disimpan ');
            window.open('".base_url()."registrasi/pembayaran/struk/".$dataIns['id_pendaftaran']."','_blank');
            window.location.href='".base_url()."registrasi';
            </script>";   
        }
        
    }

    public function add_ajax_pendaftaran($id_pasien){
        $this->cekstatuslogin();
        $this->pendem->pendaftarannya($id_pasien);
    }

    public function total($id_pendaftaran){
        $this->cekstatuslogin();
        $result = $this->peem->getHarga($id_pendaftaran);
        echo $result->tarif;
    }

    public function struk($id_pendaftaran){
        $data['bayaran'] = $this->peem->getByDaftar('tambahan',$id_pendaftaran)->row();
        $data['info'] = $this->peem->getViewByDaftar($id_pendaftaran)->row();
        $this->load->view($this->_module.'/pdf',$data);
    }

}