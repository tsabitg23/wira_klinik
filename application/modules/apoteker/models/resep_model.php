<?php

class resep_model extends CI_Model{
    private $_table           = 'pembayaran';
    private $_table1           = 'v_pendaftaran';
    protected $primary_key  = 'id_resep';

    public function save_as_new($data) {
        $this->db->set('crt','now()',false);
        $this->db->set('mdf','now()',false);
        $this->db->set('tgl','now()',false);
        $this->db->set('id_pembayaran','uuid()',false);
        $this->db->set('tipe_pembayaran','resep');
        $this->db->insert($this->_table, $data);
    }

    public function getData($key    = ""){
        $this->db->select("*");
        $this->db->where('id_pendaftaran',$key);
        $this->db->from($this->_table1);
        
        return $this->db->get();
    }

    public function getDataResep($id_pend,$key    = ""){
        $this->db->select("b.*,c.nama as nama_obat");
        $this->db->where('a.dlt',null);
        $this->db->where('a.id_pendaftaran',$id_pend);
        $this->db->join('resep_detail b','b.id_resep = a.id_resep','inner');
        $this->db->join('obat c','b.id_obat = c.id_obat','inner');
        $this->db->from('resep a');

        if($key != "")
            $this->db->where($this->primary_key,$key);

        return $this->db->get();
    }

    public function getStatusResep($id_pend){
        $this->db->select("a.status_pembayaran");
        $this->db->where('a.dlt',null);
        $this->db->where('a.id_pendaftaran',$id_pend);
        $this->db->from('resep a');

        return $this->db->get();
    }

}