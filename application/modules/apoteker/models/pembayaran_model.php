<?php

class pembayaran_model extends CI_Model{
    private $_table           = 'pembayaran';
    private $_table1           = 'v_pendaftaran';
    protected $primary_key  = 'id_pembayaran';

    public function save_as_new($data) {
        $this->db->set('crt','now()',false);
        $this->db->set('mdf','now()',false);
        $this->db->set('tgl','now()',false);
        $this->db->set($this->primary_key,'uuid()',false);
        $this->db->insert($this->_table, $data);
    }

    // untuk update data
    public function save($data, $key) {
        $this->db->set('mdf','now()',false);
        $this->db->update($this->_table, $data, array($this->primary_key => $key));
    }

    // untuk update data
    public function delete($key) {
        $this->db->set('dlt','now()',false);
        $this->db->where(array($this->primary_key => $key));
        $this->db->update($this->_table);
    }

    public function getData($key    = ""){
        $this->db->select("*");
        $this->db->where('dlt',null);
        $this->db->from($this->_table);

        if($key != "")
            $this->db->where($this->primary_key,$key);

        return $this->db->get();
    }

    public function getByDaftar($jenis,$key    = ""){
        $this->db->select("*");
        $this->db->where('dlt',null);
        $this->db->where('tipe_pembayaran',$jenis);
        $this->db->from($this->_table);

        if($key != "")
            $this->db->where('id_pendaftaran',$key);

        return $this->db->get();
    }

    public function getViewByDaftar($key    = ""){
        $this->db->select("*");
        $this->db->from($this->_table1);

        if($key != "")
            $this->db->where('id_pendaftaran',$key);

        return $this->db->get();
    }

    public function getHarga($key){
        $query = $this->db->query("select total_pembayaran('".$key."','tambahan') as tarif");
        return $query->row();
    }

    public function optionPasien($default = '-- Pilih Pasien --',$key = '') {
        $option = array();
        $this->db->where('status_ambil_resep','0');
        $this->db->from('v_pendaftaran');

        $list   = $this->db->get();
        if(!empty($key)){
            $this->db->where('id_pasien',$key);
        }
        else{
            if (!empty($default)){
             if($list->num_rows() > 0){

                $option['kosong'] = $default;
             }
             else{
                $option['kosong'] = 'Pasien yang belum melunasi Pembayaran tidak ada';
             }
            }
        }
        



        foreach ($list->result() as $row) {
            $option[$row->id_pasien] = $row->id_pasien;
        }

        return $option;
    }

}