<?php

class admin_model extends CI_Model{
    private $_table           = 'admin';
    private $_table1           = 'config_kat_admin';
    protected $primary_key  = 'id_admin';

    public function getData($key    = ""){
        $this->db->select("a.*,b.nama as jenis");
        $this->db->from($this->_table.' a');
        $this->db->join('config_kat_admin b','b.id_c_kat_admin = a.id_c_kat_admin','inner');
        $this->db->where('a.dlt',null);

        if($key != "")
            $this->db->where($this->primary_key,$key);

        return $this->db->get();
    }

    public function save($data, $key) {
        $this->db->set('mdf','now()',false);
        $this->db->update($this->_table, $data, array($this->primary_key => $key));
    }

    public function cekOldPass($id,$old){
           $this->db->select('*');
           $this->db->from('admin');
           $this->db->where('id_admin', $id);
           $this->db->where('password', $old);
           $this->db->limit(1);
           $query = $this->db->get();
               if($query->num_rows() == 1){
                 return $query->result();
               }
               else{
                 return false; 
               }
    }

}