<head>
    <title>Apoteker - Proses Resep</title>
</head>
          
<div class="warper container-fluid">
<ol class="breadcrumb">
    <li>Apoteker</li>
    <li><a href="<?php echo base_url('/apoteker');?>">Penebusan Resep</a></li>
    <li class="active">Form Proses</li>
</ol>
<div class="page-header"><h1>Form Proses Resep</h1></div>

<div class="row">
            
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Form Pembayaran</div>
            <div class="panel-body">
                    <?php 
                      $attrib = array('class' => 'form-horizontal');
                      $hidden = array('id' => !empty($default->id_c_jenis_obat)? $default->id_c_jenis_obat : '');
                      echo form_open_multipart($action,$attrib,$hidden);?>
                      <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Nama Pasien</label>
                        <div class="col-sm-8">
                          <?php echo form_input('nama',$nama, 'disabled class="form-control col-md-7 col-xs-12" id="nama" ') ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="id_pendaftaran" class="col-sm-2 control-label">Kode Pendaftaran</label>
                        <div class="col-sm-8">
                          <?php echo form_input('data[id_pendaftaran]',$id_pendaftaran, 'readonly class="form-control col-md-7 col-xs-12" id="id_pendaftaran" ') ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="total" class="col-sm-2 control-label">Total Biaya</label>
                        <div class="col-sm-4">
                          <?php echo form_input('data[jumlah]',$total, 'readonly class="form-control col-md-7 col-xs-12" id="total" ') ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="bayar" class="col-sm-2 control-label">Bayar</label>
                        <div class="col-sm-4">
                          <?php 
                          $data = array(
                              'name'        => 'data[bayar]',
                              'id'          => 'bayar',
                              'value'       => '',
                              'type' => 'number',
                              'class' => 'form-control'
                              );

                          echo form_input($data);
                          ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="kembali" class="col-sm-2 control-label">Kembali</label>
                        <div class="col-sm-4">
                          <?php echo form_input('data[kembali]','0', 'readonly class="form-control col-md-7 col-xs-12" id="kembali" ') ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-9">
                          <button type="submit" class="btn btn-primary" id="simpan">Simpan</button>
                          <a href="<?php echo base_url('/admin/jenis_obat'); ?>" class="btn btn-danger">Batal</a>
                        </div>
                      </div>
                    <?php echo form_close();?>
            </div>
        </div>
    </div>
                
</div> 
          
</div>

    <?=js('jquery/jquery.min.js')?>
    <?=js('bootstrap/bootstrap.min.js')?>
    <?=js('app/custom.js')?>
    <?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
    <?=js('plugins/select2/select2.min.js')?>

<script type="text/javascript">
    $(document).ready(function($){
        $('#resep').addClass('active');
        validate();
        $('#nama').on('change paste keyup', validate);
        $('#bayar').on('change paste keyup',function(){
            $('#kembali').val(hitungKembalian());
            validate();
        })
        $('#bayar').focus();
    });

    function validate(){
        if ($('#nama').val().length    >   0 && $('#bayar').val().length    >   0 && $('#kembali').val()>=0) {
            $("#simpan").prop("disabled", false);
        }
        else {
            $("#simpan").prop("disabled", true);
        }
    }

    function hitungKembalian(){
      var a = parseInt($('#total').val());
      var b = parseInt($('#bayar').val());
      var c = b-a;
      return c;
    }
</script>