<head>
    <title>Apoteker</title>
</head>

                
<div class="warper container-fluid">
<ol class="breadcrumb">
    <li>Apoteker</li>
    <li class="actives">Penebusan Resep</li>
</ol>            
<div class="page-header"><h1>Tebus Resep</h1></div>
	    
	    <div class="row">
	       <div class="col-md-12">
	          <h5 class="no-margn tabular">Cari data pendaftaran untuk tebus resep</h5>    
	          <hr>
	          <div class="alert alert-danger alert-dismissible" role="alert" id="hasilZero" style="display:none;">
	             <strong>Data Resep Tidak Ditemukan!</strong> pendaftaran tidak ada atau pendaftaran tidak memiliki resep 
	         </div>
	         <div class="alert alert-success alert-dismissible" role="alert" id="hasilFound" style="display:none;">
	             <strong>Data Ditemukan!</strong> Silahkan mengisi form yang tersedia 
	         </div>
	         <div class="alert alert-danger alert-dismissible" role="alert" id="hasilFoundStatus" style="display:none;">
	             <strong>Pasien Belum Melunasi Pendaftarannya!</strong> Harap melunasi dahulu dibagian registrasi 
	         </div>
	         <form role="form" id="searchnya" method="GET" action="<?php echo base_url('apoteker');?>">
	             <div class="form-group">
	                <div class="form-group">
	                    <input type="hidden" value="<?php echo $status;?>" id="result">
	                    <div class="col-sm-1">
	                        <?php 
	                        $data = array(
	                            'name'        => 'depan',
	                            'id'          => 'depan',
	                            'value'       => 'DA',
	                            'type' => 'text',
	                            'disabled' =>true,
	                            'class' => 'form-control'
	                            );

	                        echo form_input($data);
	                        ?>
	                    </div>
	                    <div class="col-sm-3">
	                      <?php 
	                      $data = array(
	                         'name'        => 'id_pendaftaran',
	                         'id'          => 'id_pendaftaran',
	                         'value'       => '',
	                         'placeholder' => 'Kode Pendaftaran',
	                         'type' => 'number',
	                         'class' => 'form-control'
	                         );

	                      echo form_input($data);
	                      ?>
	                  </div>
	                  <div class="col-sm-2">
	                      <button class="btn btn-success" id="searchRow"><i class="fa fa-search"></i> Cari</button>
	                  </div>
	              </div>
	          </div>
	      </form>	
	  </div>
	</div>

	<div class="row" style="margin-top:5%;">
	    <div class="col-md-12" id="dataResep" style="display:none;">
	        <div class="panel panel-default">
	            <div class="panel-heading">Data Resep </div>
	            <div class="panel-body nicescroll">
	            	<div class="row">
	            	
	            	    <div class="col-md-6 tabular">
	            	    
	            	        <table class="table no-bor">
	            	        	<tr>
	            	        		<td><strong>Data Pendaftaran</strong></td>		
	            	        	</tr>
	            	        	<tr>
	            	        		<td>Nama Pasien</td>		
	            	        		<td>:</td>		
	            	        		<td style="text-transform:capitalize;"><?php echo $gen_info->nama?></td>		
	            	        	</tr>
	            	        	<tr>
	            	        		<td>Tanggal Daftar</td>		
	            	        		<td>:</td>		
	            	        		<td><?php echo nice_date($gen_info->tgl,'d M Y');?></td>		
	            	        	</tr>
	            	        	<tr>
	            	        		<td>Poliklinik</td>		
	            	        		<td>:</td>		
	            	        		<td><?php echo $gen_info->poliklinik;?></td>		
	            	        	</tr>
	            	        	<tr>
	            	        		<td>Dokter</td>		
	            	        		<td>:</td>		
	            	        		<td><?php echo $gen_info->dokter;?></td>		
	            	        	</tr>
	            	        	<tr>
	            	        		<td><strong>Status Pembayaran Resep</strong></td>		
	            	        		<td>:</td>		
	            	        		<td><?php echo ($status_resep->status_pembayaran == 1)? '<span class="label label-success">LUNAS</span>' : '<span class="label label-warning">BELUM LUNAS</span>';?></td>		
	            	        	</tr>
	            	        </table>
	            	        
	            	        
	            	    </div>
	            	    
	            		
	            	</div>
	                <table id="tbl_info" class="table table-striped">
	                  <thead>
	                    <tr>
	                      <th width="1">No</th>
	                      <th>Obat</th>
	                      <th>Dosis</th>
	                      <th>Jumlah</th>
	                    </tr>
	                  </thead>
	                  <tbody>
	                  <?php $no=0; foreach ($resephasilnya as $resep ) { $no++;?>    
	                            <tr>
	                                <td><?php echo $no;?></td>
	                                <td><?php echo $resep->nama_obat;?></td>
	                                <td><?php echo $resep->dosis;?></td>
	                                <td><?php echo $resep->jumlah;?></td>
	                            </tr>
	                            <?php

	                        } ?>
	                  </tbody>
	                  </table>
	                <form method="POST" action="<?php echo base_url().'apoteker/form';?>">
	                <input type="hidden" name="id_pendaftaran" value="<?php echo $gen_info->id_pendaftaran?>">
	                <input type="hidden" name="nama" value="<?php echo $gen_info->nama?>">
	                <input type="hidden" name="total" value="<?php echo $gen_info->total_resep?>">
	                <div class="col-md-12 text-right">
	                	<button class="btn btn-primary" <?php echo ($status_resep->status_pembayaran == 1)? 'style="display:none"' : '' ;?>><i class="fa fa-arrow-circle-right"></i> Proses Resep</button>
                    </div>
                    </form>
	            </div>
	        </div>
	    </div>
	</div>
</div>
        
<?=js('jquery/jquery.min.js')?>
<?=js('bootstrap/bootstrap.min.js')?>
<?=js('app/custom.js')?>
<?=js('plugins/underscore/underscore-min.js')?> 
<?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>

<script type="text/javascript">
	$(document).ready(function(){
		var status = $('#result').val();
		hasilZero(status);
		$('#resep').addClass('active');
	});

	function hasilZero(status){
       if(status ==0){
          $('#hasilZero').css('display','inherit');
          $('#id_pendaftaran').focus();
      }
      else if(status ==1){
          $('#hasilFound').css('display','inherit');
          $('#dataResep').css('display','inherit');
      }
      else if(status == 2){
       	  $('#id_pendaftaran').focus();
      }
      else if(status == 3){
           $('#hasilFoundStatus').css('display','inherit');
           $('#id_pendaftaran').focus();   
      }
}
</script>