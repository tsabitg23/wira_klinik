<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apoteker extends MX_Controller {

	private $_module;

	public function __construct() {
        parent::__construct();

        //set Module location
        $this->_level = 'apoteker';
        $this->_module = 'apoteker';
        $this->_header = 'layout/header_'.$this->_module;
        $this->_footer = 'layout/footer';
        
        $this->load->model('resep_model','resep');
        $this->load->model('pembayaran_model','peem');
        $this->load->helper('date');
    }
    public function cekstatuslogin() {
        if ($this->session->has_userdata('uname') == FALSE) {
            redirect(base_url());
        }
        elseif ($this->session->has_userdata('uname') == true && $this->session->userdata('level') != $this->_level) {
            redirect(base_url('/'.$this->session->userdata('level')));
        }
    }

	public function index()
	{	
        $this->cekstatuslogin();
        $data['resephasilnya'][0] = new stdClass();
        $data['resephasilnya'][0]->nama_obat = '123';
        $data['resephasilnya'][0]->dosis = '123';
        $data['resephasilnya'][0]->jumlah = '123';

        $data['gen_info'] = new stdClass();
        $data['gen_info']->nama = '123';
        $data['gen_info']->tgl = '123';
        $data['gen_info']->poliklinik = '123';
        $data['gen_info']->dokter = '123';

        $data['status_resep'] = new stdClass();
        $data['status_resep']->status_pembayaran = '123';

        $data['status'] = 2;
        $id_pendaftaran = 'DA'.$this->input->get('id_pendaftaran');
        if($this->input->get('id_pendaftaran') != ''){
            $result = $this->resep->getData($id_pendaftaran);
            if($result->num_rows() > 0){
                if($result->row()->status_ambil_resep){
                    if($result->row()->total_resep > 0){
                        $data['status'] = 1;
                        $data['resephasilnya'] = $this->resep->getDataResep($id_pendaftaran)->result();
                        $data['status_resep'] = $this->resep->getStatusResep($id_pendaftaran)->row();
                        $data['gen_info'] = $result->row();
                    }
                    else{
                        $data['status'] = 0;
                    }
                }
                else{
                    $data['status'] = 3;
                }
            }
            else{
                $data['status'] = 0;
            }
        }
		$this->load->view($this->_header);
		$this->load->view($this->_module.'/index',$data);
		$this->load->view($this->_footer);
		
	}

    public function form(){
        $this->cekstatuslogin();
        $data['id_pendaftaran'] = $this->input->post('id_pendaftaran');
        $data['nama'] = $this->input->post('nama');
        $data['total'] = $this->input->post('total');
        $data['action'] = $this->_module.'/save';
        $this->load->view($this->_header);
        $this->load->view($this->_module.'/form',$data);
        $this->load->view($this->_footer);
           
    }

    public function save(){
            
        $this->db->trans_begin();
        $dataIns   = $this->input->post("data");
        $dataIns['id_admin'] = $this->session->userdata('id');

            $this->resep->save_as_new($dataIns);
        
        if ($this->db->trans_status() === FALSE ) {
            $this->db->trans_rollback();
            echo "<script>
            alert('Data tidak tersimpan karna kesalahan tertentu ');
            window.location.href='".base_url()."apoteker';
            </script>";
        }
        else {
            $this->db->trans_commit();
            echo "<script>
            alert('Data Berhasil Disimpan ');
            window.open('".base_url()."apoteker/struk/".$dataIns['id_pendaftaran']."','_blank');
            window.location.href='".base_url()."apoteker';
            </script>";   
        }
        
    }

    public function struk($id_pendaftaran){
        $data['bayaran'] = $this->peem->getByDaftar('resep',$id_pendaftaran)->row();
        $data['info'] = $this->peem->getViewByDaftar($id_pendaftaran)->row();
        
        $this->load->view($this->_module.'/pdf',$data);
    }


}
?>
