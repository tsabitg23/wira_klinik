<head>
    <title>Kategori Obat - Form</title>
</head>
          
<div class="warper container-fluid">
<ol class="breadcrumb">
    <li>Admin</li>
    <li>Config</li>
    <li><a href="<?php echo base_url('/admin/kat_obat');?>">Kategori Obat</a></li>
    <li class="active">Form</li>
</ol>
<div class="page-header"><h1>Form Kategori Obat</h1></div>

<div class="row">
            
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Tambah / Edit Data</div>
            <div class="panel-body">
                    <?php 
                      $attrib = array('class' => 'form-horizontal');
                      $hidden = array('id' => !empty($default->id_c_kat_obat)? $default->id_c_kat_obat : '');
                      echo form_open_multipart($action,$attrib,$hidden);?>
                      <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Kategori Obat</label>
                        <div class="col-sm-8">
                          <?php echo form_input('data[nama]',!empty($default->nama)? $default->nama : '', 'placeholder="Kategori" class="form-control col-md-7 col-xs-12" id="nama" ') ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-9">
                          <button type="submit" class="btn btn-primary" id="simpan">Simpan</button>
                          <a href="<?php echo base_url('/admin/kat_obat'); ?>" class="btn btn-danger">Batal</a>
                        </div>
                      </div>
                    <?php echo form_close();?>
            </div>
        </div>
    </div>
                
</div> 
          
</div>

        <!-- JQuery v1.9.1 -->
    <?=js('jquery/jquery.min.js')?>
    
    <!-- Bootstrap -->
    <?=js('bootstrap/bootstrap.min.js')?>

    <!-- Custom JQuery -->
    <?=js('app/custom.js')?>
    
   <?=js('plugins/underscore/underscore-min.js')?> 
   
    <!-- NanoScroll -->
    <?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
    <?=js('plugins/select2/select2.min.js')?>
<script type="text/javascript">
    $(document).ready(function($){
        $('#konfigurasi').addClass('active');
        $('#obat_kat').addClass('active');
        
        validate();
        $('#nama').on('change paste keyup', validate);
    });

    function validate(){
        if ($('#nama').val().length    >   0) {
            $("#simpan").prop("disabled", false);
        }
        else {
            $("#simpan").prop("disabled", true);
        }
    }
</script>