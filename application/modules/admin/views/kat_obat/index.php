<head>
    <title>Kategori Obat</title>
</head>

<div class="warper container-fluid">
<ol class="breadcrumb">
    <li>Admin</li>
    <li>Config</li>
    <li class="active">Kategori Obat</li>
</ol>
<div class="page-header"><h1>Kategori Obat</h1></div>

<div class="row">
            
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Data Kategori Obat</div>
                        <div class="panel-body nicescroll">
                        <p style="margin-bottom:20px;"> 
                            <a href="<?php echo base_url('admin/kat_obat/form');?>"><button class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</button></a>
                        </p>

                        
                            <table id="tbl_kat" class="table table-bordered">
                              <thead>
                                <tr>
                                  <th width="1">No</th>
                                  <th>Nama</th>
                                  <th width="25%">Aksi</th>
                                </tr>
                              </thead>
                              <tbody>
                              <?php $no=0; foreach ($kategori_obat as $kat ) { $no++?>
                                <tr>
                                <td><?php echo $no;?></td>
                                <td><?php echo $kat->nama;?></td>
                                <td>
                                    <a href="<?php echo base_url().'admin/kat_obat/form?id='.base64_encode($kat->id_c_kat_obat) ?>">
                                      <button class="btn btn-success btn-xs">Edit</button>
                                    </a>
                                    <a href="<?php echo base_url().'admin/kat_obat/delete?id='.base64_encode($kat->id_c_kat_obat) ?>"><button class="btn btn-danger btn-xs">Delete</button></a>
                                </td>
                                </tr>
                               <?php } ?>   
                              </tbody>
                            </table>
                            
                        
                        </div>
                    </div>
                </div>
                
            </div>            
</div>

    <?=js('jquery/jquery.min.js')?>
    <?=js('bootstrap/bootstrap.min.js')?>
    <?=js('app/custom.js')?>
    <?=js('plugins/underscore/underscore-min.js')?> 
    <?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
    <?=js('plugins/data-table/jquery.dataTables.min.js')?>
    <?=js('plugins/data-table/dataTables.bootstrap.min.js')?>
    <?=js('plugins/select2/select2.min.js')?>    
<script type="text/javascript">
$(document).ready(function($){
    $('#konfigurasi').addClass('active');
    $('#tbl_kat').dataTable();
});
  

</script>

