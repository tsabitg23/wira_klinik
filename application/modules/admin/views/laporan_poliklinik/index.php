<head>
    <title>Dashboard Admin</title>
</head>
          
<div class="warper container-fluid">
<ol class="breadcrumb">
    <li>Admin</li>
    <li class="active">Pendapatan Poliklinik</li>
</ol>
<div class="page-header"><h1>Pendapatan Poliklinik</h1></div>
        <div class="row" style="margin-bottom:4%;">
           <div class="col-md-12">
              <h5 class="no-margn tabular">Cari data pendaftaran untuk tebus resep</h5>    
              <hr>
             <form role="form" id="searchnya" method="GET" action="<?php echo base_url('admin/laporan_poliklinik');?>">
                 <div class="form-group">
                    <div class="form-group">
                        <div class="col-sm-2">
                            <?php 
                            echo form_dropdown('jenis',$jenis_satuan,'','class="form-control choose-select" id="select-jenis"');
                            ?>
                        </div>
                        <div class="col-sm-3" style="display:none;" id="tanggal">
                          <?php 
                          $data = array(
                             'name'        => 'tanggal',
                             'value'       => '',
                             'placeholder' => 'Tanggal',
                             'id'=>'select-tanggal',
                             'type' => 'date',
                             'class' => 'form-control'
                             );

                          echo form_input($data);
                          ?>
                      </div>
                      <div class="col-sm-2" id="bulan" style="display:none;">
                          <?php 
                            echo form_dropdown('tanggal',$bulan,'','class="form-control choose-select" id="select-bulan"');
                            ?>
                      </div>
                      <div class="col-sm-2" id="tahun" style="display:none;">
                          <?php 
                            echo form_input('tahun','','class="form-control" placeholder="Tahun" id="select-tahun"');
                            ?>
                      </div>
                      <div class="col-sm-2">
                          <button class="btn btn-success" id="searchRow"><i class="fa fa-search"></i> Cari</button>
                      </div>
                  </div>
              </div>
          </form>   
      </div>
    </div>

    <span style="float:right;margin-bottom:2%;"><a class="btn btn-warning" id="btnCetak"><i class="fa fa-print"></i> Cetak Laporan</a></span>
    <div id="chart">
    </div>
    <div id="chart2">
    </div>
    <div class="row">
                
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Data Pendapatan Poliklinik <?php echo $tanggalnya;?></div>
                            <div class="panel-body nicescroll">
                                <form target="_blank" method="get" action="<?php echo base_url().'admin/laporan_poliklinik/laporan'?>" id="form-laporan">
                                    <input type="hidden" name="jenis" value="<?php echo $hid_jenis?>">
                                    <input type="hidden" name="tanggal" value="<?php echo $hid_tanggal?>">
                                    <input type="hidden" name="tahun" value="<?php echo $hid_tahun?>">
                                </form>                            
                                <table id="tbl_poli" class="table">
                                  <thead>
                                    <tr>
                                      <th></th>
                                      <th>Jumlah Pasien</th>
                                      <th>Pendapatan</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  <?php $no1=0;$no2=0; foreach ($poliklinik as $poli ) {
                                      $no1+=$poli->jumlah_pasien;
                                      $no2+=$poli->pendapatan;
                                    } ?>   
                                   <tr>
                                     <td>Total</td>
                                     <td id="jmlPasien"><?php echo $no1;?></td>
                                     <td id="jmlPendapatan"><?php echo $no2?></td>
                                   </tr>
                                  </tbody>
                                </table>
                                
                            
                            </div>
                        </div>
                    </div>
                    
                </div>
</div>

    <?=js('jquery/jquery.min.js')?>
    <?=js('bootstrap/bootstrap.min.js')?>
    <?=js('app/custom.js')?>
    <?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
    <?=js('plugins/highcharts/highcharts.js')?>    
    <?=js('plugins/highcharts/data.js')?>
<script type="text/javascript">

jQuery(function(){
     new Highcharts.Chart({
      chart: {
       renderTo: 'chart',
       type: 'column',
      },
      title: {
       text: 'Pendapatan Poliklinik <?php echo $tanggalnya;?>',
       x: -20
      },
      subtitle: {
       text: 'Pendapatan Poliklinik dari hasil biaya layanan <?php echo $tanggalnya;?>',
       x: -20
      },
      xAxis: {
       categories: ['Pendapantan']
      },
      yAxis: {
       title: {
        text: 'Pendapatan (Rp)'
       }
      },
      series: <?php echo json_encode($grafik)?>
     });
    });

jQuery(function(){
     new Highcharts.Chart({
      chart: {
       renderTo: 'chart2',
       type: 'column',
      },
      title: {
       text: 'Jumlah Pasien <?php echo $tanggalnya;?>',
       x: -20
      },
      subtitle: {
       text: 'Jumlah pasien yang datang ke poliklinik untuk berobat <?php echo $tanggalnya;?>',
       x: -20
      },
      xAxis: {
       categories: ['Jumlah Pasien']
      },
      yAxis: {
       title: {
        text: 'Jumlah Pasien (orang)'
       }
      },
      series: <?php echo json_encode($grafik2)?>
     });
    });

	$(document).ready(function(){
		$('#laporan').addClass('active');
        var jenis = $('#select-jenis').val();
        selectNya(jenis);

        $('#select-jenis').change(function(){
            selectNya($(this).val());
        });

        $('#btnCetak').click(function(){
            $('#form-laporan').submit();            
        })
        $('#pendapatan').addClass('active');
	})

    function selectNya(jenis){
        if(jenis == 'date'){
            $('#tanggal').css('display','inherit');
            $('#bulan').css('display','none');
            $('#tahun').css('display','none');
            $('#select-tanggal').prop('disabled',false);
            $('#select-bulan').prop('disabled',true);
            $('#select-tahun').prop('disabled',true);
        }
        else if(jenis == 'month'){
            $('#tanggal').css('display','none');
            $('#bulan').css('display','inherit');
            $('#tahun').css('display','inherit');
            $('#select-tanggal').prop('disabled',true);
            $('#select-bulan').prop('disabled',false);
            $('#select-tahun').prop('disabled',false);
        }
        else if(jenis == 'year'){
            $('#tanggal').css('display','none');
            $('#bulan').css('display','none');
            $('#tahun').css('display','inherit');
            $('#select-tanggal').prop('disabled',true);
            $('#select-bulan').prop('disabled',true);
            $('#select-tahun').prop('disabled',false);
        }

    }
</script>
