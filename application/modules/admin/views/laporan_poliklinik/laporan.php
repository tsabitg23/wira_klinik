<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="icon" type="image/png" href="<?php echo base_url('/assets/img/logo/icon.png');?>">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?=css('font/bariol.css');?>
	<?=css('bootstrap/bootstrap.css');?> 
	<?=css('font/roboto.css');?>
	<?=css('main/main.css');?>
</head>
<body onload="window.print()">


	<div class="row">
		<div class="col-md-12" id="dataResep">
			<div class="panel panel-default">
				<div class="panel-body nicescroll">
					<div class="row">
						<div class="col-lg-4 col-lg-offset-4">
							<h3 class="text-center"><?=img('logo/logobluesmall.png')?></h3>
						</div>
						<div class="col-md-6 tabular">

							<table class="table no-bor">
								<tr>
									<td><strong>Data Pendapatan Poliklinik</strong></td>		
								</tr>
								<tr>
									<td><?php
									if($jenis == 'date'){
										echo "Tanggal";
									}
									else if($jenis == 'month'){
										echo "Bulan";
									}
									else if($jenis == 'year'){
										echo "Tahun";
									}
									 ?></td>		
									<td>:</td>		
									<td><?php echo $tanggalnya;?></td>		
								</tr>
							</table>

						</div>
						<table id="tbl_poli" class="table">
                                  <thead>
                                    <tr>
                                      <th width="1">No</th>
                                      <th>Nama</th>
                                      <th>Jumlah Pasien</th>
                                      <th>Pendapatan</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  <?php $no=0; foreach ($poliklinik as $poli ) { $no++?>
                                    <tr>
                                    <td><?php echo $no;?></td>
                                    <td><?php echo $poli->nama;?></td>
                                    <td><?php echo $poli->jumlah_pasien;?></td>
                                    <td><?php echo $poli->pendapatan;?></td>
                                    </tr>
                                   <?php } ?>   
                                  </tbody>
                                </table>


					</div>
					</div>
				</div>
			</div>
		</div>

	</body>
	</html>

	<?=js('jquery/jquery.min.js')?>
	<?=js('bootstrap/bootstrap.min.js')?>
	<?=js('app/custom.js')?>

	<script type="text/javascript">

	</script>