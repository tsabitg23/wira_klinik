<head>
  <title>Dokter - Form</title>
</head>

<div class="warper container-fluid">
  <ol class="breadcrumb">
    <li>Admin</li>
    <li><a href="<?php echo base_url('/admin/dokter');?>">Dokter</a></li>
    <li class="active">Form</li>
  </ol>
  <div class="page-header"><h1>Form Dokter</h1></div>

  <div class="row">

    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">Tambah Data</div>
        <div class="panel-body">
          <?php 
          $attrib = array('class' => 'form-horizontal','id' =>'form-dokter');
          $hidden = array('id' => !empty($default->id_dokter)? $default->id_dokter : '','jenis'=>'add');
          echo form_open_multipart($action,$attrib,$hidden);?>
          <div class="form-group">
            <label for="nama" class="col-sm-2 control-label">Nama</label>
            <div class="col-sm-8">
              <?php echo form_input('data[nama]',!empty($default->nama)? $default->nama : '', 'placeholder="Nama Dokter" class="form-control col-md-7 col-xs-12" id="nama" ') ?>
            </div>
          </div>
          <div class="form-group">
            <label for="poli" class="col-sm-2 control-label">Poliklinik</label>
            <div class="col-sm-2">
              <?php echo form_dropdown('data[id_poliklinik]',$poli,'', 'class="form-control choose-select" id="poliklinik" ') ?>
            </div>
          </div>
          <div class="form-group">
            <label for="spesialis" class="col-sm-2 control-label">Spesialis</label>
            <div class="col-sm-2">
              <?php echo form_dropdown('data[id_c_spesialis]',$spesialis,'', 'class="form-control choose-select" id="spesialis" ') ?>
            </div>
          </div>
          <div class="form-group" >
            <label for="alamat" class="col-sm-2 control-label">Alamat</label>
            <div class="col-sm-8">
            <?php 
            $data = array(
                  'name'        => 'data[alamat]',
                  'id'          => 'alamat',
                  'value'       => !empty($default->alamat)? $default->alamat : '',
                  'rows'        => '5',
                  'cols'        => '40',
                  'placeholder' => 'Alamat Dokter',
                  'class' => 'form-control'
                );

              echo form_textarea($data);
            ?>
            </div>
          </div>
          <div class="form-group" id="tel">
            <label class="col-sm-2 control-label">Telepon</label>
            <div class="col-sm-8">
              <?php echo form_input('data[telepon]',!empty($default->telepon)? $default->telepon : '', 'placeholder="Nomor Telepon Dokter" class="form-control col-md-7 col-xs-12" id="telepon" ') ?>
              <span class="help-block" id="tel-note">* Telepon hanya boleh berisi angka</span>
            </div>
          </div>
          <div class="form-group" id="gaj">
            <label class="col-sm-2 control-label">Gaji</label>
            <div class="col-sm-8">
              <?php
              echo form_input('data[gaji]',!empty($default->telepon)? $default->telepon : '', 'placeholder="Gaji Dokter" class="form-control col-md-7 col-xs-12" id="gaji"')
              ?>
              <span class="help-block" id="gaji-note">* Gaji Hanya boleh berisi angka</span>
            </div>
          </div>
          <input type="hidden" id="dataJad" name="dataJad" value="">
          <div class="form-group">
            <label for="jadwal" class="col-sm-2 control-label">Jadwal</label>
            <div class="col-sm-2">
              <?php echo form_dropdown('jadwal',$hari,'', 'class="form-control choose-select" id="jadwal" ') ?>
              <span class="help-block"><small>* Input jadwal dapat dilewati</small></span>
            </div>
            <div class="col-sm-4" id="har">
              <div class='input-group date' id="datetimepicker" >
                <?php echo form_input('mulai','', 'class="form-control" id="mulai" placeholder="Praktik Mulai"') ?>
                <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
              </span>
              <?php echo form_input('selesai','', 'class="form-control" id="selesai" placeholder="Praktik Selesai"') ?>
            </div>
          </div>
          <div class="col-sm-2">
            <a class="btn btn-success" id="addRow">Tambah</a>
          </div>
        </div>
        <table id="tbl_jad" class="table table-bordered" style="width:60%;margin:20px 17%;">
          <thead>
            <th>Hari</th>
            <th>Mulai</th>
            <th>Selesai</th>
            <th width="5%">Aksi</th>
          </thead>
          <tbody>
          </tbody>
        </table>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-9">
            <a type="submit" class="btn btn-primary" id="simpan">Simpan</a>
            <a href="<?php echo base_url('/admin/dokter'); ?>" class="btn btn-danger">Batal</a>
          </div>
        </div>
        <?php echo form_close();?>
      </div>
    </div>
  </div>

</div> 

</div>

<?=js('jquery/jquery.min.js')?>
<?=js('bootstrap/bootstrap.min.js')?>
<?=js('app/custom.js')?>
<?=js('plugins/underscore/underscore-min.js')?> 
<?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
<?=js('plugins/data-table/jquery.dataTables.min.js')?>
<?=js('plugins/data-table/dataTables.bootstrap.min.js')?>
<?=js('plugins/select2/select2.min.js')?>
<?=js('plugins/tabletojson/jquery.tabletojson.js')?>
<?=js('plugins/meiomask/meiomask.min.js')?>

<script type="text/javascript">
  var benar = false;
  function checkGaji(){
    var gaji = $('#gaji').val();
    var checko = $.isNumeric(gaji);
    if(checko== false){
      $('#gaji-note').show();
      $('#gaj').addClass('has-error');
      benar = false;
    }
    else if(checko == true){
     $('#gaji-note').hide();
     $('#gaj').removeClass('has-error');
     benar  = true;
   }
 }
 function checkTelepon(){
  var telepon = $('#telepon').val();
    var checko = $.isNumeric(telepon);
    if(checko== false){
      $('#tel-note').show();
      $('#tel').addClass('has-error');
      benar = false;
    }
    else if(checko == true){
     $('#tel-note').hide();
     $('#tel').removeClass('has-error');
     benar  = true;
   }
 }

 $(document).ready(function($){   
  $('.help-block').hide();

  $('#addRow').on('click', function () {
    var data = '<tr><td>'+$('#select2-jadwal-container').text()+'</td><td>'+$('#mulai').val()+'</td><td>'+$('#selesai').val()+'</td><td><a class="btn btn-danger btn-xs hapus-dong"><i class="fa fa-close"></i></a></td></tr>';
    $('#tbl_jad').append(data);
              $('#jadwal').val("").trigger('change');
              $('#mulai').val("");
              $('#selesai').val("");
              $('#jadwal').focus();
            } );
  $('#tbl_jad').on('click','a.hapus-dong',function(){
    $(this).parent().parent().remove();
  })
  $('#simpan').on('click',function(){
    var table = $('#tbl_jad').tableToJSON({
      ignoreColumns : [3]
    });
    var url   = "<?php echo base_url('/admin/dokter/save')?>";

    $('#dataJad').val(JSON.stringify(table));
    $('#form-dokter').submit();
  });

  $('#gaji').on('change paste keyup', function() {
    checkGaji();
  });
  $('#telepon').on('change paste keyup', function() {
    checkTelepon();
  });
  $('#dokter').addClass('active'); 
  $('#nama,#alamat,#gaji,#telepon').on('change paste keyup', validate);
  $('#mulai,#selesai').on('change paste keyup', validate2);
  $('.choose-select').select2();
  validate();
  validate2();
  $("#mulai,#selesai").setMask("29:59")
  .keypress(function() {
    var currentMask = $(this).data('mask').mask;
    var newMask = $(this).val().match(/^2.*/) ? "23:59" : "29:59";
    if (newMask != currentMask) {
      $(this).setMask(newMask);
    }
  });
});

 function validate(){
  if ($('#nama').val().length    >   0 && $('#alamat').val().length    >   0 && $('#telepon').val().length    >   0 && $('#gaji').val().length    >   0 && benar== true) {
    $("#simpan").removeClass("disabled");
  }
  else {
    $("#simpan").addClass("disabled");
  }
}

function validate2(){
  if ($('#mulai').val().length    >   0 && $('#selesai').val().length    >   0 ) {
    $("#addRow").removeClass("disabled");
  }
  else {
    $("#addRow").addClass("disabled");
  }
}

</script>