<head>
  <title>Dokter - Detail</title>
</head>

<div class="warper container-fluid">
  <ol class="breadcrumb">
    <li>Admin</li>
    <li><a href="<?php echo base_url('/admin/dokter');?>">Dokter</a></li>
    <li class="active" style="text-transform:capitalize;"><?php echo $default->nama;?></li>
  </ol>
  <div class="page-header" style="text-transform:capitalize;"><h1><?php echo $default->nama;?> <small><a href="<?php echo base_url('/admin/dokter');?>"><button class="btn btn-primary btn-xs"><i class="fa fa-arrow-left"></i> back</button></a></small></h1></div>

  <div class="row">
  <div class="col-md-12" id="info">
      <div class="panel panel-default">
        <div class="panel-heading">Info Dokter</div>
        <div class="panel-body nicescroll">
          <table id="tbl_info" class="table">
            <thead>
              <tr>
                <th>Data</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
            <tr>
              <td>Nama</td>
              <td ><?php echo $default->nama;?></td>
            </tr>
            <tr>
              <td>Poliklinik</td>
              <td ><?php echo $default->poliklinik;?></td>
            </tr>
            <tr>
              <td>Spesialis</td>
              <td ><?php echo $default->spesialis;?></td>
            </tr>
            <tr>
              <td>Alamat</td>
              <td ><?php echo $default->alamat;?></td>
            </tr>
            <tr>
              <td>Telepon</td>
              <td ><?php echo $default->telepon;?></td>
            </tr>
            <tr>
              <td>Gaji</td>
              <td ><?php echo $default->gaji;?></td>
            </tr>
            </tbody>
            </table>
          </div>
        </div>
      </div>
  </div>

  <div class="row">
  <div class="col-md-12" id="jad">
      <div class="panel panel-default">
        <div class="panel-heading">Info Jadwal</div>
        <div class="panel-body nicescroll">
        <?php 
        echo form_open($action);
        ?>
          <div class="form-group" style="margin-bottom:50px;">
            <label for="hari" style="margin-left:2%;"><span id="tekusu">Tambah</span> Data</label><br>
            <div class="col-sm-2">
              <?php echo form_dropdown('hari',$hari,'', 'class="form-control choose-select" id="hari" ') ?>
            </div>
           <input type="hidden" name="id_d" value="<?php echo $default->id_dokter;?>">
           <input type="hidden" id="idJad" name="id_d_j" value="<?php echo base64_encode('coy');?>">
            <div class="col-sm-5" id="har">
              <div class='input-group date' id="datetimepicker" >
                <?php echo form_input('data[praktik_mulai]','', 'class="form-control" id="mulai" placeholder="Praktik Mulai"') ?>
                <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
              </span>
              <?php echo form_input('data[praktik_selesai]','', 'class="form-control" id="selesai" placeholder="Praktik Selesai"') ?>
            </div>
          </div>
          <div class="col-sm-4">
              <button class="btn btn-primary" id="addRow">Tambah</button>
              <a class="btn btn-danger" style="visibility:hidden;" id="cancel" onclick="clearForm()">Batal</a>
            </div>
          </div>
        <?php echo form_close();?>
          <table id="tbl_jad" class="table">
            <thead>
              <tr>
                <th width="1%">No</th>
                <th>Hari</th>
                <th>Praktik Mulai</th>
                <th>Praktik Selesai</th>
                <th width="15%">Aksi</th>
              </tr>
            </thead>
            <tbody>
            <?php $no=0; foreach ($jadwal as $jad ) { $no++?>
            <?php
                   $a;
                  switch ($jad->hari) {
                    case 'Monday':
                        $a = 'Senin';
                        break;
                    case 'Tuesday':
                        $a = 'Selasa';
                        break;
                    case 'Wednesday':
                        $a = 'Rabu';
                        break;
                    case 'Thursday':
                        $a = 'Kamis';
                        break;
                    case 'Friday':
                        $a = 'Jumat';
                        break;
                    case 'Saturday':
                        $a = 'Sabtu';
                        break;
                    
                    default:
                        $a = 'Minggu';
                        break;
                }
                  ?>    
                <tr>
                  <td><?php echo $no;?></td>
                  <td><?php echo $a;?></td>
                  <td><?php echo date('H:i',strtotime($jad->praktik_mulai));?></td>
                  <td><?php echo date('H:i',strtotime($jad->praktik_selesai));?></td>
                  <td>
                    <a class="editRow"><button class="btn btn-success btn-xs"><i class="fa fa-edit"></i></button></a>
                    <a href="<?php echo base64_encode($jad->id_d_jadwal)?>"></a>
                    <a class="deleteRow" href="<?php echo base_url().'admin/dokter/deletejad?id='.base64_encode($jad->id_d_jadwal).'&&id_d='.base64_encode($default->id_dokter)?>"><button class="btn btn-danger btn-xs"><i class="fa fa-close"></i></button></a>
                  </td>
                </tr>
                <?php } ?>   

              </tbody>
            </table>
          </div>
        </div>
      </div>
  </div>      

  </div>

  <?=js('jquery/jquery.min.js')?>
  <?=js('bootstrap/bootstrap.min.js')?>
  <?=js('app/custom.js')?>
  <?=js('plugins/underscore/underscore-min.js')?> 
  <?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
  <?=js('plugins/data-table/jquery.dataTables.min.js')?>
  <?=js('plugins/data-table/dataTables.bootstrap.min.js')?>
  <?=js('plugins/select2/select2.min.js')?>
  <?=js('plugins/meiomask/meiomask.min.js')?>

  <script type="text/javascript">
    $(document).ready(function($){
      $('#tbl_jad').on('click','a.editRow',function(){
         $('#addRow').removeClass('btn-primary');
         $('#addRow').addClass('btn-success');
         $('#addRow').text('Simpan');
         $('#tekusu').text('Edit');
         $('#cancel').css("visibility",'visible');
         changeValue($(this).parent().prev().prev().prev().text());
         $('#mulai').val($(this).parent().prev().prev().text());
         $('#selesai').val($(this).parent().prev().text());
         $('#idJad').val($(this).next().attr('href'));
         $('#hari').focus();
         console.log($('#idJad').val());
         validate();
      });
      validate();
      $('#dokter').addClass('active');
      $('.choose-select').select2();
      $('#mulai,#selesai').on('change paste keyup', validate);
      $("#mulai,#selesai").setMask("29:59")
      .keypress(function() {
        var currentMask = $(this).data('mask').mask;
        var newMask = $(this).val().match(/^2.*/) ? "23:59" : "29:59";
        if (newMask != currentMask) {
          $(this).setMask(newMask);
        }
      });

    });

    function clearForm(){
        $('#layanan').val("");
        $('#harga').val("");
        $('#addRow').removeClass('btn-success');
        $('#addRow').addClass('btn-primary');
        $('#addRow').text('Tambah');
        $('#tekusu').text('Tambah');
        $('#cancel').css("visibility",'hidden');   
    }
    function validate(){
        if ($('#mulai').val().length    >   0 && $('#selesai').val().length    >   0) {
            $("#addRow").prop("disabled", false);
        }
        else {
            $("#addRow").prop("disabled", true);
        }
    }

    function changeValue(day){
        $('#hari').val(day).trigger('change');
    }
  </script>

