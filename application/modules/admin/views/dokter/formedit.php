<head>
    <title>Dokter - Form</title>
</head>
          
<div class="warper container-fluid">
<ol class="breadcrumb">
    <li>Admin</li>
    <li><a href="<?php echo base_url('/admin/dokter');?>">Dokter</a></li>
    <li class="active">Form Edit</li>
</ol>
<div class="page-header"><h1>Form Edit Dokter <small>Untuk edit jadwal, klik lihat di <a href="<?php echo base_url('/admin/dokter');?>">Dokter</a></small></h1></div>

<div class="row">
            
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Edit Data</div>
            <div class="panel-body">
                    <?php 
                      $attrib = array('class' => 'form-horizontal','id' =>'form-poli');
                      $hidden = array('id' => !empty($default->id_dokter)? $default->id_dokter : '','jenis'=>'edit');
                      echo form_open_multipart($action,$attrib,$hidden);?>
                      <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Nama</label>
                        <div class="col-sm-8">
                          <?php echo form_input('data[nama]',!empty($default->nama)? $default->nama : '', 'placeholder="Nama Dokter" class="form-control col-md-7 col-xs-12" id="nama" ') ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="poli" class="col-sm-2 control-label">Poliklinik</label>
                        <div class="col-sm-2">
                          <?php echo form_dropdown('data[id_poliklinik]',$poli,!empty($default->id_poliklinik)? $default->id_poliklinik : '', 'class="form-control choose-select" id="poliklinik" ') ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="spesialis" class="col-sm-2 control-label">Spesialis</label>
                        <div class="col-sm-2">
                          <?php echo form_dropdown('data[id_c_spesialis]',$spesialis,!empty($default->id_c_spesialis)? $default->id_c_spesialis : '', 'class="form-control choose-select" id="spesialis" ') ?>
                        </div>
                      </div>
                      <div class="form-group" >
                        <label for="alamat" class="col-sm-2 control-label">Alamat</label>
                        <div class="col-sm-8">
                        <?php 
                        $data = array(
                              'name'        => 'data[alamat]',
                              'id'          => 'alamat',
                              'value'       => !empty($default->alamat)? $default->alamat : '',
                              'rows'        => '5',
                              'cols'        => '40',
                              'placeholder' => 'Alamat Dokter',
                              'class' => 'form-control'
                            );

                          echo form_textarea($data);
                        ?>
                        </div>
                      </div>
                      <div class="form-group" id="tel">
                        <label class="col-sm-2 control-label">Telepon</label>
                        <div class="col-sm-8">
                          <?php 
                          $data = array(
                                'name'        => 'data[telepon]',
                                'id'          => 'telepon',
                                'value'       => !empty($default->telepon)? $default->telepon : '',
                                'placeholder' => 'Nomor telepon dokter',
                                'class' => 'form-control col-md-7 col-xs-12',
                                'type' => 'number'
                              );

                            echo form_input($data);
                          ?>
                        </div>
                      </div>
                      <div class="form-group" id="gaj">
                        <label class="col-sm-2 control-label">Gaji</label>
                        <div class="col-sm-8">
                          <?php 
                          $data = array(
                                'name'        => 'data[gaji]',
                                'id'          => 'gaji',
                                'value'       => !empty($default->gaji)? $default->gaji : '',
                                'placeholder' => 'Gaji Dokter',
                                'class' => 'form-control col-md-7 col-xs-12',
                                'type' => 'number'
                              );

                            echo form_input($data);
                          ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-9">
                          <button type="submit" class="btn btn-primary" id="simpan">Simpan</button>
                          <a href="<?php echo base_url('/admin/dokter'); ?>" class="btn btn-danger">Batal</a>
                        </div>
                      </div>
                    <?php echo form_close();?>
            </div>
        </div>
    </div>
                
</div> 
          
</div>

    <?=js('jquery/jquery.min.js')?>
    <?=js('bootstrap/bootstrap.min.js')?>
    <?=js('app/custom.js')?>
    <?=js('plugins/underscore/underscore-min.js')?> 
    <?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
    <?=js('plugins/data-table/dataTables.bootstrap.min.js')?>
    <?=js('plugins/select2/select2.min.js')?>

<script type="text/javascript">
    $(document).ready(function($){   
        
        $('#dokter').addClass('active'); 
        $('#nama,#alamat,#telepon,#gaji').on('change paste keyup', validate);
        validate();
        $('#poliklinik,#spesialis').on('change',function(){
            if($(this).val() == 'kosong'){
                $("#simpan").prop("disabled", true);
            }
            else{
                validate();
            }
        })
        $('.choose-select').select2();
    });

    function validate(){
   if ($('#nama').val().length    >   0 && $('#alamat').val().length    >   0 && $('#telepon').val().length    >   0 && $('#gaji').val().length    >   0 ) {
            $("#simpan").prop("disabled", false);
        }
        else {
            $("#simpan").prop("disabled", true);
        }
    }
</script>