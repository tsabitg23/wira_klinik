<head>
    <title>Dokter</title>
</head>

<div class="warper container-fluid">
<ol class="breadcrumb">
    <li>Admin</li>
    <li class="active">Dokter</li>
</ol>
<div class="page-header"><h1>Dokter</h1></div>

<div class="row">
            
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Data Dokter</div>
                        <div class="panel-body nicescroll">
                        <p style="margin-bottom:20px;"> 
                            <a href="<?php echo base_url('admin/dokter/form');?>"><button class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</button></a>
                        </p>

                        
                            <table id="tbl_dokter" class="table table-bordered">
                              <thead>
                                <tr>
                                  <th width="1">No</th>
                                  <th>Nama</th>
                                  <th>Poliklinik</th>
                                  <th>Spesialis</th>
                                  <th width="25%">Aksi</th>
                                </tr>
                              </thead>
                              <tbody>
                              <?php $no=0; foreach ($dokter as $dok ) { $no++?>
                                <tr>
                                <td><?php echo $no;?></td>
                                <td><?php echo $dok->nama;?></td>
                                <td><?php echo $dok->poliklinik;?></td>
                                <td><?php echo $dok->spesialis;?></td>
                                <td>
                                    <a href="<?php echo base_url().'admin/dokter/detail?id='.base64_encode($dok->id_dokter) ?>">
                                      <button class="btn btn-info btn-xs">Lihat</button>
                                    </a>
                                    <a href="<?php echo base_url().'admin/dokter/formedit?id='.base64_encode($dok->id_dokter) ?>">
                                      <button class="btn btn-success btn-xs">Edit</button>
                                    </a>
                                    <a href="<?php echo base_url().'admin/dokter/delete?id='.base64_encode($dok->id_dokter) ?>"><button class="btn btn-danger btn-xs">Delete</button></a>
                                </td>
                                </tr>
                               <?php } ?>   
                              </tbody>
                            </table>
                            
                        
                        </div>
                    </div>
                </div>
                
            </div>            
</div>

    <?=js('jquery/jquery.min.js')?>
    <?=js('bootstrap/bootstrap.min.js')?>
    <?=js('app/custom.js')?>
    <?=js('plugins/underscore/underscore-min.js')?> 
    <?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
    <?=js('plugins/data-table/jquery.dataTables.min.js')?>
    <?=js('plugins/data-table/dataTables.bootstrap.min.js')?>   
<script type="text/javascript">
$(document).ready(function($){
    $('#tbl_dokter').dataTable();
});
  

</script>

