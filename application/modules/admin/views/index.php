<head>
    <title>Dashboard Admin</title>
</head>
          
<div class="warper container-fluid">
<ol class="breadcrumb">
    <li>Admin</li>
    <li class="active">Dashboard</li>
</ol>
<div class="page-header"><h1>Dashboard</h1></div>
		<div class="row">
	    	<div class="col-lg-3">
	        	<div class="panel panel-default">
	            	<div class="panel-body text-center">
	                	<h5 class="no-margn tabular"><strong>Jumlah Pasien Hari Ini</strong></h5>
	                    <p class="tabular"><?php echo $daily_pasien.' Orang';?></p>
	                    <div id="dashboard-stats-sparkline5"></div>
	                </div>
	            </div>
	        </div>
	        <div class="col-lg-3">
	        	<div class="panel panel-default">
	            	<div class="panel-body text-center">
	                	<h5 class="no-margn tabular"><strong>Jumlah Pasien Bulan ini</strong></h5>
	                    <p class="tabular"><?php echo $monthly_pasien.' Orang';?></p>
	                    <div id="dashboard-stats-sparkline6"></div>
	                </div>
	            </div>
	        </div>
	        <div class="col-lg-3">
	        	<div class="panel panel-default">
	            	<div class="panel-body text-center">
	                	<h5 class="no-margn tabular"><strong>Pendapatan Hari Ini</strong></h5>
	                    <p class="tabular"><?php echo 'Rp.'.number_format($daily_income);?></p>
	                    <div id="dashboard-stats-sparkline5"></div>
	                </div>
	            </div>
	        </div>
	        <div class="col-lg-3">
	        	<div class="panel panel-default">
	            	<div class="panel-body text-center">
	                	<h5 class="no-margn tabular"><strong>Pendapatan Bulan Ini</strong></h5>
	                    <p class="tabular"><?php echo 'Rp.'.number_format($monthly_income);?></p>
	                    <div id="dashboard-stats-sparkline6"></div>
	                </div>
	            </div>
	        </div>
	    </div>

	    <span style="float:right;margin-bottom:2%;"><a class="btn btn-info" id="btnCetak" href="<?php echo base_url().'admin/laporan_poliklinik'?>"><i class="fa fa-eye"></i> Lihat Lebih Banyak</a></span>
	    <div id="chart">
	    </div>
	    <div id="chart2">
	    </div>
</div>

    <?=js('jquery/jquery.min.js')?>
    <?=js('bootstrap/bootstrap.min.js')?>
    <?=js('app/custom.js')?>
    <?=js('plugins/underscore/underscore-min.js')?> 
    <?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
    <?=js('plugins/highcharts/highcharts.js')?>

<script type="text/javascript">
	jQuery(function(){
	     new Highcharts.Chart({
	      chart: {
	       renderTo: 'chart',
	       type: 'column',
	      },
	      title: {
	       text: 'Pendapatan Poliklinik <?php echo $tanggalnya;?>',
	       x: -20
	      },
	      subtitle: {
	       text: 'Pendapatan Poliklinik dari hasil biaya layanan <?php echo $tanggalnya;?>',
	       x: -20
	      },
	      xAxis: {
	       categories: ['Pendapantan']
	      },
	      yAxis: {
	       title: {
	        text: 'Pendapatan (Rp)'
	       }
	      },
	      series: <?php echo json_encode($grafik)?>
	     });
	    });

	jQuery(function(){
	     new Highcharts.Chart({
	      chart: {
	       renderTo: 'chart2',
	       type: 'column',
	      },
	      title: {
	       text: 'Jumlah Pasien <?php echo $tanggalnya;?>',
	       x: -20
	      },
	      subtitle: {
	       text: 'Jumlah pasien yang datang ke poliklinik untuk berobat <?php echo $tanggalnya;?>',
	       x: -20
	      },
	      xAxis: {
	       categories: ['Jumlah Pasien']
	      },
	      yAxis: {
	       title: {
	        text: 'Jumlah Pasien (orang)'
	       }
	      },
	      series: <?php echo json_encode($grafik2)?>
	     });
	    });
</script>