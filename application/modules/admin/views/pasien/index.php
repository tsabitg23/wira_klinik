<head>
    <title>Pasien</title>
</head>

<div class="warper container-fluid">
<ol class="breadcrumb">
    <li>Admin</li>
    <li class="active">Data Pasien</li>
</ol>
<div class="page-header"><h1>Data Pasien</h1></div>

<div class="row">
            
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Data Pasien Terdaftar</div>
                        <div class="panel-body nicescroll">
                        
                            <table id="tbl_pasien" class="table table-bordered">
                              <thead>
                                <tr>
                                  <th width="1">No</th>
                                  <th>Nama</th>
                                  <th>Jenis Kelamin</th>
                                  <th>Alamat</th>
                                  <th>Tanggal Lahir</th>
                                  <th>Telepon</th>
                                  <th width="25%">Aksi</th>
                                </tr>
                              </thead>
                              <tbody>
                              <?php $no=0; foreach ($pasien as $pas ) { $no++?>
                                <tr>
                                <td><?php echo $no;?></td>
                                <td><?php echo $pas->nama_depan." ".$pas->nama_belakang;?></td>
                                <td><?php echo ($pas->jenis_kelamin=='L')?'Laki-laki':'perempuan';?></td>
                                <td><?php echo $pas->alamat;?></td>
                                <td><?php echo nice_date($pas->tgl_lahir,'d M Y');?></td>
                                <td><?php echo $pas->telepon;?></td>
                                <td width="10%">
                                    <a href="<?php echo base_url().'admin/pasien/delete?id='.base64_encode($pas->id_pasien) ?>"><button class="btn btn-danger btn-xs">Delete</button></a>
                                </td>
                                </tr>
                               <?php } ?>   
                              </tbody>
                            </table>
                            
                        
                        </div>
                    </div>
                </div>
                
            </div>            
</div>

    <?=js('jquery/jquery.min.js')?>
    <?=js('bootstrap/bootstrap.min.js')?>
    <?=js('app/custom.js')?>
    <?=js('plugins/underscore/underscore-min.js')?> 
    <?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
    <?=js('plugins/data-table/jquery.dataTables.min.js')?>
    <?=js('plugins/data-table/dataTables.bootstrap.min.js')?>
    <?=js('plugins/select2/select2.min.js')?>    
<script type="text/javascript">
$(document).ready(function($){
    $('#tbl_pasien').dataTable({responsive:true});
});
  

</script>

