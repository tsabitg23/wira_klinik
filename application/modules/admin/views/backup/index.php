<head>
    <title>Backup Database</title>
</head>
          
<div class="warper container-fluid">
<ol class="breadcrumb">
    <li>Admin</li>
    <li class="active">Backup Database</li>
</ol>
<div class="page-header"><h1>Backup Database</h1></div>
	    <div class="row">
	       <div class="col-md-12">
	          <h5 class="no-margn tabular">Klik Untuk Backup Database</h5>    
	          <hr>
	          <form role="form" id="searchnya" method="GET">
	             <div class="form-group">
	                <div class="form-group">
	                   
	                  <div class="col-sm-2">
	                      <a class="btn btn-success" id="searchRow" href="<?php echo base_url().'admin/backup/do_backup';?>"><i class="fa fa-download"></i> Backup!</a>
	                  </div>
	              </div>
	          </div>
	      </form>	
	  </div>
	</div>

</div>

    <?=js('jquery/jquery.min.js')?>
    <?=js('bootstrap/bootstrap.min.js')?>
    <?=js('app/custom.js')?>
    <?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
    <?=js('plugins/highcharts/highcharts.js')?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#konfigurasi').addClass('active');
		$('#backup').addClass('active');
	});
</script>