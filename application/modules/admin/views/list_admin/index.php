<head>
    <title>Daftar Admin</title>
</head>

<div class="warper container-fluid">
<ol class="breadcrumb">
    <li>Admin</li>
    <li class="active">Data Admin</li>
</ol>
<div class="page-header"><h1>Daftar Admin</h1></div>

<div class="row">
            
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Daftar Admin Wira Polyclinic</div>
                        <div class="panel-body nicescroll">
                        <p style="margin-bottom:20px;"> 
                            <a href="<?php echo base_url('admin/list_admin/form');?>"><button class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Admin</button></a>
                        </p>

                        
                            <table id="tbl_admin" class="table table-bordered">
                              <thead>
                                <tr>
                                  <th width="1">No</th>
                                  <th>Username</th>
                                  <th>level</th>
                                  <th>Nama</th>
                                  <th>Alamat</th>
                                  <th width="25%">Aksi</th>
                                </tr>
                              </thead>
                              <tbody>
                              <?php $no=0; foreach ($admin as $ad ) { $no++?>
                                <tr>
                                <td><?php echo $no;?></td>
                                <td><?php echo $ad->username;?></td>
                                <td><?php echo ($ad->jenis == 'asisten_dokter')? 'asisten dokter' : $ad->jenis;?></td>
                                <td><?php echo $ad->nama;?></td>
                                <td><?php echo $ad->alamat;?></td>
                                <td>
                                <?php
                                  if($ad->jenis != 'admin'){
                                    echo "<a href='".base_url()."admin/list_admin/formedit?id=".base64_encode($ad->id_admin)."'>
                                      <button class='btn btn-success btn-xs'>Edit</button>
                                    </a>";
                                  }
                                  elseif($ad->jenis == 'admin' && $ad->id_admin == $this->session->userdata('id')){
                                    echo "<a href='".base_url()."admin/setting#data'><button class='btn btn-success btn-xs'>Edit</button></a>";
                                  }
                                ?>
                                <?php
                                  $tujuan = ($ad->id_admin != $this->session->userdata('id')) ? base_url().'admin/list_admin/formpass?id='.base64_encode($ad->id_admin) : base_url().'admin/setting#pass';
                                ?>
                                    <a href="<?php echo $tujuan; ?>"><button class='btn btn-info btn-xs'>Edit Password</button>
                                    </a>
                                <?php
                                  if($ad->id_admin != $this->session->userdata('id')){
                                    echo "<a href='".base_url()."admin/list_admin/delete?id=".base64_encode($ad->id_admin)."'>
                                      <button class='btn btn-danger btn-xs'>Delete</button>
                                    </a>";
                                  }
                                ?>  
                                </td>
                                </tr>
                               <?php } ?>   
                              </tbody>
                            </table>
                            
                        
                        </div>
                    </div>
                </div>
                
            </div>            
</div>

    <?=js('jquery/jquery.min.js')?>
    <?=js('bootstrap/bootstrap.min.js')?>
    <?=js('app/custom.js')?>
    <?=js('plugins/underscore/underscore-min.js')?> 
    <?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
    <?=js('plugins/data-table/jquery.dataTables.min.js')?>
    <?=js('plugins/data-table/dataTables.bootstrap.min.js')?>
    <?=js('plugins/select2/select2.min.js')?>    
<script type="text/javascript">
$(document).ready(function($){
    $('#tbl_admin').dataTable();
});
  

</script>

