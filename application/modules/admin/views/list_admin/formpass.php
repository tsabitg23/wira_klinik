<head>
    <title>Admin - Form Password</title>
</head>
          
<div class="warper container-fluid">
<ol class="breadcrumb">
    <li>Admin</li>
    <li><a href="<?php echo base_url('/admin/list_admin');?>">Data Admin</a></li>
    <li class="active">Form Edit Password</li>
</ol>
<div class="page-header"><h1>Form Edit Password</h1></div>

<div class="row">
            
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Edit Password admin</div>
            <div class="panel-body">
                    <?php 
                      $attrib = array('class' => 'form-horizontal');
                      $hidden = array('id' =>!empty($default->id_admin)? $default->id_admin : '','jenis'=>'password');
                      echo form_open_multipart($action,$attrib,$hidden);?>
                      <div class="form-group">
                        <label for="username" class="col-sm-2 control-label">Username</label>
                        <div class="col-sm-8">
                          <?php echo form_input('data[username]',!empty($default->username)? $default->username :'', 'placeholder="Username" disabled class="form-control col-md-7 col-xs-12" id="username" ') ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="pass" class="col-sm-2 control-label">Password Baru</label>
                        <div class="col-sm-8">
                          <?php echo form_password('data[password]','', 'placeholder="Password Baru" class="form-control col-md-7 col-xs-12" id="pass" ') ?>
                        </div>
                      </div>
                      <div class="form-group" id="con">
                        <label class="col-sm-2 control-label">Konfirmasi Password</label>
                        <div class="col-sm-8">
                          <?php 
                          $js = array('onChange' => 'checkPasswordMatch();');
                          echo form_password('confirm_password','', 'placeholder="Konfirmasi Password Baru" class="form-control col-md-7 col-xs-12" id="conpass"')
                          ?>
                          <span class="help-block"><small id="divCheckPasswordMatch"></small></span>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-9">
                          <button type="submit" class="btn btn-primary" id="simpan">Simpan</button>
                          <a href="<?php echo base_url('/admin/list_admin'); ?>" class="btn btn-danger">Batal</a>
                        </div>
                      </div>
                    <?php echo form_close();?>
            </div>
        </div>
    </div>
                
</div> 
          
</div>

        <!-- JQuery v1.9.1 -->
    <?=js('jquery/jquery.min.js')?>
    
    <!-- Bootstrap -->
    <?=js('bootstrap/bootstrap.min.js')?>

    <!-- Custom JQuery -->
    <?=js('app/custom.js')?>
    
   <?=js('plugins/underscore/underscore-min.js')?> 
   
    <!-- NanoScroll -->
    <?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
    <?=js('plugins/select2/select2.min.js')?>
<script type="text/javascript">
     var benar = false;
     function checkPasswordMatch() {
           var password = $("#pass").val();
           var confirmPassword = $("#conpass").val();
           console.log(password + " | " + confirmPassword);
           if (password !== confirmPassword){
              $('#help-block').show();
               $("#divCheckPasswordMatch").html("Passwords do not match!");
               $('#con').addClass('has-error');
               benar = false;
           }else if(password === confirmPassword){
              $('#help-block').hide();
               $("#divCheckPasswordMatch").html("Passwords match.");
               $('#con').removeClass('has-error');
               benar  = true;
           }
         }

         $(function() {
            $('#help-block').hide();
            $('#conpass').on('change paste keyup', function() {
                checkPasswordMatch();
                console.log("jalan bro");
            });
         });
         $(document).ready(function (){
              validate();
              $('#admin').addClass('active');
              $('#username, #pass, #conpass').on('change paste keyup', validate);
          });

          function validate(){
              if ($('#username').val().length   >   0   &&
                  $('#pass').val().length  >   0   &&
                  $('#conpass').val().length    >   0
                  && benar  == true) {
                  $("#simpan").prop("disabled", false);
              }
              else {
                  $("#simpan").prop("disabled", true);
              }
          }
</script>