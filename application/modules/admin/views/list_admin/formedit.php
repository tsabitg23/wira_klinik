<head>
    <title>Admin - Form Edit</title>
</head>
          
<div class="warper container-fluid">
<ol class="breadcrumb">
    <li>Admin</li>
    <li><a href="<?php echo base_url('/admin/list_admin');?>">Data Admin</a></li>
    <li class="active">Form Edit Level</li>
</ol>
<div class="page-header"><h1>Form Edit Level</h1></div>

<div class="row">
            
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Edit Level Admin</div>
            <div class="panel-body">
                    <?php 
                      $attrib = array('class' => 'form-horizontal');
                      $hidden = array('id' =>!empty($default->id_admin)? $default->id_admin : '','jenis'=>'edit');
                      echo form_open($action,$attrib,$hidden);?>
                      <div class="form-group">
                        <label for="username" class="col-sm-2 control-label">Username</label>
                        <div class="col-sm-8">
                          <?php echo form_input('data[username]',!empty($default->username)? $default->username :'', 'placeholder="Username" disabled class="form-control col-md-7 col-xs-12" id="username" ') ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Nama</label>
                        <div class="col-sm-8">
                          <?php echo form_input('data[nama]',!empty($default->nama)? $default->nama :'', 'placeholder="Nama Lengkap Admin" class="form-control col-md-7 col-xs-12" id="nama" ') ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="username" class="col-sm-2 control-label">Alamat</label>
                        <div class="col-sm-8">
                        <?php 
                        $data = array(
                              'name'        => 'data[alamat]',
                              'id'          => 'alamat',
                              'value'       => !empty($default->alamat)? $default->alamat : '',
                              'rows'        => '5',
                              'cols'        => '40',
                              'placeholder' => 'Alamat Admin',
                              'class' => 'form-control'
                            );

                          echo form_textarea($data);
                        ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="jenis" class="col-sm-2 control-label">Jenis Admin</label>
                        <div class="col-sm-2">
                          <?php echo form_dropdown('data[id_c_kat_admin]',$level,!empty($default->id_c_kat_admin)? $default->id_c_kat_admin :'','class="form-control choose-select" id="jenis" ') ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-9">
                          <button type="submit" class="btn btn-primary" id="simpan">Simpan</button>
                          <a href="<?php echo base_url('/admin/list_admin'); ?>" class="btn btn-danger">Batal</a>
                        </div>
                      </div>
                    <?php echo form_close();?>
            </div>
        </div>
    </div>
                
</div> 
          
</div>

        <!-- JQuery v1.9.1 -->
    <?=js('jquery/jquery.min.js')?>
    
    <!-- Bootstrap -->
    <?=js('bootstrap/bootstrap.min.js')?>

    <!-- Custom JQuery -->
    <?=js('app/custom.js')?>
    
   <?=js('plugins/underscore/underscore-min.js')?> 
   
    <!-- NanoScroll -->
    <?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
    <?=js('plugins/select2/select2.min.js')?>
<script type="text/javascript">

         $(document).ready(function (){
              $('#admin').addClass('active');
              $('.choose-select').select2();
          });
</script>