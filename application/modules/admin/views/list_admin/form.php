<head>
    <title>Admin - Form</title>
</head>
          
<div class="warper container-fluid">
<ol class="breadcrumb">
    <li>Admin</li>
    <li><a href="<?php echo base_url('/admin/list_admin');?>">Data Admin</a></li>
    <li class="active">Form Insert</li>
</ol>
<div class="page-header"><h1>Form Insert</h1></div>

<div class="row">
            
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Tambah admin</div>
            <div class="panel-body">
                    <?php 
                      $attrib = array('class' => 'form-horizontal');
                      $hidden = array('id' =>'','jenis'=>'add');
                      echo form_open_multipart($action,$attrib,$hidden);?>
                      <div class="form-group">
                        <label for="username" class="col-sm-2 control-label">Username</label>
                        <div class="col-sm-8">
                          <?php echo form_input('data[username]','', 'placeholder="Username" class="form-control col-md-7 col-xs-12" id="username" ') ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="pass" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-8">
                          <?php echo form_password('data[password]','', 'placeholder="New Password" class="form-control col-md-7 col-xs-12" id="pass" ') ?>
                        </div>
                      </div>
                      <div class="form-group" id="con">
                        <label class="col-sm-2 control-label">Confirm Password</label>
                        <div class="col-sm-8">
                          <?php 
                          $js = array('onChange' => 'checkPasswordMatch();');
                          echo form_password('confirm_password','', 'placeholder="Confirm Password" class="form-control col-md-7 col-xs-12" id="conpass"')
                          ?>
                          <span class="help-block"><small id="divCheckPasswordMatch"></small></span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="jenis" class="col-sm-2 control-label">Level Admin</label>
                        <div class="col-sm-2">
                          <?php echo form_dropdown('data[id_c_kat_admin]',$level,'','class="form-control choose-select" id="jenis" ') ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Nama</label>
                        <div class="col-sm-8">
                          <?php echo form_input('data[nama]',!empty($default->nama)? $default->nama :'', 'placeholder="Nama Lengkap Admin" class="form-control col-md-7 col-xs-12" id="nama" ') ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="username" class="col-sm-2 control-label">Alamat</label>
                        <div class="col-sm-8">
                        <?php 
                        $data = array(
                              'name'        => 'data[alamat]',
                              'id'          => 'alamat',
                              'value'       => !empty($default->alamat)? $default->alamat : '',
                              'rows'        => '5',
                              'cols'        => '40',
                              'placeholder' => 'Alamat Admin',
                              'class' => 'form-control'
                            );

                          echo form_textarea($data);
                        ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-9">
                          <button type="submit" class="btn btn-primary" id="simpan">Simpan</button>
                          <a href="<?php echo base_url('/admin/list_admin'); ?>" class="btn btn-danger">Batal</a>
                        </div>
                      </div>
                    <?php echo form_close();?>
            </div>
        </div>
    </div>
                
</div> 
          
</div>

    <?=js('jquery/jquery.min.js')?>
    <?=js('bootstrap/bootstrap.min.js')?>
    <?=js('app/custom.js')?>
    <?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
    <?=js('plugins/select2/select2.min.js')?>
<script type="text/javascript">
     var benar = false;
     function checkPasswordMatch() {
           var password = $("#pass").val();
           var confirmPassword = $("#conpass").val();
           console.log(password + " | " + confirmPassword);
           if (password !== confirmPassword){
              $('#help-block').show();
               $("#divCheckPasswordMatch").html("Passwords do not match!");
               $('#con').addClass('has-error');
               benar = false;
           }else if(password === confirmPassword){
              $('#help-block').hide();
               $("#divCheckPasswordMatch").html("Passwords match.");
               $('#con').removeClass('has-error');
               benar  = true;
           }
         }

         $(function() {
            $('#help-block').hide();
            $('#conpass').on('change paste keyup', function() {
                checkPasswordMatch();
                console.log("jalan bro");
            });
         });
         $(document).ready(function (){
              validate();
              $('#admin').addClass('active');
              $('#username, #pass, #conpass,#alamat,#nama').on('change paste keyup', validate);
              $('#jenis').change(function(){
                validate();
              })
              $('#jenis').select2();     
          });

          function validate(){
              if ($('#username').val().length   >   0   &&
                  $('#pass').val().length  >   0   &&
                  $('#alamat').val().length  >   0   &&
                  $('#nama').val().length  >   0   &&
                  $('#conpass').val().length    >   0
                  && benar  == true && $('#jenis').val() != 'kosong') {
                  $("#simpan").prop("disabled", false);
              }
              else {
                  $("#simpan").prop("disabled", true);
              }
          }
</script>