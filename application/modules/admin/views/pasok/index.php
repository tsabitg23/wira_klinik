<head>
    <title>Obat - Pemasokan</title>
</head>

<div class="warper container-fluid">
<ol class="breadcrumb">
    <li>Admin</li>
    <li><a href="<?php echo base_url('/admin/obat');?>">Obat</a></li>
    <li class="active">Pemasokan</li>
</ol>
<div class="page-header"><h1>Data Pemasokan Obat</h1></div>

<div class="row">
            
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Data Pemasokan Obat</div>
                        <div class="panel-body nicescroll">
                        <p style="margin-bottom:20px;"> 
                            <a href="<?php echo base_url('admin/obat/pasok/form');?>"><button class="btn btn-primary"><i class="fa fa-plus"></i> Pasok Obat</button></a>
                        </p>

                        
                            <table id="tbl_pemasokan" class="table table-bordered">
                              <thead>
                                <tr>
                                  <th width="1">No</th>
                                  <th>tanggal</th>
                                  <th>Nama Obat</th>
                                  <th>jumlah</th>
                                  <th>total</th>
                                  <th>Admin</th>
                                  <th width="25%">Aksi</th>
                                </tr>
                              </thead>
                              <tbody>
                              <?php $no=0; foreach ($pasok as $pask ) { $no++?>
                                <tr>
                                <td><?php echo $no;?></td>
                                <td><?php echo $pask->tgl;?></td>
                                <td><?php echo $pask->nama_obat;?></td>
                                <td><?php echo $pask->jumlah;?></td>
                                <td><?php echo "Rp.".$pask->total;?></td>
                                <td><?php echo $pask->admin;?></td>
                                <td>
                                    <a href="<?php echo base_url().'admin/obat/pasok/form?id='.base64_encode($pask->id_pemasokan) ?>">
                                      <button class="btn btn-success btn-xs">Edit</button>
                                    </a>
                                    <a href="<?php echo base_url().'admin/obat/pasok/delete?id='.base64_encode($pask->id_pemasokan) ?>"><button class="btn btn-danger btn-xs">Delete</button></a>
                                </td>
                                </tr>
                               <?php } ?>   
                              </tbody>
                            </table>
                            
                        
                        </div>
                    </div>
                </div>
                
            </div>            
</div>

    <?=js('jquery/jquery.min.js')?>
    <?=js('bootstrap/bootstrap.min.js')?>
    <?=js('app/custom.js')?>
    <?=js('plugins/underscore/underscore-min.js')?> 
    <?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
    <?=js('plugins/data-table/jquery.dataTables.min.js')?>
    <?=js('plugins/data-table/dataTables.bootstrap.min.js')?>
    <?=js('plugins/select2/select2.min.js')?>    
<script type="text/javascript">
$(document).ready(function($){
    $('#obat').addClass('active');
    $('#tbl_pemasokan').dataTable();
});
  

</script>

