<head>
    <title>Pasok - Form</title>
</head>
          
<div class="warper container-fluid">
<ol class="breadcrumb">
    <li>Admin</li>
    <li><a href="<?php echo base_url('/admin/obat');?>">Obat</a></li>
    <li><a href="<?php echo base_url('/admin/obat/pasok');?>">Pemasokan</a></li>
    <li class="active">Form</li>
</ol>
<div class="page-header"><h1>Form Pemasokan Obat</h1></div>

<div class="row">
            
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Tambah / Edit Data</div>
            <div class="panel-body">
                    <?php 
                      $attrib = array('class' => 'form-horizontal');
                      $hidden = array('id' => !empty($default->id_pemasokan)? $default->id_pemasokan : '');
                      echo form_open_multipart($action,$attrib,$hidden);?>
                      <div class="form-group">
                        <label for="id_obat" class="col-sm-2 control-label">Obat</label>
                        <div class="col-sm-2">
                          <?php echo form_dropdown('data[id_obat]',$obat,!empty($default->id_obat)? $default->id_obat : '','class="form-control choose-select" id="id_obat" ') ?>
                        </div>
                        <label for="harga" class="col-sm-1 control-label">Harga</label>
                        <div class="col-sm-2">
                          <?php echo form_input('harga',!empty($default->harga)? $default->harga : '0','class="form-control choose-select" disabled id="harga" ') ?>
                        </div>
                      </div>
                      <div class="form-group" id="con">
                        <label class="col-sm-2 control-label">Jumlah</label>
                        <div class="col-sm-2">
                          <?php 
                          $js = array('onChange' => 'checkJumlah();');
                          echo form_input('data[jumlah]',!empty($default->jumlah)? $default->jumlah  : '', 'placeholder="Jumlah Obat" class="form-control col-md-7 col-xs-12" id="jumlahobat"')
                          ?>
                          <span class="help-block"><small id="divCheckJumlah"></small></span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="total" class="col-sm-2 control-label">Total</label>
                        <div class="col-sm-5">
                          <?php echo form_input('data[total]',!empty($default->total)? $default->total : '0', 'placeholder="total" readonly class="form-control col-md-7 col-xs-12" id="total" ') ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-9">
                          <button type="submit" class="btn btn-primary" id="simpan">Simpan</button>
                          <a href="<?php echo base_url('/admin/obat/pasok'); ?>" class="btn btn-danger">Batal</a>
                        </div>
                      </div>
                    <?php echo form_close();?>
            </div>
        </div>
    </div>
                
</div> 
          
</div>

        <!-- JQuery v1.9.1 -->
    <?=js('jquery/jquery.min.js')?>
    
    <!-- Bootstrap -->
    <?=js('bootstrap/bootstrap.min.js')?>

    <!-- Custom JQuery -->
    <?=js('app/custom.js')?>
    
   <?=js('plugins/underscore/underscore-min.js')?> 
   
    <!-- NanoScroll -->
    <?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
    <?=js('plugins/select2/select2.min.js')?>
<script type="text/javascript">
    var benar = false;
    function checkJumlah() {
          var confirmPassword = $("#jumlahobat").val();
          var checko = $.isNumeric(confirmPassword);
          if (checko== false && $('#jumlahobat').val().length>0 ){
             $('#help-block').show();
              $("#divCheckJumlah").html("Jumlah hanya boleh berisi angka!");
              $('#con').addClass('has-error');
              benar = false;
          }else if(checko == true){
             $('#help-block').hide();
             $("#divCheckJumlah").html("");
              $('#con').removeClass('has-error');
              benar  = true;
              console.log(checko);
          }
    }

    $(function() {
       $('#help-block').hide();
       $('#jumlahobat').on('change paste keyup', function() {
           checkJumlah();
       });
    });

    $(document).ready(function($){
        $('#obat').addClass('active');
        $('#pasok').addClass('active');        
        $("#id_obat").change(function (){
                if($(this).val() != 'kosong'){
                var url = "<?php echo site_url('admin/pasok/harga');?>/"+$(this).val();
                $.get(url, function(result) {
                    $('#harga').val(result);
                    $('#jumlahobat').focus();
                });
                }
                else{
                  $.get(url, function(result) {
                      $('#harga').val(0);
                      $('#jumlahobat').val(0);
                      $('#total').val(0);
                  });
                }
                validate();
        })

        hargaAwal();        
        validate();
        $("#jumlahobat").on('change paste keyup',function(){
            $('#total').val(hitungTotal);
        })
        $
        $('#jumlahobat').on('change paste keyup', validate);
    });

    function validate(){
        if ($('#jumlahobat').val().length    >   0 && benar == true && $('#id_obat').val().length   >   6) 
          {
            $("#simpan").prop("disabled", false);
          }
        else {
            $("#simpan").prop("disabled", true);
        }
        $('#id_obat').select2();
    }

    function hargaAwal(){
          var a = $('#id_obat').val();
          if(a != 'kosong'){
            var url = "<?php echo site_url('admin/pasok/harga');?>/"+a;
            $.get(url, function(result) {
                $('#harga').val(result);
            }); 
          }
        
        
    }
    function hitungTotal(){
        var harga = $('#harga').val();
        var jumlah = $('#jumlahobat').val();
        return harga*jumlah;
    }
</script>