<head>
    <title>Obat - Form</title>
</head>
          
<div class="warper container-fluid">
<ol class="breadcrumb">
    <li>Admin</li>
    <li><a href="<?php echo base_url('/admin/obat');?>">Obat</a></li>
    <li class="active">Form</li>
</ol>
<div class="page-header"><h1>Form Obat</h1></div>

<div class="row">
            
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Tambah / Edit Data</div>
            <div class="panel-body">
                    <?php 
                      $attrib = array('class' => 'form-horizontal');
                      $hidden = array('id' => !empty($default->id_obat)? $default->id_obat : '');
                      echo form_open_multipart($action,$attrib,$hidden);?>
                      <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Nama</label>
                        <div class="col-sm-8">
                          <?php echo form_input('data[nama]',!empty($default->nama)? $default->nama : '', 'placeholder="Nama Obat" class="form-control col-md-7 col-xs-12" id="nama" ') ?>
                        </div>
                      </div>
                      <div class="form-group" id="con">
                        <label class="col-sm-2 control-label">Harga</label>
                        <div class="col-sm-8">
                          <?php 
                          $js = array('onChange' => 'checkPasswordMatch();');
                          echo form_input('data[harga]',!empty($default->harga)? $default->harga  : '', 'placeholder="Harga Obat" class="form-control col-md-7 col-xs-12" id="hargaobat"')
                          ?>
                          <span class="help-block"><small id="divCheckPasswordMatch"></small></span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="jenis" class="col-sm-2 control-label">Jenis Obat</label>
                        <div class="col-sm-4">
                          <?php echo form_dropdown('data[id_c_jenis_obat]',$jenis,!empty($default->id_c_jenis_obat)? $default->id_c_jenis_obat : '','class="form-control choose-select" id="jenis" ') ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="kategori" class="col-sm-2 control-label">Kategori Obat</label>
                        <div class="col-sm-4">
                          <?php echo form_dropdown('data[id_c_kat_obat]',$kategori,!empty($default->id_c_kat_obat)? $default->id_c_kat_obat  : '','class="form-control choose-select" id="kategori" ') ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="umur" class="col-sm-2 control-label">Rating Umur</label>
                        <div class="col-sm-2">
                          <?php echo form_dropdown('data[umur]',$umur,!empty($default->umur)? $default->umur  : '','class="form-control choose-select" id="umur" ') ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-9">
                          <button type="submit" class="btn btn-primary" id="simpan">Simpan</button>
                          <a href="<?php echo base_url('/admin/obat'); ?>" class="btn btn-danger">Batal</a>
                        </div>
                      </div>
                    <?php echo form_close();?>
            </div>
        </div>
    </div>
                
</div> 
          
</div>

        <!-- JQuery v1.9.1 -->
    <?=js('jquery/jquery.min.js')?>
    
    <!-- Bootstrap -->
    <?=js('bootstrap/bootstrap.min.js')?>

    <!-- Custom JQuery -->
    <?=js('app/custom.js')?>
    
   <?=js('plugins/underscore/underscore-min.js')?> 
   
    <!-- NanoScroll -->
    <?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
    <?=js('plugins/select2/select2.min.js')?>
<script type="text/javascript">

    $(function() {
       $('#help-block').hide();
       $('#hargaobat').on('change paste keyup', function() {
           checkPasswordMatch();
           console.log("jalan bro");
       });
    });

    $(document).ready(function($){
        $('#obat').addClass('active');
        $('#data').addClass('active');
        $('.choose-select').select2();
        validate();
        $('#nama,#hargaobat').on('change paste keyup', validate);
        $('#kategori,#jenis,#umur').change(function(){
            validate();
        })
    });

    function validate(){
        if ($('#nama').val().length    >   0 &&
            $('#hargaobat').val().length    >   0 &&
            $('#kategori').val() != 'kosong' &&
            $('#jenis').val() != 'kosong' &&
            $('#umur').val() != 'kosong'
          ) {
            $("#simpan").prop("disabled", false);
        }
        else {
            $("#simpan").prop("disabled", true);
        }
    }
</script>