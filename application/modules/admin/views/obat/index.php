<head>
    <title>Obat - Data</title>
</head>

<div class="warper container-fluid">
<ol class="breadcrumb">
    <li>Admin</li>
    <li class="active">Obat</li>
</ol>
<div class="page-header"><h1>Data Obat</h1></div>

<div class="row">
            
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Data Obat</div>
                        <div class="panel-body nicescroll">
                        <p style="margin-bottom:20px;"> 
                            <a href="<?php echo base_url('admin/obat/form');?>"><button class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</button></a>
                        </p>

                        
                            <table id="tbl_obat" class="table table-bordered">
                              <thead>
                                <tr>
                                  <th width="1">No</th>
                                  <th>Nama</th>
                                  <th>Jenis</th>
                                  <th>Kategori</th>
                                  <th>Rating Umur</th>
                                  <th>Harga</th>
                                  <th>Stok</th>
                                  <th width="25%">Aksi</th>
                                </tr>
                              </thead>
                              <tbody>
                              <?php $no=0; foreach ($obat as $obt ) { $no++?>
                                <tr>
                                <td><?php echo $no;?></td>
                                <td><?php echo $obt->nama;?></td>
                                <td><?php echo $obt->jenis_obat;?></td>
                                <td><?php echo $obt->kat_obat;?></td>
                                <td>
                                  <?php if($obt->umur =='D'){
                                   echo 'Dewasa';
                                  }
                                  else if($obt->umur =='A'){
                                    echo 'Anak-anak';
                                  }
                                  else{
                                    echo 'Semua umur';
                                  }
                                  ?>
                                </td>
                                <td><?php echo $obt->harga;?></td>
                                <td><?php echo $obt->stok;?></td>
                                <td>
                                    <a href="<?php echo base_url().'admin/obat/form?id='.base64_encode($obt->id_obat) ?>">
                                      <button class="btn btn-success btn-xs">Edit</button>
                                    </a>
                                    <a href="<?php echo base_url().'admin/obat/pasok/form?id_o='.base64_encode($obt->id_obat) ?>">
                                      <button class="btn btn-info btn-xs">Tambah Stok</button>
                                    </a>
                                    <a href="<?php echo base_url().'admin/obat/delete?id='.base64_encode($obt->id_obat) ?>"><button class="btn btn-danger btn-xs">Delete</button></a>
                                </td>
                                </tr>
                               <?php } ?>   
                              </tbody>
                            </table>
                            
                        
                        </div>
                    </div>
                </div>
                
            </div>            
</div>

    <?=js('jquery/jquery.min.js')?>
    <?=js('bootstrap/bootstrap.min.js')?>
    <?=js('app/custom.js')?>
    <?=js('plugins/underscore/underscore-min.js')?> 
    <?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
    <?=js('plugins/data-table/jquery.dataTables.min.js')?>
    <?=js('plugins/data-table/dataTables.bootstrap.min.js')?>
    <?=js('plugins/select2/select2.min.js')?>    
<script type="text/javascript">
$(document).ready(function($){
    $('#obat').addClass('active');
    $('#tbl_obat').dataTable();
});
  

</script>

