<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="icon" type="image/png" href="<?php echo base_url('/assets/img/logo/icon.png');?>">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?=css('font/bariol.css');?>
	<?=css('bootstrap/bootstrap.css');?> 
	<?=css('font/roboto.css');?>
	<?=css('main/main.css');?>
</head>
<body onload="window.print()">


	<div class="row">
		<div class="col-md-12" id="dataResep">
			<div class="panel panel-default">
				<div class="panel-body nicescroll">
					<div class="row">
						<div class="col-lg-4 col-lg-offset-4">
							<h3 class="text-center"><?=img('logo/logobluesmall.png')?></h3>
						</div>
						<div class="col-md-6 tabular">

							<table class="table no-bor">
								<tr>
									<td><strong>Data Poliklinik</strong></td>		
								</tr>
								<tr>
									<td>Nama Poliklinik</td>		
									<td>:</td>		
									<td style="text-transform:capitalize;"><?php echo $info->nama?></td>		
								</tr>
								<tr>
									<td>Tahun</td>		
									<td>:</td>		
									<td><?php echo $tahun;?></td>		
								</tr>
							</table>


						</div>


					</div>
					<table id="tbl_pend" class="table">
						<thead>
							<tr>
								<th>Bulan</th>
								<th>Jumlah Pasien</th>
								<th>Pendapatan</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($penghasilan as $hasil ) {?>
								<tr>
									<td><?php echo nice_date('2017-'.$hasil->bulan.'-01','F');?></td>
									<td class='jum'><?php echo $hasil->jumlah_pasien;?></td>
									<td class='sub'><?php echo $hasil->pendapatan;?></td>
								</tr>
								<?php } ?>   
								<tr>
									<td>Total</td>
									<td id="total_pasien"></td>
									<td id="total_tahun"></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

	</body>
	</html>

	<?=js('jquery/jquery.min.js')?>
	<?=js('bootstrap/bootstrap.min.js')?>
	<?=js('app/custom.js')?>

	<script type="text/javascript">
	$(document).ready(function(){
		var sum = 0;
		var jum = 0;
		$('.sub').each(function(){
		    sum += parseFloat($(this).text());  
		});
		$('.jum').each(function(){
		    jum += parseFloat($(this).text());  
		});
		$('#total_tahun').text(sum);
		$('#total_pasien').text(jum);
	});
	</script>