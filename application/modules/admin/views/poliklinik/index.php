<head>
    <title>Poliklinik</title>
</head>

<div class="warper container-fluid">
<ol class="breadcrumb">
    <li>Admin</li>
    <li class="active">Poliklinik</li>
</ol>
<div class="page-header"><h1>Poliklinik</h1></div>

<div class="row">
            
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Data Poliklinik</div>
                        <div class="panel-body nicescroll">
                        <p style="margin-bottom:20px;"> 
                            <a href="<?php echo base_url('admin/poliklinik/form');?>"><button class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</button></a>
                        </p>

                        
                            <table id="tbl_poli" class="table table-bordered">
                              <thead>
                                <tr>
                                  <th width="1">No</th>
                                  <th>Nama</th>
                                  <th width="25%">Aksi</th>
                                </tr>
                              </thead>
                              <tbody>
                              <?php $no=0; foreach ($poliklinik as $poli ) { $no++?>
                                <tr>
                                <td><?php echo $no;?></td>
                                <td><?php echo $poli->nama;?></td>
                                <td>
                                    <a href="<?php echo base_url().'admin/poliklinik/detail?id='.base64_encode($poli->id_poliklinik) ?>">
                                      <button class="btn btn-info btn-xs">Lihat</button>
                                    </a>
                                    <a href="<?php echo base_url().'admin/poliklinik/formedit?id='.base64_encode($poli->id_poliklinik) ?>">
                                      <button class="btn btn-success btn-xs">Edit</button>
                                    </a>
                                    <a href="<?php echo base_url().'admin/poliklinik/delete?id='.base64_encode($poli->id_poliklinik) ?>"><button class="btn btn-danger btn-xs">Delete</button></a>
                                </td>
                                </tr>
                               <?php } ?>   
                              </tbody>
                            </table>
                            
                        
                        </div>
                    </div>
                </div>
                
            </div>            
</div>

    <?=js('jquery/jquery.min.js')?>
    <?=js('bootstrap/bootstrap.min.js')?>
    <?=js('app/custom.js')?>
    <?=js('plugins/underscore/underscore-min.js')?> 
    <?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
    <?=js('plugins/data-table/jquery.dataTables.min.js')?>
    <?=js('plugins/data-table/dataTables.bootstrap.min.js')?>
    <?=js('plugins/select2/select2.min.js')?>    
<script type="text/javascript">
$(document).ready(function($){
    $('#tbl_poli').dataTable();
});
  

</script>

