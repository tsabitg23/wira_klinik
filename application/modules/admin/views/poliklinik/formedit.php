<head>
    <title>Poliklinik - Form</title>
</head>
          
<div class="warper container-fluid">
<ol class="breadcrumb">
    <li>Admin</li>
    <li><a href="<?php echo base_url('/admin/poliklinik');?>">Poliklinik</a></li>
    <li class="active">Form Edit</li>
</ol>
<div class="page-header"><h1>Form Edit Poliklinik <small>Untuk edit layanan, klik lihat di <a href="<?php echo base_url('/admin/poliklinik');?>">Poliklinik</a></small></h1></div>

<div class="row">
            
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Edit Data</div>
            <div class="panel-body">
                    <?php 
                      $attrib = array('class' => 'form-horizontal','id' =>'form-poli');
                      $hidden = array('id' => !empty($default->id_poliklinik)? $default->id_poliklinik : '','jenis'=>'edit');
                      echo form_open_multipart($action,$attrib,$hidden);?>
                      <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Nama</label>
                        <div class="col-sm-8">
                          <?php echo form_input('data[nama]',!empty($default->nama)? $default->nama : '', 'placeholder="Nama Poliklinik" class="form-control col-md-7 col-xs-12" id="nama" ') ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-9">
                          <button type="submit" class="btn btn-primary" id="simpan">Simpan</button>
                          <a href="<?php echo base_url('/admin/poliklinik'); ?>" class="btn btn-danger">Batal</a>
                        </div>
                      </div>
                    <?php echo form_close();?>
            </div>
        </div>
    </div>
                
</div> 
          
</div>

    <?=js('jquery/jquery.min.js')?>
    <?=js('bootstrap/bootstrap.min.js')?>
    <?=js('app/custom.js')?>
    <?=js('plugins/underscore/underscore-min.js')?> 
    <?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
    <?=js('plugins/data-table/dataTables.bootstrap.min.js')?>
    <?=js('plugins/select2/select2.min.js')?>

<script type="text/javascript">
    $(document).ready(function($){   
        
        $('#poliklinik').addClass('active'); 
        $('#nama').on('change paste keyup', validate);
        validate();
    });

    function validate(){
  if ($('#nama').val().length    >   0) {
            $("#simpan").prop("disabled", false);
        }
        else {
            $("#simpan").prop("disabled", true);
        }
    }
</script>