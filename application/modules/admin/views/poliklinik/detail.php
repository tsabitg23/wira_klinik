<head>
  <title>Poliklinik</title>
</head>

<div class="warper container-fluid">
  <ol class="breadcrumb">
    <li>Admin</li>
    <li><a href="<?php echo base_url('/admin/poliklinik');?>">Poliklinik</a></li>
    <li class="active" style="text-transform:capitalize;"><?php echo $default->nama;?></li>
  </ol>
  <div class="page-header" style="text-transform:capitalize;"><h1><?php echo $default->nama;?> <small><a href="<?php echo base_url('/admin/poliklinik');?>"><button class="btn btn-primary btn-xs"><i class="fa fa-arrow-left"></i> back</button></a></small></h1></div>

  <div class="row">
           <div class="col-md-12">
              <h5 class="no-margn tabular">Tampilkan Data Tahun</h5>    
              <hr>
             <form role="form" id="searchnya" method="POST" action="<?php echo base_url().'admin/poliklinik/detail?id='.base64_encode($default->id_poliklinik);?>">
                 <div class="form-group">
                    <div class="form-group">
                      <div class="col-sm-2" id="tahun">
                          <?php 
                            echo form_input('tahun','','class="form-control" placeholder="Tahun" id="select-tahun"');
                            ?>
                      </div>
                      <div class="col-sm-2">
                          <button class="btn btn-success" id="searchRow"><i class="fa fa-search"></i> Cari</button>
                      </div>
                  </div>
              </div>
          </form>   
      </div>
      <div class="text-right" style="margin-bottom:2%;">
      <a class="btn btn-warning" id="cetak-laporan"><i class="fa fa-print"></i> Cetak Laporan</a>
      <form method="POST" target="_blank" id="form-tahun-laporan" action="<?php echo base_url().'admin/poliklinik/laporan/'.$default->id_poliklinik?>">
        <input type="hidden" name="tahun" value="<?php echo $tahunnya;?>">
      </form>
      </div>
    </div>


  <div class="row">
  <div id="chart">
  </div>

  <div id="chart2">
  </div>
    <div class="col-md-12" id="lay">
      <div class="panel panel-default">
        <div class="panel-heading">Pendapatan Poliklinik <?php echo date('Y');?></div>
        <div class="panel-body nicescroll">
          <table id="tbl_pend" class="table">
            <thead>
              <tr>
                <th></th>
                <th>Jumlah Pasien</th>
                <th>Pendapatan</th>
              </tr>
            </thead>
            <tbody>
            <?php $no1 = 0;$no2 = 0;foreach ($penghasilan as $hasil ) {
                
                $no1 += $hasil->jumlah_pasien;
                $no2 += $hasil->pendapatan;
                } ?>   
                <tr>
                  <td>Total</td>
                  <td><?php echo $no1;?></td>
                  <td><?php echo $no2;?></td>
                </tr>
              </tbody>
            </table>


          </div>
        </div>
      </div>
    </div>

  <div class="row">

    <div class="col-md-12" id="lay">
      <div class="panel panel-default">
        <div class="panel-heading">Poliklinik Layanan</div>
        <div class="panel-body nicescroll">
        <?php 
        echo form_open($action);
        ?>
          <div class="form-group" style="margin-bottom:50px;">
            <label for="layanan" style="margin-left:2%;"><span id="tekusu">Tambah</span> Data</label><br>
            <div class="col-sm-4">
              <?php echo form_input('data[nama]','', 'placeholder="Nama Layanan" class="form-control col-md-7 col-xs-12" id="layanan" ') ?>
            </div>
            <input type="hidden" name="id_p" value="<?php echo $default->id_poliklinik;?>">
           <input type="hidden" id="idLay" name="idLay" value="<?php echo base64_encode('coy');?>">
            <div class="col-sm-3" id="har">
              <?php echo form_input('data[tarif]', '', 'placeholder="Tarif" class="form-control col-md-7 col-xs-12" id="harga" ') ?>
            </div>
            <div class="col-sm-4">
              <button class="btn btn-primary" id="addRow">Tambah</button>
              <a class="btn btn-danger" style="visibility:hidden;" id="cancel" onclick="clearForm()">Batal</a>
            </div>
          </div>
        <?php echo form_close();?>
          <table id="tbl_lay" class="table table-bordered">
            <thead>
              <tr>
                <th width="1%">No</th>
                <th>Layanan</th>
                <th>Harga</th>
                <th width="15%">Aksi</th>
              </tr>
            </thead>
            <tbody>
            <?php $no=0; foreach ($layanan as $lay ) { $no++?>
                <tr>
                  <td><?php echo $no;?></td>
                  <td><?php echo $lay->nama;?></td>
                  <td><?php echo $lay->tarif;?></td>
                  <td>
                    <a class="editRow"><button class="btn btn-success btn-xs"><i class="fa fa-edit"></i></button></a>
                    <a href="<?php echo base64_encode($lay->id_p_layanan)?>"></a>
                    <a class="deleteRow" href="<?php echo base_url().'admin/poliklinik/deletelay?id='.base64_encode($lay->id_p_layanan).'&&id_p='.base64_encode($default->id_poliklinik)?>"><button class="btn btn-danger btn-xs"><i class="fa fa-close"></i></button></a>
                  </td>
                </tr>
                <?php } ?>   

              </tbody>
            </table>


          </div>
        </div>
      </div>
    </div>      

  </div>

  <?=js('jquery/jquery.min.js')?>
  <?=js('bootstrap/bootstrap.min.js')?>
  <?=js('app/custom.js')?>
  <?=js('plugins/underscore/underscore-min.js')?> 
  <?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
  <?=js('plugins/data-table/jquery.dataTables.min.js')?>
  <?=js('plugins/data-table/dataTables.bootstrap.min.js')?>
  <?=js('plugins/select2/select2.min.js')?>    
  <?=js('plugins/highcharts/highcharts.js')?>    
  <?=js('plugins/highcharts/data.js')?>    
      
  <script type="text/javascript">
    jQuery(function(){
     new Highcharts.Chart({
      chart: {
       renderTo: 'chart',
       type: 'line',
      },
      title: {
       text: 'Jumlah Pasien Tahun <?php echo $tahunnya;?>',
       x: -20
      },
      subtitle: {
       text: 'Jumlah pasien yang datang ke poliklinik per tahun <?php echo $tahunnya;?>',
       x: -20
      },
      xAxis: {
       categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun',
                        'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des']
      },
      yAxis: {
       title: {
        text: 'Jumlah Pasien (Orang)'
       }
      },
      series: [{
       name: 'Jumlah Pasien (Orang)',
       data: <?php echo json_encode($grafik); ?>
      }]
     });
    });

    jQuery(function(){
     new Highcharts.Chart({
      chart: {
       renderTo: 'chart2',
       type: 'line',
      },
      title: {
       text: 'Pendapatan Tahun <?php echo $tahunnya;?>',
       x: -20
      },
      subtitle: {
       text: 'Pendapatan poliklinik dari biaya layanan per tahun <?php echo $tahunnya;?>',
       x: -20
      },
      xAxis: {
       categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun',
                        'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des']
      },
      yAxis: {
       title: {
        text: 'Pendapatan (Rp)'
       }
      },
      series: [{
       name: 'Pendapatan (Rp)',
       data: <?php echo json_encode($grafik2); ?>
      }]
     });
    }); 
    $(document).ready(function($){

      $('#tbl_lay').on('click','a.editRow',function(){
         $('#addRow').removeClass('btn-primary');
         $('#addRow').addClass('btn-success');
         $('#addRow').text('Simpan');
         $('#tekusu').text('Edit');
         $('#layanan').focus();
         $('#layanan').val($(this).parent().prev().prev().text());
         $('#harga').val($(this).parent().prev().text());
         $('#cancel').css("visibility",'visible');
         $('#idLay').val($(this).next().attr('href'));
         validate();
      })
      $('#poliklinik').addClass('active'); 
      $('#tbl_lay').DataTable({
        searching : false,
        "lengthChange": false,
        ordering: false
      });
      $('#cetak-laporan').click(function(){
          $('#form-tahun-laporan').submit();
      });
      $('#layanan,#harga').on('change paste keyup', validate);
      validate();
      
    });

    function clearForm(){
        $('#layanan').val("");
        $('#harga').val("");
        $('#addRow').removeClass('btn-success');
        $('#addRow').addClass('btn-primary');
        $('#addRow').text('Tambah');
        $('#tekusu').text('Tambah');
         $('#cancel').css("visibility",'hidden');
        
    }
    function validate(){
        if ($('#layanan').val().length    >   0 && $.isNumeric($('#harga').val()) == true) {
            $("#addRow").prop("disabled", false);
        }
        else {
            $("#addRow").prop("disabled", true);
        }
    }

    


  </script>

