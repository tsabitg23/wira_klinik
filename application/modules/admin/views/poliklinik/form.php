<head>
    <title>Poliklinik - Form</title>
</head>
          
<div class="warper container-fluid">
<ol class="breadcrumb">
    <li>Admin</li>
    <li><a href="<?php echo base_url('/admin/poliklinik');?>">Poliklinik</a></li>
    <li class="active">Form</li>
</ol>
<div class="page-header"><h1>Form Poliklinik</h1></div>

<div class="row">
            
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Tambah Data</div>
            <div class="panel-body">
                    <?php 
                      $attrib = array('class' => 'form-horizontal','id' =>'form-poli');
                      $hidden = array('id' => !empty($default->id_poliklinik)? $default->id_poliklinik : '','jenis'=>'add');
                      echo form_open_multipart($action,$attrib,$hidden);?>
                      <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Nama</label>
                        <div class="col-sm-8">
                          <?php echo form_input('data[nama]',!empty($default->nama)? $default->nama : '', 'placeholder="Nama Poliklinik" class="form-control col-md-7 col-xs-12" id="nama" ') ?>
                        </div>
                      </div>
                      <input type="hidden" id="dataLay" name="dataLay" value="">
                      <div class="form-group">
                        <label for="layanan" class="col-sm-2 control-label">Layanan</label>
                        <div class="col-sm-4">
                          <?php echo form_input('layanan','', 'placeholder="Nama Layanan" class="form-control col-md-7 col-xs-12" id="layanan" ') ?>
                        <span class="help-block"><small>* Input layanan dapat dilewati</small></span>
                        </div>
                        <div class="col-sm-2" id="har">
                          <?php echo form_input('harga','', 'placeholder="Harga" class="form-control col-md-7 col-xs-12" id="harga" ') ?>
                        </div>
                        <div class="col-sm-2">
                          <a class="btn btn-success" id="addRow">Tambah</a>
                        </div>
                      </div>
                      <table id="tbl_lay" class="table table-bordered" style="width:60%;margin:20px 17%;">
                        <thead>
                          <th>Layanan</th>
                          <th>Harga</th>
                          <th width="5%">Aksi</th>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-9">
                          <a type="submit" class="btn btn-primary" id="simpan">Simpan</a>
                          <a href="<?php echo base_url('/admin/poliklinik'); ?>" class="btn btn-danger">Batal</a>
                        </div>
                      </div>
                    <?php echo form_close();?>
            </div>
        </div>
    </div>
                
</div> 
          
</div>

    <?=js('jquery/jquery.min.js')?>
    <?=js('bootstrap/bootstrap.min.js')?>
    <?=js('app/custom.js')?>
    <?=js('plugins/underscore/underscore-min.js')?> 
    <?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
    <?=js('plugins/data-table/jquery.dataTables.min.js')?>
    <?=js('plugins/data-table/dataTables.bootstrap.min.js')?>
    <?=js('plugins/select2/select2.min.js')?>
    <?=js('plugins/tabletojson/jquery.tabletojson.js')?>

<script type="text/javascript">
    $(document).ready(function($){   
        
        $('#addRow').on('click', function () {
            var data = '<tr><td>'+$('#layanan').val()+'</td><td>'+$('#harga').val()+'</td><td><a class="btn btn-danger btn-xs hapus-dong"><i class="fa fa-close"></i></a></td></tr>';
              $('#tbl_lay').append(data);
              $('#layanan').val("");
              $('#harga').val("");
              $('#layanan').focus();
        } );
        $('#tbl_lay').on('click','a.hapus-dong',function(){
          $(this).parent().parent().remove();
        })
        $('#simpan').on('click',function(){
          var table = $('#tbl_lay').tableToJSON({
            ignoreColumns : [2]
          });
          var url   = "<?php echo base_url('/admin/poliklinik/save')?>";
          
          $('#dataLay').val(JSON.stringify(table));
          $('#form-poli').submit();
        });

        $('#poliklinik').addClass('active'); 
        $('#nama').on('change paste keyup', validate);
        $('#harga,#layanan').on('change paste keyup', validate2);
        validate();
        validate2();
    });

    function validate(){
        if ($('#nama').val().length    >   0) {
            $("#simpan").removeClass("disabled");
        }
        else {
            $("#simpan").addClass("disabled");
        }
    }
    function validate2(){
        if ($.isNumeric($('#harga').val()) == true) {
            $("#addRow").removeClass("disabled");
            $("#har").removeClass("has-error");
        }
        else {
              $("#addRow").addClass("disabled");
            if($('#harga').val().length > 0){
              $("#har").addClass("has-error");
              alert('Masukan hanya angka');
              $('#harga').focus();
            }
        }
    }
</script>