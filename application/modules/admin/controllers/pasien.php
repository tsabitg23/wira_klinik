<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pasien extends MX_Controller {

	private $_module;

	public function __construct() {
        parent::__construct();

        //set Module location
        $this->_level = 'admin';
        $this->_module = 'admin/pasien';
        $this->_header = 'layout/header_admin';
        $this->_footer = 'layout/footer';

        $this->load->model('pasien_model', 'pm');
        $this->load->helper('date'); 
    }

    public function cekstatuslogin() {
        if ($this->session->has_userdata('uname') == FALSE) {
            redirect(base_url());
        }
        elseif ($this->session->has_userdata('uname') == true && $this->session->userdata('level') != $this->_level) {
            redirect(base_url('/'.$this->session->userdata('level')));
        }
    }

    public function index()
    {   
        $this->cekstatuslogin();
        $data['pasien'] = $this->pm->getData()->result();
        $this->load->view($this->_header);
        $this->load->view($this->_module.'/index',$data);
        $this->load->view($this->_footer);
    }

    public function delete()
    {
        $id  = base64_decode($this->input->get('id'));
        $this->db->trans_begin();
        $this->pm->delete($id);
        if ($this->db->trans_status() === FALSE ) {
            $this->db->trans_rollback();
            echo "<script>
            alert('Data tidak Berhasil Dihapus kesalahan tertentu ');
            window.location.href='".base_url()."admin/pasien';
            </script>";
        }
        else {
            $this->db->trans_commit();
            echo "<script>
            alert('Data Berhasil Dihapus ');
            window.location.href='".base_url()."admin/pasien';
            </script>";

        }
    }

}
?>
