<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class poliklinik extends MX_Controller {

	private $_module;

	public function __construct() {
        parent::__construct();

        //set Module location
        $this->_level = 'admin';
        $this->_module = 'admin/poliklinik';
        $this->_header = 'layout/header_admin';
        $this->_footer = 'layout/footer';

        $this->load->helper('string');
        $this->load->helper('date');
        $this->load->model('poliklinik_model', 'polim');
        $this->load->model('layanan_model', 'layam');
 
    }

    public function cekstatuslogin() {
        if ($this->session->has_userdata('uname') == FALSE) {
            redirect(base_url());
        }
        elseif ($this->session->has_userdata('uname') == true && $this->session->userdata('level') != $this->_level) {
            redirect(base_url('/'.$this->session->userdata('level')));
        }
    }

    public function index()
    {   
        $this->cekstatuslogin();
        $data['poliklinik'] = $this->polim->getData()->result();
        $this->load->view($this->_header);
        $this->load->view($this->_module.'/index',$data);
        $this->load->view($this->_footer);
    }

    public function form(){
        $this->cekstatuslogin();

        $data['action'] = $this->_module.'/save';
        $id = base64_decode($this->input->get('id'));
        if($id != ""){
            $data['default']    = $this->polim->getData($id)->row();
        }
        elseif ($id == "") 
        {
            $data['default']        = "";
        }

        $this->load->view($this->_header);
        $this->load->view($this->_module.'/form',$data);
        $this->load->view($this->_footer);
    }

    public function formedit(){
        $this->cekstatuslogin();

        $data['action'] = $this->_module.'/save';
        $id = base64_decode($this->input->get('id'));
        if($id != ""){
            $data['default']    = $this->polim->getData($id)->row();
        }
        elseif ($id == "") 
        {
            $data['default']        = "";
        }

        $this->load->view($this->_header);
        $this->load->view($this->_module.'/formedit',$data);
        $this->load->view($this->_footer);
    }

    public function save(){

        $jenis = $this->input->post('jenis');
        $this->db->trans_begin();
        $layanan = json_decode($this->input->post('dataLay'));
        $dataIns   = $this->input->post("data");
        if($jenis == "add"){
            $dataIns['id_poliklinik'] = "PO".random_string('numeric',8);
        }

        if($this->input->post('id') != ""){
            $id = $this->input->post('id');
        }

        if ($jenis == "add") {
            $this->polim->save_as_new($dataIns);
            foreach ($layanan as $key) {
                $data['nama'] = $key->Layanan;
                $data['tarif'] = $key->Harga;
                $data['id_poliklinik'] = $dataIns['id_poliklinik'];
                $this->layam->save_as_new($data);
            }
        }
        else if($jenis == "edit"){
            $this->polim->save($dataIns, $id);
        }

        if ($this->db->trans_status() === FALSE ) {
            $this->db->trans_rollback();
            echo "<script>
            alert('Data tidak tersimpan karna kesalahan tertentu ');
            window.location.href='".base_url()."admin/poliklinik';
            </script>";
        }
        else {
            $this->db->trans_commit();
            echo "<script>
            alert('Data Berhasil Disimpan ');
            window.location.href='".base_url()."admin/poliklinik';
            </script>";   
        }
        
    }

    public function delete()
    {
        $id  = base64_decode($this->input->get('id'));
        $this->db->trans_begin();
        $this->polim->delete($id);
        if ($this->db->trans_status() === FALSE ) {
            $this->db->trans_rollback();
            echo "<script>
            alert('Data tidak Berhasil Dihapus kesalahan tertentu ');
            window.location.href='".base_url()."admin/poliklinik';
            </script>";
        }
        else {
            $this->db->trans_commit();
            echo "<script>
            alert('Data Berhasil Dihapus ');
            window.location.href='".base_url()."admin/poliklinik';
            </script>";

        }
    }

    public function detail(){
        $this->cekstatuslogin();
        $tahun = date('Y');
        $data['tahunnya'] = $tahun;
        $data['action'] = $this->_module.'/savelay';
        $id = base64_decode($this->input->get('id'));
        if($id != ""){
            $data['default']    = $this->polim->getData($id)->row();
            $data['layanan']    = $this->layam->getData($id)->result();
            if($this->input->post('tahun') != ''){
                $tahun = $this->input->post('tahun');
                $data['tahunnya'] = $tahun;
            }

            $data['penghasilan'] = $this->polim->getPendapatanBulanan($id,$tahun);
            $data['grafik'] = '';
            $data['grafik2'] = '';
            foreach ($data['penghasilan'] as $key) {
                $data['grafik'][] = (float)$key->jumlah_pasien;
            }
            foreach ($data['penghasilan'] as $key) {
                $data['grafik2'][] = (float)$key->pendapatan;
            }
        }
        elseif ($id == "") 
        {
            redirect(base_url('/admin/poliklinik'));
        }

        $this->load->view($this->_header);
        $this->load->view($this->_module.'/detail',$data);
        $this->load->view($this->_footer);
    }

    public function laporan($id){
        $tahun = $this->input->post('tahun');
        $data['tahun'] = $tahun;
        $data['info'] = $this->polim->getData($id)->row();
        $data['penghasilan'] = $this->polim->getPendapatanBulanan($id,$tahun);

        $this->load->view($this->_module.'/laporan',$data);
    }

    public function savelay(){
        $this->db->trans_begin();
        $dataIns   = $this->input->post("data");
        $id_p  = $this->input->post("id_p");
        $dataIns['id_poliklinik'] = $id_p;
        $id_l  = base64_decode($this->input->post("idLay"));

        if($id_l == "coy"){
            $this->layam->save_as_new($dataIns);
        }
        else{
            $this->layam->save($dataIns,$id_l);
        }
            
        
        if ($this->db->trans_status() === FALSE ) {
            $this->db->trans_rollback();
            echo "<script>
            alert('Data tidak tersimpan karna kesalahan tertentu ');
            window.location.href='".base_url()."admin/poliklinik/detail?id=".base64_encode($id_p)."#lay';
            </script>";
        }
        else {
            $this->db->trans_commit();
            echo "<script>
            alert('Data Berhasil Disimpan ');
            window.location.href='".base_url()."admin/poliklinik/detail?id=".base64_encode($id_p)."#lay';
            </script>";   
        }
        
    }

    public function deletelay()
    {
        $id  = base64_decode($this->input->get('id'));
        $id_p  = base64_decode($this->input->get('id_p'));
        $this->db->trans_begin();
        $this->layam->delete($id);
        if ($this->db->trans_status() === FALSE ) {
            $this->db->trans_rollback();
            echo "<script>
            alert('Data tidak Berhasil Dihapus kesalahan tertentu ');
            window.location.href='".base_url()."admin/poliklinik/detail?id=".base64_encode($id_p)."#lay';
            </script>";
        }
        else {
            $this->db->trans_commit();
            echo "<script>
            alert('Data Berhasil Dihapus ');
            window.location.href='".base_url()."admin/poliklinik/detail?id=".base64_encode($id_p)."#lay';
            </script>";

        }
    }

}
?>
