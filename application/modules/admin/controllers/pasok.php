<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pasok extends MX_Controller {

	private $_module;

	public function __construct() {
        parent::__construct();

        //set Module location
        $this->_level = 'admin';
        $this->_module = 'admin/obat/pasok';
        $this->_header = 'layout/header_admin';
        $this->_footer = 'layout/footer';

        $this->load->model('obat_pasok_model', 'pasokm');
 
    }

    public function cekstatuslogin() {
        if ($this->session->has_userdata('uname') == FALSE) {
            redirect(base_url());
        }
        elseif ($this->session->has_userdata('uname') == true && $this->session->userdata('level') != $this->_level) {
            redirect(base_url('/'.$this->session->userdata('level')));
        }
    }

    public function index()
    {   
        $this->cekstatuslogin();
        $data['pasok'] = $this->pasokm->getData()->result();
        $this->load->view($this->_header);
        $this->load->view($this->_level.'/pasok/index',$data);
        $this->load->view($this->_footer);
    }

    public function form(){
        $this->cekstatuslogin();

        $data['action'] = $this->_module.'/save';
        $data['obat'] = $this->pasokm->optionObat();
        $id = base64_decode($this->input->get('id'));
        $id_o = base64_decode($this->input->get('id_o'));
        if($id != "" && $id_o ==""){
            $data['default']    = $this->pasokm->getData($id)->row();
        }
        elseif ($id == "" && $id_o == "") 
        {
            $data['default']        = "";
        }
        elseif ($id == "" && $id_o != "") 
        {
            $data['default'] = new stdClass();
            $data['default']->id_obat = $id_o;
            $data['default']->harga = $this->pasokm->getHarga($id_o)->harga;
        }

        $this->load->view($this->_header);
        $this->load->view($this->_level.'/pasok/form',$data);
        $this->load->view($this->_footer);
    }

    public function save(){
            
        $id = $this->input->post('id');
        $this->db->trans_begin();
        $dataIns   = $this->input->post("data");
        $dataIns['id_admin'] = $this->session->userdata('id');
        if ($id == "") {
            $this->pasokm->save_as_new($dataIns);
        }
        else if($id != ""){
            $this->pasokm->save($dataIns, $id);
        }

        if ($this->db->trans_status() === FALSE ) {
            $this->db->trans_rollback();
            echo "<script>
            alert('Data tidak tersimpan karna kesalahan tertentu ');
            window.location.href='".base_url()."admin/obat/pasok';
            </script>";
        }
        else {
            $this->db->trans_commit();
            echo "<script>
            alert('Data Berhasil Disimpan ');
            window.location.href='".base_url()."admin/obat/pasok';
            </script>";   
        }
        
    }

    public function delete()
    {
        $id  = base64_decode($this->input->get('id'));
        $this->db->trans_begin();
        $this->pasokm->delete($id);
        if ($this->db->trans_status() === FALSE ) {
            $this->db->trans_rollback();
            echo "<script>
            alert('Data tidak Berhasil Dihapus kesalahan tertentu ');
            window.location.href='".base_url()."admin/obat/pasok';
            </script>";
        }
        else {
            $this->db->trans_commit();
            echo "<script>
            alert('Data Berhasil Dihapus ');
            window.location.href='".base_url()."admin/obat/pasok';
            </script>";

        }
    }

    public function harga($id_obat){
        $this->cekstatuslogin();
        echo $this->pasokm->getHarga($id_obat)->harga;
    }

}
?>
