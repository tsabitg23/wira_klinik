<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kat_obat extends MX_Controller {

	private $_module;

	public function __construct() {
        parent::__construct();

        //set Module location
        $this->_level = 'admin';
        $this->_module = 'admin/kat_obat';
        $this->_header = 'layout/header_admin';
        $this->_footer = 'layout/footer';

        $this->load->model('kategori_obat_model', 'km');
 
    }

    public function cekstatuslogin() {
        if ($this->session->has_userdata('uname') == FALSE) {
            redirect(base_url());
        }
        elseif ($this->session->has_userdata('uname') == true && $this->session->userdata('level') != $this->_level) {
            redirect(base_url('/'.$this->session->userdata('level')));
        }
    }

    public function index()
    {   
        $this->cekstatuslogin();
        $data['kategori_obat'] = $this->km->getData()->result();
        $this->load->view($this->_header);
        $this->load->view($this->_module.'/index',$data);
        $this->load->view($this->_footer);
    }

    public function form(){
        $this->cekstatuslogin();

        $data['action'] = $this->_module.'/save';
        $id = base64_decode($this->input->get('id'));
        if($id != ""){
            $data['default']    = $this->km->getData($id)->row();
        }
        elseif ($id == "") 
        {
            $data['default']        = "";
        }

        $this->load->view($this->_header);
        $this->load->view($this->_module.'/form',$data);
        $this->load->view($this->_footer);
    }

    public function save(){
            
        $id = $this->input->post('id');
        $this->db->trans_begin();
        $dataIns   = $this->input->post("data");

        if ($id == "") {
            $this->km->save_as_new($dataIns);
        }
        else if($id != ""){
            $this->km->save($dataIns, $id);
        }

        if ($this->db->trans_status() === FALSE ) {
            $this->db->trans_rollback();
            echo "<script>
            alert('Data tidak tersimpan karna kesalahan tertentu ');
            window.location.href='".base_url()."admin/kat_obat';
            </script>";
        }
        else {
            $this->db->trans_commit();
            echo "<script>
            alert('Data Berhasil Disimpan ');
            window.location.href='".base_url()."admin/kat_obat';
            </script>";   
        }
        
    }

    public function delete()
    {
        $id  = base64_decode($this->input->get('id'));
        $this->db->trans_begin();
        $this->km->delete($id);
        if ($this->db->trans_status() === FALSE ) {
            $this->db->trans_rollback();
            echo "<script>
            alert('Data tidak Berhasil Dihapus kesalahan tertentu ');
            window.location.href='".base_url()."admin/kat_obat';
            </script>";
        }
        else {
            $this->db->trans_commit();
            echo "<script>
            alert('Data Berhasil Dihapus ');
            window.location.href='".base_url()."admin/kat_obat';
            </script>";

        }
    }

}
?>
