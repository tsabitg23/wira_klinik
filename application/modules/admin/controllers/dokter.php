<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class dokter extends MX_Controller {

	private $_module;

	public function __construct() {
        parent::__construct();

        //set Module location
        $this->_level = 'admin';
        $this->_module = 'admin/dokter';
        $this->_header = 'layout/header_admin';
        $this->_footer = 'layout/footer';

        $this->load->helper('string');
        $this->load->model('dokter_model', 'dokem');
        $this->load->model('dokter_jadwal_model', 'jadm');
 
    }

    public function cekstatuslogin() {
        if ($this->session->has_userdata('uname') == FALSE) {
            redirect(base_url());
        }
        elseif ($this->session->has_userdata('uname') == true && $this->session->userdata('level') != $this->_level) {
            redirect(base_url('/'.$this->session->userdata('level')));
        }
    }

    public function index()
    {   
        $this->cekstatuslogin();
        $data['dokter'] = $this->dokem->getData()->result();
        $this->load->view($this->_header);
        $this->load->view($this->_module.'/index',$data);
        $this->load->view($this->_footer);
    }

    public function form(){
        $this->cekstatuslogin();

        $data['hari'] = array('1' => 'Senin', '2' => 'Selasa','3' => 'Rabu','4' => 'Kamis','5' => 'Jumat','6' => 'Sabtu','7' => 'Minggu');
        $data['poli'] = $this->dokem->optionPoli();
        $data['spesialis'] = $this->dokem->optionSpesialis();
        $data['action'] = $this->_module.'/save';
        $id = base64_decode($this->input->get('id'));
        if($id != ""){
            $data['default']    = $this->dokem->getData($id)->row();
        }
        elseif ($id == "") 
        {
            $data['default']        = "";
        }

        $this->load->view($this->_header);
        $this->load->view($this->_module.'/form',$data);
        $this->load->view($this->_footer);
    }

    public function formedit(){
        $this->cekstatuslogin();

        $data['action'] = $this->_module.'/save';
        $data['poli'] = $this->dokem->optionPoli();
        $data['spesialis'] = $this->dokem->optionSpesialis();
        $id = base64_decode($this->input->get('id'));
        if($id != ""){
            $data['default']    = $this->dokem->getData($id)->row();
        }
        elseif ($id == "") 
        {
            $data['default']        = "";
        }

        $this->load->view($this->_header);
        $this->load->view($this->_module.'/formedit',$data);
        $this->load->view($this->_footer);
    }

    public function save(){

        $jenis = $this->input->post('jenis');
        $this->db->trans_begin();
        $jadwal = json_decode($this->input->post('dataJad'));

        $dataIns   = $this->input->post("data");
        if($jenis == "add"){
            $dataIns['id_dokter'] = "DO".random_string('numeric',8);
        }

        if($this->input->post('id') != ""){
            $id = $this->input->post('id');
        }

        if ($jenis == "add") {
            $this->dokem->save_as_new($dataIns);
            foreach ($jadwal as $key) {
                $a;
                switch ($key->Hari) {
                    case 'Senin':
                        $a = 'Monday';
                        break;
                    case 'Selasa':
                        $a = 'Tuesday';
                        break;
                    case 'Rabu':
                        $a = 'Wednesday';
                        break;
                    case 'Kamis':
                        $a = 'Thursday';
                        break;
                    case 'Jumat':
                        $a = 'Friday';
                        break;
                    case 'Sabtu':
                        $a = 'Saturday';
                        break;
                    
                    default:
                        $a = 'Sunday';
                        break;
                }
                $data['hari'] = $a;
                $data['praktik_mulai'] = $key->Mulai;
                $data['praktik_selesai'] = $key->Selesai;
                $data['id_dokter'] = $dataIns['id_dokter'];
                $this->jadm->save_as_new($data);
            }
        }
        else if($jenis == "edit"){
            $this->dokem->save($dataIns, $id);
        }

        if ($this->db->trans_status() === FALSE ) {
            $this->db->trans_rollback();
            echo "<script>
            alert('Data tidak tersimpan karna kesalahan tertentu ');
            window.location.href='".base_url()."admin/dokter';
            </script>";
        }
        else {
            $this->db->trans_commit();
            echo "<script>
            alert('Data Berhasil Disimpan ');
            window.location.href='".base_url()."admin/dokter';
            </script>";   
        }
        
    }

    public function delete()
    {
        $id  = base64_decode($this->input->get('id'));
        $this->db->trans_begin();
        $this->dokem->delete($id);
        if ($this->db->trans_status() === FALSE ) {
            $this->db->trans_rollback();
            echo "<script>
            alert('Data tidak Berhasil Dihapus kesalahan tertentu ');
            window.location.href='".base_url()."admin/dokter';
            </script>";
        }
        else {
            $this->db->trans_commit();
            echo "<script>
            alert('Data Berhasil Dihapus ');
            window.location.href='".base_url()."admin/dokter';
            </script>";

        }
    }

    public function detail(){
        $this->cekstatuslogin();

        $data['action'] = $this->_module.'/savejad';
        $id = base64_decode($this->input->get('id'));
        if($id != ""){
            $data['default']    = $this->dokem->getData($id)->row();
            $data['jadwal']    = $this->jadm->getData($id)->result();
        }
        elseif ($id == "") 
        {
            redirect(base_url('/admin/dokter'));
        }
        $data['hari'] = array('Senin' => 'Senin', 'Selasa' => 'Selasa','Rabu' => 'Rabu','Kamis' => 'Kamis','Jumat' => 'Jumat','Sabtu' => 'Sabtu','Minggu' => 'Minggu');

        $this->load->view($this->_header);
        $this->load->view($this->_module.'/detail',$data);
        $this->load->view($this->_footer);
    }

    public function savejad(){
        $this->db->trans_begin();
        $dataIns   = $this->input->post("data");
        $id_d  = $this->input->post("id_d");
        $hari  = $this->input->post('hari');
        $a;
        switch ($hari) {
            case 'Senin':
                        $a = 'Monday';
                        break;
                    case 'Selasa':
                        $a = 'Tuesday';
                        break;
                    case 'Rabu':
                        $a = 'Wednesday';
                        break;
                    case 'Kamis':
                        $a = 'Thursday';
                        break;
                    case 'Jumat':
                        $a = 'Friday';
                        break;
                    case 'Sabtu':
                        $a = 'Saturday';
                        break;
                    
                    default:
                        $a = 'Sunday';
                        break;
        }
        $dataIns['id_dokter'] = $id_d;
        $dataIns['hari'] = $a;
        $id_l  = base64_decode($this->input->post("id_d_j"));

        if($id_l == "coy"){
            $this->jadm->save_as_new($dataIns);
        }
        else{
            $this->jadm->save($dataIns,$id_l);
        }
            
        
        if ($this->db->trans_status() === FALSE ) {
            $this->db->trans_rollback();
            echo "<script>
            alert('Data tidak tersimpan karna kesalahan tertentu ');
            window.location.href='".base_url()."admin/dokter/detail?id=".base64_encode($id_d)."#jad';
            </script>";
        }
        else {
            $this->db->trans_commit();
            echo "<script>
            alert('Data Berhasil Disimpan ');
            window.location.href='".base_url()."admin/dokter/detail?id=".base64_encode($id_d)."#jad';
            </script>";   
        }
        
    }

    public function deletejad()
    {
        $id  = base64_decode($this->input->get('id'));
        $id_d  = base64_decode($this->input->get('id_d'));
        $this->db->trans_begin();
        $this->jadm->delete($id);
        if ($this->db->trans_status() === FALSE ) {
            $this->db->trans_rollback();
            echo "<script>
            alert('Data tidak Berhasil Dihapus kesalahan tertentu ');
            window.location.href='".base_url()."admin/dokter/detail?id=".base64_encode($id_d)."#jad';
            </script>";
        }
        else {
            $this->db->trans_commit();
            echo "<script>
            alert('Data Berhasil Dihapus ');
            window.location.href='".base_url()."admin/dokter/detail?id=".base64_encode($id_d)."#jad';
            </script>";

        }
    }



}
?>
