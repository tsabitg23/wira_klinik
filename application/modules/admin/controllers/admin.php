<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller {

	private $_module;

	public function __construct() {
        parent::__construct();

        //set Module location
        $this->_level = 'admin';
        $this->_module = 'admin';
        $this->_header = 'layout/header_'.$this->_module;
        $this->_footer = 'layout/footer';
        
        $this->load->helper('string');
        $this->load->model('dashboard_model','dash');
        $this->load->model('poliklinik_model','polim');
    }
	public function index()
	{	
        $jenis = 'date';
        $tanggal = date('Y-m-d');
        $tahun = date('Y');
        $data['tanggalnya'] = date('d F Y');
        $data['monthly_income'] = $this->dash->getMonthlyIncome()->income;
        $data['monthly_pasien'] = $this->dash->getMonthlyPasien()->pasien;
        $data['daily_income'] = $this->dash->getDailyIncome()->income;
        $data['daily_pasien'] = $this->dash->getDailyPasien()->pasien;
        
        $data['grafik'] = '';
        $data['grafik2'] = '';
        $data['poliklinik'] = $this->polim->getViewPendapatan($jenis,$tanggal,$tahun);
        $no = 0;
        $no2 = 0;
        foreach ($data['poliklinik'] as $key) {
            $data['grafik'][$no] = new StdClass();
            $data['grafik'][$no]->name = $key->nama;
            $data['grafik'][$no]->data[] = (float)$key->pendapatan;
            $no++;
        }

        foreach ($data['poliklinik'] as $key) {
            $data['grafik2'][$no2] = new StdClass();
            $data['grafik2'][$no2]->name = $key->nama;
            $data['grafik2'][$no2]->data[] = (float)$key->jumlah_pasien;
            $no2++;
        }


        $this->cekstatuslogin();
		$this->load->view($this->_header);
		$this->load->view($this->_module.'/index',$data);
		$this->load->view($this->_footer);
		
	}

    public function cekstatuslogin() {
        if ($this->session->has_userdata('uname') == FALSE) {
            redirect(base_url());
        }
        elseif ($this->session->has_userdata('uname') == true && $this->session->userdata('level') != $this->_level) {
            redirect(base_url('/'.$this->session->userdata('level')));
        }
    }


}
?>
