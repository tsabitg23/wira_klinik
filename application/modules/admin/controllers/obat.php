<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class obat extends MX_Controller {

	private $_module;

	public function __construct() {
        parent::__construct();

        //set Module location
        $this->_level = 'admin';
        $this->_module = 'admin/obat';
        $this->_header = 'layout/header_admin';
        $this->_footer = 'layout/footer';

        $this->load->model('obat_model', 'obm');
 
    }

    public function cekstatuslogin() {
        if ($this->session->has_userdata('uname') == FALSE) {
            redirect(base_url());
        }
        elseif ($this->session->has_userdata('uname') == true && $this->session->userdata('level') != $this->_level) {
            redirect(base_url('/'.$this->session->userdata('level')));
        }
    }

    public function index()
    {   
        $this->cekstatuslogin();
        $data['obat'] = $this->obm->getData()->result();
        $this->load->view($this->_header);
        $this->load->view($this->_module.'/index',$data);
        $this->load->view($this->_footer);
    }

    public function form(){
        $this->cekstatuslogin();

        $data['action'] = $this->_module.'/save';
        $data['jenis'] = $this->obm->optionJenis();
        $data['umur'] = array('kosong'=>'-- Pilih Rating --','D' => 'Dewasa' ,'A' => 'Anak-anak','SU' => 'Semua Umur' );
        $data['kategori'] = $this->obm->optionKat();
        $id = base64_decode($this->input->get('id'));
        if($id != ""){
            $data['default']    = $this->obm->getData($id)->row();
        }
        elseif ($id == "") 
        {
            $data['default']        = "";
        }

        $this->load->view($this->_header);
        $this->load->view($this->_module.'/form',$data);
        $this->load->view($this->_footer);
    }

    public function save(){
            
        $id = $this->input->post('id');
        $this->db->trans_begin();
        $dataIns   = $this->input->post("data");

        if ($id == "") {
            $this->obm->save_as_new($dataIns);
        }
        else if($id != ""){
            $this->obm->save($dataIns, $id);
        }

        if ($this->db->trans_status() === FALSE ) {
            $this->db->trans_rollback();
            echo "<script>
            alert('Data tidak tersimpan karna kesalahan tertentu ');
            window.location.href='".base_url()."admin/obat';
            </script>";
        }
        else {
            $this->db->trans_commit();
            echo "<script>
            alert('Data Berhasil Disimpan ');
            window.location.href='".base_url()."admin/obat';
            </script>";   
        }
        
    }

    public function delete()
    {
        $id  = base64_decode($this->input->get('id'));
        $this->db->trans_begin();
        $this->obm->delete($id);
        if ($this->db->trans_status() === FALSE ) {
            $this->db->trans_rollback();
            echo "<script>
            alert('Data tidak Berhasil Dihapus kesalahan tertentu ');
            window.location.href='".base_url()."admin/obat';
            </script>";
        }
        else {
            $this->db->trans_commit();
            echo "<script>
            alert('Data Berhasil Dihapus ');
            window.location.href='".base_url()."admin/obat';
            </script>";

        }
    }

}
?>
