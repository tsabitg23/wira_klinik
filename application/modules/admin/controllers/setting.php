<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class setting extends MX_Controller {

	private $_module;

	public function __construct() {
        parent::__construct();

        //set Module location
        $this->_level  = 'admin';
        $this->_module = 'admin/setting';
        $this->_header = 'layout/header_admin';
        $this->_footer = 'layout/footer';

        $this->load->model('admin_model', 'setm');
 
    }

    public function cekstatuslogin() {
        if ($this->session->has_userdata('uname') == FALSE) {
            redirect(base_url());
        }
        elseif ($this->session->has_userdata('uname') == true && $this->session->userdata('level') != $this->_level) {
            redirect(base_url('/'.$this->session->userdata('level')));
        }
    }


    public function index(){
        $this->cekstatuslogin();

        $data['action'] = $this->_module.'/save';
        $id = $this->session->userdata('id');
        $data['default']    = $this->setm->getData($id)->row();
        
        $this->load->view($this->_header);
        $this->load->view($this->_module.'/index',$data);
        $this->load->view($this->_footer);
    }

    public function save(){
        $id = $this->input->post('id');
        $jenis = $this->input->post('jenis');
        $dataIns   = $this->input->post("data");
        $this->db->trans_begin();
        if($jenis == 'pass'){
            $result = $this->setm->cekOldPass($id,md5($this->input->post('oldpass')));
            if($result){

                $dataIns['password'] = md5($dataIns['password']);
                
                
            }
            else{
                echo "<script>
                    alert('Password Lama Salah');
                    window.location.href='".base_url()."admin/setting#pass';
                    </script>"; 
            }    
        }
        
        $this->setm->save($dataIns, $id);
        

        if ($this->db->trans_status() === FALSE ) {
            $this->db->trans_rollback();
            echo "<script>
            alert('Data tidak tersimpan karna kesalahan tertentu ');
            window.location.href='".base_url()."admin/setting';
            </script>";
        }
        else {
            $this->db->trans_commit();
            echo "<script>
            alert('Data Berhasil Disimpan ');
            window.location.href='".base_url()."admin/setting';
            </script>";   
        }
        
    }

}
?>
