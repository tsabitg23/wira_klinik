<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class backup extends MX_Controller {

	private $_module;

	public function __construct() {
        parent::__construct();

        //set Module location
        $this->_level = 'admin';
        $this->_module = 'admin/backup';
        $this->_header = 'layout/header_admin';
        $this->_footer = 'layout/footer';
        
    }

    public function cekstatuslogin() {
        if ($this->session->has_userdata('uname') == FALSE) {
            redirect(base_url());
        }
        elseif ($this->session->has_userdata('uname') == true && $this->session->userdata('level') != $this->_level) {
            redirect(base_url('/'.$this->session->userdata('level')));
        }
    }

    public function index()
    {   
        $this->cekstatuslogin();
        $data = array();
        $this->load->view($this->_header);
        $this->load->view($this->_module.'/index',$data);
        $this->load->view($this->_footer);
    }

    public function do_backup(){
        // Load the DB utility class
        $this->load->dbutil();

        // Backup database dan dijadikan variable
        $backup = $this->dbutil->backup();

        // Load file helper dan menulis ke server untuk keperluan restore
        $this->load->helper('file');
        write_file('/backup/database/wiraDb-'.date('Y-m-d').'.sql.zip', $backup);

        // Load the download helper dan melalukan download ke komputer
        $this->load->helper('download');
        force_download('wiraDb-'.date('Y-m-d').'.sql.zip', $backup);
    }



}
?>
