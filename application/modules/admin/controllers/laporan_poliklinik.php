<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class laporan_poliklinik extends MX_Controller {

	private $_module;

	public function __construct() {
        parent::__construct();

        //set Module location
        $this->_level = 'admin';
        $this->_module = 'admin/laporan_poliklinik';
        $this->_header = 'layout/header_admin';
        $this->_footer = 'layout/footer';

        $this->load->helper('date');
        $this->load->model('poliklinik_model','polim');
        
    }
    public function index()
    {   
        $jenis = 'date';
        $tanggal = date('Y-m-d');
        $tahun = date('Y');
        $data['hid_jenis'] = 'date';
        $data['hid_tanggal'] = $tanggal;
        $data['hid_tahun'] = $tahun;
        $data['jenisnya'] = '';
        $data['tanggalnya'] = date('d F Y');
        $data['jenis_satuan'] = array('date'=>'Hari','month'=>'Bulan','year'=>'Tahun');
        $data['bulan'] = array('1'=>'Januari','2'=>'Februari','3'=>'Maret','4'=>'April','5'=>'Mei','6'=>'Juni','7'=>'Juli','8'=>'Agustus','9'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
        if($this->input->get('jenis') != ''){
         
            $jenis = $this->input->get('jenis');
            $tanggal = $this->input->get('tanggal');
            $tahun = $this->input->get('tahun');
            if($jenis =='date'){
                $tahun = nice_date($tanggal,'Y');
                $data['tanggalnya'] = nice_date($tanggal,'d F Y');
            }
            else if($jenis == 'month'){
                $data['tanggalnya'] = nice_date("2017-".$tanggal.'-01','F').' Tahun '.$tahun;
            }
            else{
                $data['tanggalnya'] = ' Tahun '.$tahun;
            }
            $data['hid_jenis'] = $jenis;
            $data['hid_tanggal'] = $tanggal;
            $data['hid_tahun'] = $tahun;
        }
        $data['grafik'] = '';
        $data['grafik2'] = '';
        $data['poliklinik'] = $this->polim->getViewPendapatan($jenis,$tanggal,$tahun);
        $no = 0;
        $no2 = 0;
        foreach ($data['poliklinik'] as $key) {
            $data['grafik'][$no] = new StdClass();
            $data['grafik'][$no]->name = $key->nama;
            $data['grafik'][$no]->data[] = (float)$key->pendapatan;
            $no++;
        }

        foreach ($data['poliklinik'] as $key) {
            $data['grafik2'][$no2] = new StdClass();
            $data['grafik2'][$no2]->name = $key->nama;
            $data['grafik2'][$no2]->data[] = (float)$key->jumlah_pasien;
            $no2++;
        }

        $this->cekstatuslogin();
		$this->load->view($this->_header);
		$this->load->view($this->_module.'/index',$data);
		$this->load->view($this->_footer);
		
	}

    public function laporan(){

        if($this->input->get('jenis') != ''){
            $jenis = $this->input->get('jenis');
            $tanggal = $this->input->get('tanggal');
            $tahun = $this->input->get('tahun');
            if($jenis =='date'){
                $tahun = nice_date($tanggal,'Y');
                $data['tanggalnya'] = nice_date($tanggal,'d F Y');
            }
            else if($jenis == 'month'){
                $data['tanggalnya'] = nice_date("2017-".$tanggal.'-01','F').' Tahun '.$tahun;
            }
            else{
                $data['tanggalnya'] = ' Tahun '.$tahun;
            }
        }
        $data['jenis'] = $this->input->get('jenis');
        $data['poliklinik'] = $this->polim->getViewPendapatan($jenis,$tanggal,$tahun);
        $this->load->view($this->_module.'/laporan',$data);
    }

    public function cekstatuslogin() {
        if ($this->session->has_userdata('uname') == FALSE) {
            redirect(base_url());
        }
        elseif ($this->session->has_userdata('uname') == true && $this->session->userdata('level') != $this->_level) {
            redirect(base_url('/'.$this->session->userdata('level')));
        }
    }


}
?>
