<?php

class dokter_jadwal_model extends CI_Model{
    private $_table           = 'dokter_jadwal';
    protected $primary_key  = 'id_d_jadwal';

    public function save_as_new($data) {
        $this->db->set('crt','now()',false);
        $this->db->set('mdf','now()',false);
        $this->db->set($this->primary_key,'uuid()',false);
        $this->db->insert($this->_table, $data);
    }

    // untuk update data
    public function save($data, $key) {
        $this->db->set('mdf','now()',false);
        $this->db->update($this->_table, $data, array($this->primary_key => $key));
    }

    // untuk update data
    public function delete($key) {
        $this->db->set('dlt','now()',false);
        $this->db->where(array($this->primary_key => $key));
        $this->db->update($this->_table);
    }

    public function getData($dok,$key    = ""){
        $this->db->select("*");
        $this->db->where("id_dokter",$dok);
        $this->db->where('dlt',null);
        $this->db->from($this->_table);

        if($key != "")
            $this->db->where($this->primary_key,$key);

        return $this->db->get();
    }

}