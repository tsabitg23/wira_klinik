<?php

class admin_model extends CI_Model{
    private $_table           = 'admin';
    private $_table1           = 'config_kat_admin';
    protected $primary_key  = 'id_admin';

    public function save_as_new($data) {
        $this->db->set('crt','now()',false);
        $this->db->set('mdf','now()',false);
        $this->db->set($this->primary_key,'uuid()',false);
        $this->db->insert($this->_table, $data);
    }

    // untuk update data
    public function save($data, $key) {
        $this->db->set('mdf','now()',false);
        $this->db->update($this->_table, $data, array($this->primary_key => $key));
    }

    // untuk update data
    public function delete($key) {
        $this->db->set('dlt','now()',false);
        $this->db->where(array($this->primary_key => $key));
        $this->db->update($this->_table);
    }

    public function getData($key    = ""){
        $this->db->select("a.*,b.nama as jenis");
        $this->db->from($this->_table.' a');
        $this->db->join('config_kat_admin b','b.id_c_kat_admin = a.id_c_kat_admin','inner');
        $this->db->where('a.dlt',null);

        if($key != "")
            $this->db->where($this->primary_key,$key);

        return $this->db->get();
    }

    public function optionLevel($default = '-- Pilih Level --', $key = '') {
        $option = array();
        $this->db->where('dlt',null);
        $this->db->from($this->_table1);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();

        if (!empty($default))
            $option['kosong'] = $default;

        foreach ($list->result() as $row) {
            $option[$row->id_c_kat_admin] = $row->nama;
        }

        return $option;
    }

    public function cekOldPass($id,$old){
           $this->db->select('*');
           $this->db->from('admin');
           $this->db->where('id_admin', $id);
           $this->db->where('password', $old);
           $this->db->limit(1);
           $query = $this->db->get();
               if($query->num_rows() == 1){
                 return $query->result();
               }
               else{
                 return false; 
               }
    }

}