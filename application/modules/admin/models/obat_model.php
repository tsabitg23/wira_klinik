<?php

class obat_model extends CI_Model{
    private $_table           = 'obat';
    private $_table1           = 'config_jenis_obat';
    private $_table2           = 'config_kat_obat';
    protected $primary_key  = 'id_obat';

    public function save_as_new($data) {
        $this->db->set('crt','now()',false);
        $this->db->set('mdf','now()',false);
        $this->db->set($this->primary_key,'uuid()',false);
        $this->db->insert($this->_table, $data);
    }

    // untuk update data
    public function save($data, $key) {
        $this->db->set('mdf','now()',false);
        $this->db->update($this->_table, $data, array($this->primary_key => $key));
    }

    // untuk update data
    public function delete($key) {
        $this->db->set('dlt','now()',false);
        $this->db->where(array($this->primary_key => $key));
        $this->db->update($this->_table);
    }

    public function getData($key    = ""){
        $this->db->select("a.*,b.nama as jenis_obat, c.nama as kat_obat");
        $this->db->join('config_jenis_obat b','b.id_c_jenis_obat = a.id_c_jenis_obat','inner');
        $this->db->join('config_kat_obat c','c.id_c_kat_obat = a.id_c_kat_obat','inner');
        $this->db->where('a.dlt',null);
        $this->db->from($this->_table.' a');

        if($key != "")
            $this->db->where($this->primary_key,$key);

        return $this->db->get();
    }

    public function optionJenis($default = '-- Pilih Jenis Obat --',$key = '') {
        $option = array();
        $this->db->where('dlt',null);
        $this->db->from($this->_table1);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();

        if (!empty($default))
            $option['kosong'] = $default;

        foreach ($list->result() as $row) {
            $option[$row->id_c_jenis_obat] = $row->nama;
        }

        return $option;
    }

    public function optionKat($default = '-- Pilih Kategori Obat --' ,$key = '') {
        $option = array();
        $this->db->where('dlt',null);
        $this->db->from($this->_table2);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();

        if (!empty($default))
            $option['kosong'] = $default;

        foreach ($list->result() as $row) {
            $option[$row->id_c_kat_obat] = $row->nama;
        }

        return $option;
    }

}