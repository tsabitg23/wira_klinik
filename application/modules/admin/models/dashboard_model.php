<?php

class dashboard_model extends CI_Model{

    public function getDailyIncome($key    = ""){
    
        $query = $this->db->query("select ifnull(sum(pembayaran.jumlah),0)as income from pembayaran where date(pembayaran.tgl) = date(now())");
        return $query->row();
    }

    public function getMonthlyIncome($key    = ""){
    
        $query = $this->db->query("select ifnull(sum(pembayaran.jumlah),0)as income from pembayaran where month(pembayaran.tgl) = month(now())");
        return $query->row();
    }

    public function getDailyPasien($key    = ""){
    
        $query = $this->db->query("select ifnull(count(pendaftaran.id_pendaftaran),0)as pasien from pendaftaran where date(pendaftaran.tgl) = date(now())");
        return $query->row();
    }

    public function getMonthlyPasien($key    = ""){
    
        $query = $this->db->query("select ifnull(count(pendaftaran.id_pendaftaran),0)as pasien from pendaftaran where month(pendaftaran.tgl) = month(now())");
        return $query->row();
    }

}