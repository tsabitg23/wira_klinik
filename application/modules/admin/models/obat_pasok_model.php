<?php

class obat_pasok_model extends CI_Model{
    private $_table           = 'obat_pemasokan';
    private $_table1           = 'obat';
    protected $primary_key  = 'id_pemasokan';

    public function save_as_new($data) {
        $this->db->set('tgl','now()',false);
        $this->db->set('crt','now()',false);
        $this->db->set('mdf','now()',false);
        $this->db->set($this->primary_key,'uuid()',false);
        $this->db->insert($this->_table, $data);
    }

    // untuk update data
    public function save($data, $key) {
        $this->db->set('mdf','now()',false);
        $this->db->update($this->_table, $data, array($this->primary_key => $key));
    }

    public function delete($key) {
        $this->db->delete($this->_table, array($this->primary_key => $key));
    }

    public function getData($key    = ""){
        $this->db->select("a.*,b.nama as nama_obat, c.username as admin");
        $this->db->join('obat b','b.id_obat = a.id_obat','inner');
        $this->db->join('admin c','c.id_admin = a.id_admin','inner');
        $this->db->where('a.dlt',null);
        $this->db->from($this->_table.' a');

        if($key != "")
            $this->db->where($this->primary_key,$key);

        return $this->db->get();
    }

    public function optionObat($default = '--Pilih Obat--',$key = '') {
        $option = array();
        $this->db->where('dlt',null);
        $this->db->from($this->_table1);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();

        if (!empty($default))
            $option['kosong'] = $default;

        foreach ($list->result() as $row) {
            $option[$row->id_obat] = $row->nama;
        }

        return $option;
    }

    public function getHarga($key = ''){
        $this->db->select("a.harga");
        $this->db->where('a.dlt',null);
        $this->db->from($this->_table1.' a');

        if($key != "")
            $this->db->where('id_obat',$key);

        return $this->db->get()->row();   
    }

}