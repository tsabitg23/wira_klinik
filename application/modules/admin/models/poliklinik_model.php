<?php

class poliklinik_model extends CI_Model{
    private $_table           = 'poliklinik';
    private $_table1           = 'poliklinik_layanan';
    protected $primary_key  = 'id_poliklinik';

    public function save_as_new($data) {
        $this->db->set('crt','now()',false);
        $this->db->set('mdf','now()',false);
        $this->db->insert($this->_table, $data);
    }

    // untuk update data
    public function save($data, $key) {
        $this->db->set('mdf','now()',false);
        $this->db->update($this->_table, $data, array($this->primary_key => $key));
    }

    // untuk update data
    public function delete($key) {
        $this->db->set('dlt','now()',false);
        $this->db->where(array($this->primary_key => $key));
        $this->db->update($this->_table);

        $this->db->set('dlt','now()',false);
        $this->db->where(array('id_poliklinik' => $key));
        $this->db->update($this->_table1);

    }

    public function getData($key    = ""){
        $this->db->select("*");
        $this->db->where('dlt',null);
        $this->db->from($this->_table);

        if($key != "")
            $this->db->where($this->primary_key,$key);

        return $this->db->get();
    }

    public function getPendapatanBulanan($id_poliklinik,$tahun){
       $query = $this->db->query("CALL pendapatan_bulanan('".$id_poliklinik."','".$tahun."')");
       return $query->result();
    }

    public function getViewPendapatan($jenis,$tanggal,$tahun){
       $query = $this->db->query("CALL v_pendapatan('".$jenis."','".$tanggal."','".$tahun."')");
       return $query->result();
    }
}