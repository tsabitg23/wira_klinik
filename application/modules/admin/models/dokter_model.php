<?php

class dokter_model extends CI_Model{
    private $_table           = 'dokter';
    private $_table1          = 'poliklinik';
    private $_table2           = 'config_spesialis';
    private $_table3           = 'dokter_jadwal';
    protected $primary_key  = 'id_dokter';

    public function save_as_new($data) {
        $this->db->set('crt','now()',false);
        $this->db->set('mdf','now()',false);
        $this->db->insert($this->_table, $data);
    }

    // untuk update data
    public function save($data, $key) {
        $this->db->set('mdf','now()',false);
        $this->db->update($this->_table, $data, array($this->primary_key => $key));
    }

    // untuk update data
    public function delete($key) {
        $this->db->set('dlt','now()',false);
        $this->db->where(array($this->primary_key => $key));
        $this->db->update($this->_table);

        $this->db->set('dlt','now()',false);
        $this->db->where(array('id_dokter' => $key));
        $this->db->update($this->_table3);
    }

    public function getData($key    = ""){
        $this->db->select("a.*,b.nama as poliklinik, c.nama as spesialis");
        $this->db->join($this->_table1.' b','b.id_poliklinik=a.id_poliklinik','inner');
        $this->db->join($this->_table2.' c','c.id_c_spesialis=a.id_c_spesialis','inner');
        $this->db->where('a.dlt',null);
        $this->db->from($this->_table.' a');

        if($key != "")
            $this->db->where($this->primary_key,$key);

        return $this->db->get();
    }

    public function optionPoli($default = '-- Pilih Poliklinik --', $key = '') {
        $option = array();
        $this->db->where('dlt',null);
        $this->db->from($this->_table1);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();

        if (!empty($default))
            $option['kosong'] = $default;

        foreach ($list->result() as $row) {
            $option[$row->id_poliklinik] = $row->nama;
        }

        return $option;
    }

    public function optionSpesialis($default = '-- Pilih Spesialis --', $key = '') {
        $option = array();
        $this->db->where('dlt',null);
        $this->db->from($this->_table2);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();

        if (!empty($default))
            $option['kosong'] = $default;

        foreach ($list->result() as $row) {
            $option[$row->id_c_spesialis] = $row->nama;
        }

        return $option;
    }

}