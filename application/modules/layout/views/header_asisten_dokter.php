<!DOCTYPE html>
<html lang="en">

<head>
  
    <link rel="icon" type="image/png" href="<?php echo base_url('/assets/img/logo/icon.png');?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?=css('font/bariol.css');?>
    <?=css('plugins/select2/select2.min.css');?>
    <?=css('bootstrap/bootstrap.css');?> 
    <?=css('fontawesome/font-awesome.css');?>
    <?=css('font/roboto.css');?>
    <?=css('main/main.css');?>
    <?=css('plugins/data-table/dataTables.bootstrap.min.css');?>

</head>
<body data-ng-app>
    	
    
	<aside class="left-panel">
    		
            <div class="user text-center">
                  <?=img('logo/icon.png');?>
                  <h4 class="user-name">WIRA<span id="poli"> Polyclinic</span></h4>
            </div>
            
            
            
            <nav class="navigation">
            	<ul class="list-unstyled">
                	<li id="pemeriksaan"><a href="<?php echo base_url('/asisten_dokter'); ?>"><i class="fa fa-stethoscope"></i><span class="nav-label">Pemeriksaan</span></a></li>
                    <li id="pengaturan"><a href="<?php echo base_url('/asisten_dokter/setting'); ?>"><i class="fa fa-gear"></i> <span class="nav-label">Setting Akun</span></a></li>
                    <li id="logout"><a href="<?php echo base_url('/login/logout');?>"><i class="fa fa-sign-out"></i> <span class="nav-label">Logout</span></a>
                    </li>
                </ul>
            </nav>
            
    </aside>
    <!-- Aside Ends-->
    
    <section class="content">
    	
        <header class="top-head container-fluid">
            <button type="button" class="navbar-toggle pull-left">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            
            
            <nav class=" navbar-default hidden-xs" role="navigation">
                <ul class="nav navbar-nav">
                <li class="dropdown">
                  <a data-toggle="dropdown" class="dropdown-toggle" href="#"><?php echo $this->session->userdata('nama');?><span class="caret"></span></a>
                  <ul role="menu" class="dropdown-menu">
                    <li><a href="#">Setting Akun</a></li>
                    <li><a href="<?php echo base_url('/login/logout');?>">Logout</a></li>
                  </ul>
                </li>
              </ul>
            </nav>

        </header>