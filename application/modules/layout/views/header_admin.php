<!DOCTYPE html>
<html lang="en">

<head>
  
    <link rel="icon" type="image/png" href="<?php echo base_url('/assets/img/logo/icon.png');?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?=css('font/bariol.css');?>
    <?=css('plugins/select2/select2.min.css');?>
    <?=css('bootstrap/bootstrap.css');?> 
    <?=css('fontawesome/font-awesome.css');?>
    <?=css('font/roboto.css');?>
    <?=css('main/main.css');?>
    <?=css('plugins/data-table/dataTables.bootstrap.min.css');?>
    <?=css('plugins/highcharts/highcharts.css');?>

</head>
<body data-ng-app>
    	
    
	<aside class="left-panel">
    		
            <div class="user text-center">
                  <?=img('logo/icon.png');?>
                  <h4 class="user-name">WIRA<span id="poli"> Polyclinic</span></h4>
            </div>
            
            
            
            <nav class="navigation" >
              <ul class="list-unstyled" id="menunya">
                  <li><a href="<?php echo base_url('/admin'); ?>"><i class="fa fa-tasks"></i><span class="nav-label">Dashboard</span></a></li>
                    <li id="poliklinik"><a href="<?php echo base_url('/admin/poliklinik');?>"><i class="fa fa-hospital-o"></i><span class="nav-label">Poliklinik</span></a>
                    </li>
                    <li id="dokter"><a href="<?php echo base_url('/admin/dokter');?>"><i class="fa fa-user-md"></i> <span class="nav-label">Dokter</span></a>
                    </li>
                    <li class="has-submenu" id="obat"><a href="#"><i class="fa fa-plus-circle"></i> <span class="nav-label">Obat</span></a>
                      <ul class="list-unstyled">
                          <li id="data"><a href="<?php echo base_url('/admin/obat');?>">Data Obat</a></li>
                          <li id="pasok"><a href="<?php echo base_url('/admin/obat/pasok');?>">Pemasokan</a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo base_url('/admin/pasien'); ?>"><i class="fa fa-frown-o"></i> <span class="nav-label">Pasien</span></a>
                    </li>
                    <li class="has-submenu" id="laporan"><a href="#"><i class="fa fa-file-text-o"></i> <span class="nav-label">Laporan</span></a>
                      <ul class="list-unstyled">
                          <li id="pendapatan"><a href="<?php echo base_url('/admin/laporan_poliklinik');?>">Pendapatan Poliklinik</a></li>
                        </ul>
                    </li>
                    <li class="has-submenu" id="konfigurasi"><a href="#"><i class="fa fa-gears"></i> <span class="nav-label">Konfigurasi</span></a>
                      <ul class="list-unstyled">
                          <li id="spesialis"><a href="<?php echo base_url('/admin/spesialis');?>">Spesialis Dokter</a></li>
                          <li id="obat_jenis"><a href="<?php echo base_url('admin/jenis_obat');?>">Jenis Obat</a></li>
                          <li id="obat_kat"><a href="<?php echo base_url('admin/kat_obat');?>">Kategori Obat</a></li>
                          <li id="backup"><a href="<?php echo base_url('admin/backup');?>">Backup Database</a></li>
                        </ul>
                    </li>
                    <li id="admin"><a href="<?php echo base_url('/admin/list_admin');?>"><i class="fa fa-user-o"></i> <span class="nav-label">Admin</span></a>
                    </li>
                    <li id="pengaturan"><a href="<?php echo base_url('/admin/setting');?>"><i class="fa fa-gear"></i> <span class="nav-label">Setting Akun</span></a>
                    <li><a href="<?php echo base_url('/login/logout');?>"><i class="fa fa-sign-out"></i> <span class="nav-label">Logout</span></a>
                    </li>
                </ul>
            </nav>
            
    </aside>
    <!-- Aside Ends-->
    
    <section class="content">
    	
        <header class="top-head container-fluid">
          <span class="haha">
            <button type="button" class="navbar-toggle pull-left">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
          </span>
            
            <nav class=" navbar-default" role="navigation">
                <ul class="nav navbar-nav">
                <li class="dropdown">
                  <a data-toggle="dropdown" class="dropdown-toggle" href="#"><?php echo $this->session->userdata('nama');?><span class="caret"></span></a>
                  <ul role="menu" class="dropdown-menu">
                    <li><a href="<?php echo base_url('/admin/setting');?>">Setting Akun</a></li>
                    <li><a href="<?php echo base_url('/login/logout');?>">Logout</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
            
        </header>