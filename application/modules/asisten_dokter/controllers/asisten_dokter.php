<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Asisten_dokter extends MX_Controller {

	private $_module;

	public function __construct() {
        parent::__construct();

        //set Module location
        $this->_level = 'asisten_dokter';
        $this->_module = 'asisten_dokter';
        $this->_header = 'layout/header_'.$this->_module;
        $this->_footer = 'layout/footer';
        
        $this->load->model('pemeriksaan_model','pemem'); 
        $this->load->model('resep_model','resep'); 
        $this->load->helper('string');
    }

    public function cekstatuslogin() {
        if ($this->session->has_userdata('uname') == FALSE) {
            redirect(base_url());
        }
        elseif ($this->session->has_userdata('uname') == true && $this->session->userdata('level') != $this->_level) {
            redirect(base_url('/'.$this->session->userdata('level')));
        }
    }

	public function index()
	{	
        $this->cekstatuslogin();
        $data['hasil_daftar'] = new stdClass();
        $data['hasil_daftar']->id_pendaftaran = '123';
        $data['hasil_daftar']->keluhan = '123';
        $data['hasil_daftar']->diagnosis = '123';
        $data['resephasilnya'][0] = new stdClass();
        $data['resephasilnya'][0]->nama_obat = '123';
        $data['resephasilnya'][0]->dosis = '123';
        $data['resephasilnya'][0]->jumlah = '123';
        $data['layanan'][0] = new stdClass();
        $data['layanan'][0]->nama = '1';
        $data['layanan'][0]->jenis = 'laala';
        $data['status'] = 2;
        $data['adaTambahan'] ='';
        $data['action'] = $this->_module.'/save';
        $data['action2'] = $this->_module.'/saveres';
        $data['action3'] = $this->_module.'/savelay';
        $id = $this->input->get('id_pendaftaran');
        $id_pendaftaran = 'DA'.$this->input->get('id_pendaftaran');
        if($id != ''){
                $result = $this->pemem->getData($id_pendaftaran);
                if($result->num_rows() > 0){
                    $data['status'] = 1;
                    $data['hasil_daftar'] = $result->row();
                    $data['pasien_nama'] = $this->pemem->getNamaPasien($id_pendaftaran)->row();
                    $data['layanan'] = $this->pemem->getLayanan($id_pendaftaran);
                    $data['obat'] = $this->resep->optionObat();
                    $data['layananOP'] = $this->pemem->optionLayanan('--Pilih Layanan--',$data['pasien_nama']->id_poliklinik);
                }
                else{
                    $data['status'] = 0;
                }

                $reseplt = $this->resep->getData($id_pendaftaran);
                if($reseplt->num_rows() != 0 ){
                    $data['status'] = 3;
                    $data['resephasilnya'] = $reseplt->result();
                }

                $tambahan_rst = $this->pemem->getDataTambahan($id_pendaftaran);
                if($tambahan_rst->num_rows() != 0 ){
                    $data['adaTambahan'] = 'style="display:none"';
                }


        }	

		$this->load->view($this->_header);
		$this->load->view($this->_module.'/index',$data);
		$this->load->view($this->_footer);
		
	}

    public function save(){
            
        $id = $this->input->post('id');
        $this->db->trans_begin();
        $dataIns   = $this->input->post("dataDokter");

        $this->pemem->save($dataIns, $id);

        if ($this->db->trans_status() === FALSE ) {
            $this->db->trans_rollback();
            echo "<script>
            alert('Data tidak tersimpan karna kesalahan tertentu ');
            window.location.href='".base_url()."asisten_dokter';
            </script>";
        }
        else {
            $this->db->trans_commit();
            echo "<script>
            alert('Data Berhasil Disimpan ');
            window.location.href='".base_url()."asisten_dokter?id_pendaftaran=".substr($id, 2)."';
            </script>";   
        }
        
    }

    public function saveres(){
        $dataIns   = $this->input->post("dataResep");
        $dataIns['id_pendaftaran']   = $this->input->post("dataResDaftar");
        $dataIns['id_resep'] = 'RE'.random_string('numeric',12);
        $detail = json_decode($this->input->post('dataRes'));

        $this->db->trans_begin();
    
        $this->resep->save_as_new($dataIns);
        foreach ($detail as $key) {
            $dataInsDet['dosis'] = $key->Dosis;
            $dataInsDet['id_obat'] = $key->id_obat;
            $dataInsDet['id_resep'] = $dataIns['id_resep'];
            $dataInsDet['subtotal'] = $key->Subtotal;
            $dataInsDet['jumlah'] = $key->Jumlah;

            $this->resep->save_as_new_detail($dataInsDet);
        }

        if ($this->db->trans_status() === FALSE ) {
            $this->db->trans_rollback();
            echo "<script>
            alert('Data tidak tersimpan karna kesalahan tertentu ');
            window.location.href='".base_url()."asisten_dokter';
            </script>";
        }
        else {
            $this->db->trans_commit();
            echo "<script>
            alert('Data Berhasil Disimpan ');
            window.location.href='".base_url()."asisten_dokter?id_pendaftaran=".substr($dataIns['id_pendaftaran'], 2)."';
            </script>";   
        }
        
    }

    public function savelay(){
        $dataIns['id_pemeriksaan'] = 'PE'.random_string('numeric',12); 
        $dataIns['id_pendaftaran']   = $this->input->post("id");
        $detail = json_decode($this->input->post('dataLay'));

        $this->db->trans_begin();
    
        $this->pemem->save_as_new_periksa($dataIns);
        foreach ($detail as $key) {
            $dataInsDet['id_pemeriksaan'] = $dataIns['id_pemeriksaan'];
            $dataInsDet['id_p_layanan'] = $key->id_p_layanan;

            $this->pemem->save_as_new_periksa_detail($dataInsDet);
        }

        if ($this->db->trans_status() === FALSE ) {
            $this->db->trans_rollback();
            echo "<script>
            alert('Data tidak tersimpan karna kesalahan tertentu ');
            window.location.href='".base_url()."asisten_dokter';
            </script>";
        }
        else {
            $this->db->trans_commit();
            echo "<script>
            alert('Data Berhasil Disimpan ');
            window.location.href='".base_url()."asisten_dokter?id_pendaftaran=".substr($dataIns['id_pendaftaran'], 2)."';
            </script>";   
        }
        
    }

    public function harga($id_obat){
        $this->cekstatuslogin();
        $result = $this->resep->getHarga($id_obat);
        echo $result->harga;
    }




}
?>
