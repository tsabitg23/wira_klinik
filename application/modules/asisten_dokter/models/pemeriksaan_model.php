<?php

class pemeriksaan_model extends CI_Model{
    private $_table           = 'pendaftaran';
    private $_table1           = 'v_pendaftaran';
    private $_table2           = 'pemeriksaan';
    private $_table3           = 'pemeriksaan_detail';
    protected $primary_key  = 'id_pendaftaran';


    // untuk update data
    public function save($data, $key) {
        $this->db->set('mdf','now()',false);
        $this->db->update($this->_table, $data, array($this->primary_key => $key));
    }

    public function save_as_new_periksa($data) {
        $this->db->set('crt','now()',false);
        $this->db->set('mdf','now()',false);
        $this->db->set('tipe','tambahan');
        $this->db->insert($this->_table2, $data);
    }

    public function save_as_new_periksa_detail($data) {
        $this->db->set('crt','now()',false);
        $this->db->set('mdf','now()',false);
        $this->db->set('id_p_detail','uuid()',false);
        $this->db->insert($this->_table3, $data);
    }
    
    public function getData($key    = ""){
        $this->db->select("*");
        $this->db->where('dlt',null);
        $this->db->from($this->_table);

        if($key != "")
            $this->db->where($this->primary_key,$key);

        return $this->db->get();
    }

    public function getNamaPasien($id_pendaftaran){
        $this->db->select("nama,poliklinik,id_poliklinik");
        $this->db->where('id_pendaftaran',$id_pendaftaran);
        $this->db->from($this->_table1);

        return $this->db->get();   
    }

    public function getLayanan($id_pendaftaran){
        $query = $this->db->query("select poliklinik_layanan.nama,pemeriksaan.tipe as jenis from pendaftaran inner join pemeriksaan on pemeriksaan.id_pendaftaran = pendaftaran.id_pendaftaran inner join pemeriksaan_detail on pemeriksaan_detail.id_pemeriksaan = pemeriksaan.id_pemeriksaan inner join poliklinik_layanan on poliklinik_layanan.id_p_layanan = pemeriksaan_detail.id_p_layanan where pendaftaran.id_pendaftaran = '".$id_pendaftaran."' order by pemeriksaan.tipe ASC");
        return $query->result();
    }

    public function optionLayanan($default = '--Pilih Obat--',$id_poli) {
        $option = array();
          $this->db->where('dlt',null);
          $this->db->where('id_poliklinik',$id_poli);
          $this->db->from('poliklinik_layanan a');

        $list   = $this->db->get();

        if (!empty($default))
            $option['kosong'] = $default;

        foreach ($list->result() as $row) {
            $option[$row->id_p_layanan] = $row->nama;
        }

        return $option;
    }

    public function getDataTambahan($id_pend,$key    = ""){
        $this->db->select("b.*");
        $this->db->where('a.dlt',null);
        $this->db->where('a.id_pendaftaran',$id_pend);
        $this->db->where('b.tipe','tambahan');
        $this->db->join('pemeriksaan b','b.id_pendaftaran = a.id_pendaftaran','inner');
        $this->db->from($this->_table.' a');

        if($key != "")
            $this->db->where($this->primary_key,$key);

        return $this->db->get();
    }

    

}