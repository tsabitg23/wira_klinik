<?php

class resep_model extends CI_Model{
    private $_table           = 'resep';
    private $_table1           = 'resep_detail';
    private $_table2           = 'obat';
    protected $primary_key  = 'id_resep';


    public function save_as_new($data) {
        $this->db->set('crt','now()',false);
        $this->db->set('mdf','now()',false);
        $this->db->set('tgl','now()',false);
        $this->db->insert($this->_table, $data);
    }

    public function save_as_new_detail($data) {
        $this->db->set('crt','now()',false);
        $this->db->set('mdf','now()',false);
        $this->db->set('id_r_detail','uuid()',false);
        $this->db->insert($this->_table1, $data);
    }

    // untuk update data
    public function save($data, $key) {
        $this->db->set('mdf','now()',false);
        $this->db->update($this->_table, $data, array($this->primary_key => $key));
    }

    public function getData($id_pend,$key    = ""){
        $this->db->select("b.*,c.nama as nama_obat");
        $this->db->where('a.dlt',null);
        $this->db->where('a.id_pendaftaran',$id_pend);
        $this->db->join('resep_detail b','b.id_resep = a.id_resep','inner');
        $this->db->join('obat c','b.id_obat = c.id_obat','inner');
        $this->db->from($this->_table.' a');

        if($key != "")
            $this->db->where($this->primary_key,$key);

        return $this->db->get();
    }

    public function optionObat($default = '--Pilih Obat--',$key = '') {
        $option = array();
        $this->db->where('dlt',null);
        $this->db->from('obat');

        if(!empty($key))
            $this->db->where('id_obat',$key);

        $list   = $this->db->get();

        if (!empty($default))
            $option['kosong'] = $default;

        foreach ($list->result() as $row) {
            $option[$row->id_obat] = $row->nama;
        }

        return $option;
    }

    public function getHarga($key = ''){
        $this->db->select("a.harga");
        $this->db->where('a.dlt',null);
        $this->db->from('obat a');

        if($key != "")
            $this->db->where('id_obat',$key);

        return $this->db->get()->row();   
    }

    

}