<head>
    <title>Pemeriksaan Pasien</title>
</head>


<div class="warper container-fluid">
    <ol class="breadcrumb">
        <li>Asisten Dokter</li>
        <li class="actives">Pemeriksaan Pasien</li>
    </ol>
    <div class="page-header"><h1>Pemeriksaan Pasien</h1></div>

    <div class="row">
       <div class="col-md-12">
          <h5 class="no-margn tabular">Cari data pendaftaran untuk input data</h5>    
          <hr>
          <div class="alert alert-danger alert-dismissible" role="alert" id="hasilZero" style="display:none;">
             <strong>Hasil Tidak Ditemukan!</strong> Tidak dapat menemukan data pasien 
         </div>
         <div class="alert alert-success alert-dismissible" role="alert" id="hasilFound" style="display:none;">
             <strong>Data Ditemukan!</strong> Silahkan mengisi form yang tersedia 
         </div>
         <form role="form" id="searchnya" method="GET" action="<?php echo base_url('asisten_dokter');?>">
             <div class="form-group">
                <div class="form-group">
                    <input type="hidden" value="<?php echo $status;?>" id="result">
                    <div class="col-sm-1">
                        <?php 
                        $data = array(
                            'name'        => 'depan',
                            'id'          => 'depan',
                            'value'       => 'DA',
                            'type' => 'text',
                            'disabled' =>true,
                            'class' => 'form-control'
                            );

                        echo form_input($data);
                        ?>
                    </div>
                    <div class="col-sm-3">
                      <?php 
                      $data = array(
                         'name'        => 'id_pendaftaran',
                         'id'          => 'id_pendaftaran',
                         'value'       => '',
                         'placeholder' => 'Kode Pendaftaran',
                         'type' => 'number',
                         'class' => 'form-control'
                         );

                      echo form_input($data);
                      ?>
                  </div>
                  <div class="col-sm-5">
                      <button class="btn btn-success" id="searchRow"><i class="fa fa-search"></i> Cari</button>
                      <a type="submit" class="btn btn-info" id="tutupan" href="<?php echo base_url().'asisten_dokter';?>"><i class="fa fa-refresh"></i> Refresh</a>
                  </div>
              </div>
          </div>
      </form>	
  </div>
</div>

<div class="row" style="margin-top:5%;">
	<div class="col-md-12" id="hasilDetail" style="display:none;">
		<div class="panel panel-default">
			<div class="panel-heading">Data Pemeriksaan </div>
			<div class="panel-body nicescroll">
                <?php 
                $attrib = array('class' => 'form-horizontal','id'=>'form-pendaftaran');
                $hidden = array('id' => $hasil_daftar->id_pendaftaran);
                echo form_open($action,$attrib,$hidden);?>
                <div class="form-group">
                    <label for="kode" class="col-sm-2 control-label">Kode Pendaftaran</label>
                    <div class="col-sm-8">
                        <?php echo form_input('kode',!empty($hasil_daftar->id_pendaftaran)? $hasil_daftar->id_pendaftaran : '', 'placeholder="Nama Pasien" class="form-control col-md-7 col-xs-12" id="kode" disabled') ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="nama" class="col-sm-2 control-label">Nama</label>
                    <div class="col-sm-8">
                        <?php echo form_input('nama',!empty($pasien_nama->nama)? $pasien_nama->nama : '', 'placeholder="Nama Pasien" class="form-control col-md-7 col-xs-12" id="nama" disabled') ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="poliklinik" class="col-sm-2 control-label">Poliklinik</label>
                    <div class="col-sm-8">
                        <?php echo form_input('poliklinik',!empty($pasien_nama->poliklinik)? $pasien_nama->poliklinik : '', 'placeholder="Nama Pasien" class="form-control col-md-7 col-xs-12" id="poliklinik" disabled') ?>
                    </div>
                </div>
                    <table id="tbl_layanan_defa" class="table table-bordered" style="width:60%;margin:20px 17%;">
                    <thead>
                        <th width="1">No</th>
                        <th>Layanan</th>
                        <th>Jenis</th>
                    </thead>
                    <tbody>
                        <?php $no=0; foreach ($layanan as $pen ) { $no++;?>    
                            <tr>
                                <td><?php echo $no;?></td>
                                <td><?php echo $pen->nama;?></td>
                                <td><?php echo $pen->jenis;?></td>
                            </tr>
                            <?php

                        } ?>
                    </tbody>
                </table>
                <div class="form-group">
                    <label for="keluhan" class="col-sm-2 control-label">Keluhan</label>
                    <div class="col-sm-8">
                        <?php 
                        $data = array(
                            'name'        => 'dataDokter[keluhan]',
                            'id'          => 'keluhan',
                            'value'       => !empty($hasil_daftar->keluhan)? $hasil_daftar->keluhan : '',
                            'cols'        =>'40',
                            'rows'        =>'6',
                            'placeholder' => 'Keluhan Pasien',
                            'class' => 'form-control'
                            );

                        echo form_textarea($data);
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="diagnosis" class="col-sm-2 control-label">Diagnosis</label>
                    <div class="col-sm-8">
                        <?php 
                        $data = array(
                            'name'        => 'dataDokter[diagnosis]',
                            'id'          => 'diagnosis',
                            'value'       => !empty($hasil_daftar->diagnosis)? $hasil_daftar->diagnosis : '',
                            'cols'        =>'40',
                            'rows'        =>'6',
                            'placeholder' => 'Diagnosis dari dokter',
                            'class' => 'form-control'
                            );

                        echo form_textarea($data);
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-9">
                        <a type="submit" class="btn btn-primary" id="simpanan"><i class="fa fa-save"></i> Simpan</a>
                        <a type="submit" class="btn btn-info" <?php echo $adaTambahan;?> id="tambahan"><i class="fa fa-plus"></i> Tambah Layanan</a>
                    </div>
                </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12" id="input_tambahan" style="display:none;">
        <div class="panel panel-default">
            <div class="panel-heading">Input Layanan Tambahan</div>
            <div class="panel-body nicescroll">
                <?php 
                $attrib = array('class' => 'form-horizontal','id'=>'form-layanan');
                $hidden = array('id' => $hasil_daftar->id_pendaftaran);
                echo form_open($action3,$attrib,$hidden);?>
                <input type="hidden" id="dataLay" name="dataLay" value="">
                        <div class="form-group">
                            <label for="layanan" class="col-sm-2 control-label">Layanan</label>
                            <div class="col-sm-3">
                                <?php echo form_dropdown('layanan',$layananOP,'', 'class="form-control choose-select" id="layananOP" ') ?>
                            </div>
                            <div class="col-sm-2">
                                <a class="btn btn-success" id="addRow">Tambah</a>
                            </div>
                        </div>
                    <table id="tbl_lay" class="table table-bordered" style="width:80%;margin:20px 17%;">
                    <thead>
                        <th>Layanan</th>
                        <th>Aksi</th>
                        <th style="visibility: hidden;">id_p_layanan</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-9">
                        <a class="btn btn-primary" id="simpan_layanan"><i class="fa fa-save"></i> Simpan</a>
                        <a type="submit" class="btn btn-danger" id="tutup_layanan" href="<?php echo base_url().'asisten_dokter';?>"><i class="fa fa-close"></i> Tutup Form</a>
                    </div>
                </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>

<div class="row" >
    <div class="col-md-12" id="formResep" style="display:none;">
        <div class="panel panel-default">
            <div class="panel-heading">Input Resep </div>
            <div class="panel-body nicescroll">
                <?php 
                $attrib = array('class' => 'form-horizontal','id'=>'form-resep');
                $hidden = array('id_pendaftaran' => $hasil_daftar->id_pendaftaran);
                echo form_open($action2,$attrib,$hidden);?>
                <div class="form-group">
                    <label for="id_obat" class="col-sm-2 control-label">ID Pasien</label>
                    <div class="col-sm-3">
                        <?php 
                        echo form_dropdown('id_obat',$obat,'','class="form-control choose-select" id="id_obat"');
                        ?>
                    </div>
                </div>
                <input type="hidden" name="dataRes" value="haha" id="dataRes">
                <input type="hidden" name="dataResDaftar" value="<?php echo $hasil_daftar->id_pendaftaran;?>" id="dataResDaftar">
                <div class="form-group">
                    <label for="harga" class="col-sm-2 control-label">Harga</label>
                    <div class="col-sm-5">
                        <?php echo form_input('harga','', 'placeholder="Harga Obat" class="form-control col-md-7 col-xs-12" id="hargaobatnya" disabled') ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="dosis" class="col-sm-2 control-label">Dosis</label>
                    <div class="col-sm-5">
                        <?php echo form_input('dosis','', 'placeholder="Dosis Obat/Hari" class="form-control col-md-7 col-xs-12" id="dosis"') ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="jumlah" class="col-sm-2 control-label">Jumlah</label>
                    <div class="col-sm-5">
                        <?php 
                        $data = array(
                            'name'        => 'jumlah',
                            'id'          => 'jumlah',
                            'placeholder' => 'Jumlah obat',
                            'type' => 'number',
                            'class' => 'form-control'
                            );

                        echo form_input($data);
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-9">
                        <a class="btn btn-success" id="tambahResep">Tambah</a>
                    </div>
                </div>
                <table id="tbl_resep" class="table table-bordered" style="width:80%;margin:20px 17%;">
                    <thead>
                        <th>Obat</th>
                        <th>Dosis</th>
                        <th>Jumlah</th>
                        <th>Subtotal</th>
                        <th>Aksi</th>
                        <th style="visibility:hidden;">id_obat</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <div class="form-group">
                    <label for="total" class="col-sm-2 control-label">Total</label>
                    <div class="col-sm-5">
                        <?php echo form_input('dataResep[total]','0', 'class="form-control col-md-7 col-xs-12" readonly id="total"') ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-9">
                        <a type="submit" class="btn btn-primary" id="simpan_resep"><i class="fa fa-save"></i> Simpan</a>
                        <a type="submit" class="btn btn-danger" id="tutupan" href="<?php echo base_url().'asisten_dokter';?>"><i class="fa fa-close"></i> Batal</a>
                    </div>
                </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>


<div class="row" >
    <div class="col-md-12" id="hasilResep" style="display:none;">
        <div class="panel panel-default">
            <div class="panel-heading">Data Resep </div>
            <div class="panel-body nicescroll">
                <table id="tbl_info" class="table">
                  <thead>
                    <tr>
                      <th width="1">No</th>
                      <th>Obat</th>
                      <th>Dosis</th>
                      <th>Jumlah</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php $no=0; foreach ($resephasilnya as $resep ) { $no++;?>    
                            <tr>
                                <td><?php echo $no;?></td>
                                <td><?php echo $resep->nama_obat;?></td>
                                <td><?php echo $resep->dosis;?></td>
                                <td><?php echo $resep->jumlah;?></td>
                            </tr>
                            <?php

                        } ?>
                  </tbody>
                  </table>
                
            </div>
        </div>
    </div>
</div>

</div>

<?=js('jquery/jquery.min.js')?>
<?=js('bootstrap/bootstrap.min.js')?>
<?=js('app/custom.js')?>
<?=js('plugins/underscore/underscore-min.js')?> 
<?=js('plugins/nicescroll/jquery.nicescroll.min.js')?>
<?=js('plugins/select2/select2.min.js')?>
<?=js('plugins/tabletojson/jquery.tabletojson.js')?>

<script type="text/javascript">
    $(document).ready(function($){

       $('#pemeriksaan').addClass('active');
       var status = $('#result').val();
       hasilZero(status);
       $('#simpanan').click(function(){
        $('#form-pendaftaran').submit();
    });
       $("#id_obat").change(function (){
        if($(this).val() != 'kosong'){
            var url = "<?php echo site_url('asisten_dokter/harga');?>/"+$(this).val();
            $.get(url, function(result) {
                $('#hargaobatnya').val(result);
            });
        }
        else{
            $('#hargaobatnya').val('');
        }
        return false;
    })
       $('#layananOP').change(function(){
            validasi_tombol();
       })
       validate();
       validasi_tombol();
       $('#keluhan,#diagnosis').on('change paste keyup',validate);
       $('#tambahan').click(function(){
            $('#input_tambahan').css('display','inherit');
            $('#layananOP').focus();
       })

       $('#tambahResep').on('click', function () {
        // console.log($('#id_obat option:selected').text());
        var harga = parseInt($('#hargaobatnya').val());
        var jumlah = parseInt($('#jumlah').val());
        var subtotal = harga*jumlah;

        var data = '<tr><td>'+$('#id_obat option:selected').text()+'</td><td>'+$('#dosis').val()+'</td><td>'+$('#jumlah').val()+'</td><td>'+subtotal+'</td><td><a class="btn btn-danger btn-xs hapus-dong"><i class="fa fa-close"></i></a></td><td width="1" style="display:none;">'+$('#id_obat').val()+'</td></tr>';
        $('#tbl_resep').append(data);
        var a = parseInt($('#total').val());
        var b = subtotal;
        $('#total').val(a+b);
        $('#id_obat').val("").trigger('change');
        $('#hargaobatnya').val("");
        $('#jumlah').val("");
        $('#dosis').val("");
        $('#id_obat').focus();
        validatenyaTambahResep();
    } );
       $('#tbl_resep').on('click','a.hapus-dong',function(){
          var a = parseInt($(this).parent().prev().text());
          var b = parseInt($('#total').val());
          $('#total').val(b-a);
          $(this).parent().parent().remove();
          validatenyaTambahResep();
      })

       $('#simpan_resep').on('click',function(){
          var table = $('#tbl_resep').tableToJSON({
            ignoreColumns : [0,4]
        });
          var url   = "<?php echo base_url('/registrasi/saveres')?>";
          
          $('#dataRes').val(JSON.stringify(table));
          console.log($('#dataRes').val());
          $('#form-resep').submit();
      });



          $('#addRow').on('click', function () {
           var data = '<tr><td>'+$('#layananOP option:selected').text()+'</td><td><a class="btn btn-danger btn-xs hapus-dong"><i class="fa fa-close"></i></a></td><td width="1" style="display:none;">'+$('#layananOP').val()+'</td></tr>';
           $('#tbl_lay').append(data);
           $('#layananOP').val("").trigger('change');
           $('#layananOP').focus();
           validatenyaTambah();
       } );
          $('#tbl_lay').on('click','a.hapus-dong',function(){
             $(this).parent().parent().remove();
             validatenyaTambah();
         })

          $('#simpan_layanan').on('click',function(){
             var table = $('#tbl_lay').tableToJSON({
               ignoreColumns : [0,1]
           });
             var url   = "<?php echo base_url('/registrasi/saveres')?>";
             
             $('#dataLay').val(JSON.stringify(table));
             $('#form-layanan').submit();
         });

          $('#id_obat').change(function(){
            validatePassnambahTambahResep();
          })
          $('#dosis,#jumlah').on('change paste keyup',function(){
                validatePassnambahTambahResep();
          })

          validatenyaTambah();
          validatenyaTambahResep()
          validatePassnambahTambahResep();
   });

    function hasilZero(status){
       if(status ==0){
          $('#hasilZero').css('display','inherit');
          $('#id_pendaftaran').focus();
      }
      else if(status ==1){
          $('#hasilFound').css('display','inherit');
          $('#hasilDetail').css('display','inherit');
          $('#formResep').css('display','inherit');
      }
      else if(status == 2){
        $('#id_pendaftaran').focus();
      }
      else if(status == 3){
           $('#hasilFound').css('display','inherit');
           $('#hasilDetail').css('display','inherit');
           $('#hasilResep').css('display','inherit');
      }
}

    function validasi_tombol(){
        if ($('#layananOP').val().length    >   6) {
            $("#addRow").removeClass("disabled");
        }
        else {
            $("#addRow").addClass("disabled");
        }       
    }

function validate(){
    if ($('#keluhan').val().length    >   0 && $('#diagnosis').val().length    >   0 ) {
        $("#simpanan").removeClass("disabled");
    }
    else {
        $("#simpanan").addClass("disabled");
    }
}

function validatenyaTambah(){
    
    a = $('#tbl_lay').children().next().children().children().text().length;
    if(a == 0){
        $('#simpan_layanan').addClass('disabled');
    }
    else{
        $('#simpan_layanan').removeClass('disabled');
    }
    
}

function validatenyaTambahResep(){
    
    a = $('#tbl_resep').children().next().children().children().text().length;
    if(a == 0){
        $('#simpan_resep').addClass('disabled');
    }
    else{
        $('#simpan_resep').removeClass('disabled');
    }
    
}

function validatePassnambahTambahResep(){
    
    if($('#id_obat').val() != 'kosong' && $('#dosis').val().length > 0 && $('#jumlah').val().length > 0 && $('#jumlah').val() > 0){
        $('#tambahResep').removeClass('disabled');
    }
    else{
        $('#tambahResep').addClass('disabled');
    }
    
}
</script>