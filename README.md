Wira Klinik - Polyclinic Management Application 
---
**_We Are Fast, Precise, And Care_**

Adalah Aplikasi yang ditujukan sebagai pemenuhan tugas UKOM 2017. Aplikasi yang khusus dibuat untuk poliklinik agar proses pelayanan menjadi lebih maksimal dengan bantuan komputer

### Fitur
1. Tampilan responsive admin dan user
2. Fitur Daftar Pasien serta penebusan Obat
3. Pelayanan dan Input Resep Dokter
4. Lihat Rekam Medis Bagi Pasien

Dan masih banyak lagi

### Developer
[Tsabit Ghazwan](https://facebook.com/QNSRyoma)

contact : (+62) 896 1476 9353